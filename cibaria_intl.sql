-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 16, 2018 at 08:26 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cibariaintl86554`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2017-12-20 21:56:38', '2017-12-20 21:56:38', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href="https://gravatar.com">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://www.cibariaoutlet.com', 'yes'),
(2, 'home', 'https://www.cibariaoutlet.com', 'yes'),
(3, 'blogname', 'Cibaria International', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'bsiegel@cibaria-intl.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '1', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:88:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=4&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:29:"cibaria_coas/cibaria-coas.php";i:1;s:31:"cibaria_forms/cibaria-forms.php";i:2;s:41:"cibaria_specsheets/cibaria-specsheets.php";i:3;s:51:"display-posts-shortcode/display-posts-shortcode.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'cibaria', 'yes'),
(41, 'stylesheet', 'cibaria', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '4', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'wp_user_roles', 'a:5:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:61:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(95, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'sidebars_widgets', 'a:2:{s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(100, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(101, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:4:{i:1518818200;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1518818206;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1518889342;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(115, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1518776088;s:7:"checked";a:4:{s:7:"cibaria";s:5:"0.0.1";s:13:"twentyfifteen";s:3:"1.9";s:15:"twentyseventeen";s:3:"1.4";s:13:"twentysixteen";s:3:"1.4";}s:8:"response";a:0:{}s:12:"translations";a:0:{}}', 'no'),
(118, 'can_compress_scripts', '0', 'no'),
(123, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1513807957;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(125, 'current_theme', 'Cibaria-intl.com default theme', 'yes'),
(126, 'theme_mods_cibaria', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(127, 'theme_switched', '', 'yes'),
(180, 'recently_activated', 'a:0:{}', 'yes'),
(189, 'wpforms_preview_page', '56', 'yes'),
(190, 'wpforms_version', '1.4.3', 'yes'),
(191, 'wpforms_activated', 'a:1:{s:4:"lite";i:1516737640;}', 'yes'),
(194, 'widget_wpforms-widget', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(195, '_amn_wpforms-lite_last_checked', '1516665600', 'yes'),
(196, 'wpforms_review', 'a:2:{s:4:"time";i:1516737640;s:9:"dismissed";b:0;}', 'yes'),
(199, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:2:{i:0;O:8:"stdClass":10:{s:8:"response";s:7:"upgrade";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.4.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.4.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.4-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.4-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.4-partial-2.zip";s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.4";s:7:"version";s:5:"4.9.4";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.2";}i:1;O:8:"stdClass":11:{s:8:"response";s:10:"autoupdate";s:8:"download";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.4.zip";s:6:"locale";s:5:"en_US";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:59:"https://downloads.wordpress.org/release/wordpress-4.9.4.zip";s:10:"no_content";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.4-no-content.zip";s:11:"new_bundled";s:71:"https://downloads.wordpress.org/release/wordpress-4.9.4-new-bundled.zip";s:7:"partial";s:69:"https://downloads.wordpress.org/release/wordpress-4.9.4-partial-2.zip";s:8:"rollback";s:70:"https://downloads.wordpress.org/release/wordpress-4.9.4-rollback-2.zip";}s:7:"current";s:5:"4.9.4";s:7:"version";s:5:"4.9.4";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:5:"4.9.2";s:9:"new_files";s:0:"";}}s:12:"last_checked";i:1518776086;s:15:"version_checked";s:5:"4.9.2";s:12:"translations";a:0:{}}', 'no'),
(206, 'theme_mods_twentyfifteen', 'a:1:{s:18:"custom_css_post_id";i:-1;}', 'yes'),
(244, 'auto_core_update_failed', 'a:6:{s:9:"attempted";s:5:"4.9.4";s:7:"current";s:5:"4.9.2";s:10:"error_code";s:19:"mkdir_failed_pclzip";s:10:"error_data";N;s:9:"timestamp";i:1517936858;s:5:"retry";b:0;}', 'no'),
(245, 'auto_core_update_notified', 'a:4:{s:4:"type";s:4:"fail";s:5:"email";s:24:"bsiegel@cibaria-intl.com";s:7:"version";s:5:"4.9.4";s:9:"timestamp";i:1517936858;}', 'no'),
(263, 'WPLANG', '', 'yes'),
(264, 'new_admin_email', 'bsiegel@cibaria-intl.com', 'yes'),
(389, '_site_transient_timeout_theme_roots', '1518777886', 'no'),
(390, '_site_transient_theme_roots', 'a:4:{s:7:"cibaria";s:7:"/themes";s:13:"twentyfifteen";s:7:"/themes";s:15:"twentyseventeen";s:7:"/themes";s:13:"twentysixteen";s:7:"/themes";}', 'no'),
(391, '_site_transient_update_plugins', 'O:8:"stdClass":4:{s:12:"last_checked";i:1518776087;s:8:"response";a:1:{s:19:"akismet/akismet.php";O:8:"stdClass":11:{s:2:"id";s:21:"w.org/plugins/akismet";s:4:"slug";s:7:"akismet";s:6:"plugin";s:19:"akismet/akismet.php";s:11:"new_version";s:5:"4.0.2";s:3:"url";s:38:"https://wordpress.org/plugins/akismet/";s:7:"package";s:56:"https://downloads.wordpress.org/plugin/akismet.4.0.2.zip";s:5:"icons";a:3:{s:2:"1x";s:59:"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272";s:2:"2x";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";s:7:"default";s:59:"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272";}s:7:"banners";a:2:{s:2:"1x";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";s:7:"default";s:61:"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:2:{s:51:"display-posts-shortcode/display-posts-shortcode.php";O:8:"stdClass":9:{s:2:"id";s:37:"w.org/plugins/display-posts-shortcode";s:4:"slug";s:23:"display-posts-shortcode";s:6:"plugin";s:51:"display-posts-shortcode/display-posts-shortcode.php";s:11:"new_version";s:5:"2.9.0";s:3:"url";s:54:"https://wordpress.org/plugins/display-posts-shortcode/";s:7:"package";s:72:"https://downloads.wordpress.org/plugin/display-posts-shortcode.2.9.0.zip";s:5:"icons";a:3:{s:2:"1x";s:75:"https://ps.w.org/display-posts-shortcode/assets/icon-128x128.jpg?rev=985418";s:2:"2x";s:75:"https://ps.w.org/display-posts-shortcode/assets/icon-256x256.jpg?rev=985418";s:7:"default";s:75:"https://ps.w.org/display-posts-shortcode/assets/icon-256x256.jpg?rev=985418";}s:7:"banners";a:2:{s:2:"1x";s:77:"https://ps.w.org/display-posts-shortcode/assets/banner-772x250.jpg?rev=479842";s:7:"default";s:77:"https://ps.w.org/display-posts-shortcode/assets/banner-772x250.jpg?rev=479842";}s:11:"banners_rtl";a:0:{}}s:9:"hello.php";O:8:"stdClass":9:{s:2:"id";s:25:"w.org/plugins/hello-dolly";s:4:"slug";s:11:"hello-dolly";s:6:"plugin";s:9:"hello.php";s:11:"new_version";s:3:"1.6";s:3:"url";s:42:"https://wordpress.org/plugins/hello-dolly/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip";s:5:"icons";a:3:{s:2:"1x";s:63:"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907";s:2:"2x";s:63:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907";s:7:"default";s:63:"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907";}s:7:"banners";a:2:{s:2:"1x";s:65:"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342";s:7:"default";s:65:"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342";}s:11:"banners_rtl";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 4, '_edit_last', '1'),
(3, 4, '_edit_lock', '1513890890:1'),
(7, 11, '_edit_last', '1'),
(8, 11, '_edit_lock', '1517937632:1'),
(9, 11, '_wp_page_template', 'templates/products.php'),
(10, 13, '_edit_last', '1'),
(11, 13, '_edit_lock', '1513895695:1'),
(12, 13, '_wp_page_template', 'templates/internal.php'),
(13, 15, '_edit_last', '1'),
(14, 15, '_edit_lock', '1514327406:1'),
(15, 15, '_wp_page_template', 'templates/internal.php'),
(16, 19, '_edit_last', '1'),
(17, 19, '_edit_lock', '1518021007:1'),
(18, 19, '_wp_page_template', 'templates/internal.php'),
(19, 22, '_edit_last', '1'),
(20, 22, '_edit_lock', '1514327798:1'),
(21, 22, '_wp_page_template', 'templates/internal.php'),
(22, 24, '_edit_last', '1'),
(23, 24, '_edit_lock', '1514327953:1'),
(24, 24, '_wp_page_template', 'templates/internal.php'),
(25, 26, '_edit_last', '1'),
(26, 26, '_edit_lock', '1514328050:1'),
(27, 26, '_wp_page_template', 'templates/internal.php'),
(28, 28, '_edit_last', '1'),
(29, 28, '_edit_lock', '1514328174:1'),
(30, 28, '_wp_page_template', 'templates/internal.php'),
(31, 30, '_edit_last', '1'),
(32, 30, '_edit_lock', '1518021055:1'),
(33, 30, '_wp_page_template', 'templates/internal.php'),
(34, 35, '_edit_last', '1'),
(35, 35, '_edit_lock', '1518023997:1'),
(36, 35, '_wp_page_template', 'templates/internal.php'),
(37, 38, '_edit_last', '1'),
(38, 38, '_edit_lock', '1514330036:1'),
(39, 38, '_wp_page_template', 'templates/internal.php'),
(40, 40, '_edit_last', '1'),
(41, 40, '_edit_lock', '1518020913:1'),
(42, 40, '_wp_page_template', 'templates/internal.php'),
(43, 49, '_edit_last', '1'),
(44, 49, '_edit_lock', '1516736974:1'),
(45, 49, '_wp_page_template', 'templates/internal.php'),
(46, 51, '_edit_last', '1'),
(47, 51, '_edit_lock', '1516734995:1'),
(48, 51, '_wp_page_template', 'templates/internal.php'),
(49, 54, '_edit_last', '1'),
(50, 54, '_edit_lock', '1516737461:1'),
(51, 54, '_wp_page_template', 'templates/internal.php'),
(52, 58, '_edit_last', '1'),
(53, 58, '_edit_lock', '1516742008:1'),
(54, 58, '_wp_page_template', 'templates/internal.php'),
(55, 60, '_edit_last', '1'),
(56, 60, '_edit_lock', '1516743441:1'),
(57, 60, '_wp_page_template', 'templates/internal.php'),
(58, 63, '_edit_last', '1'),
(59, 63, '_edit_lock', '1516744054:1'),
(60, 63, '_wp_page_template', 'templates/internal.php'),
(61, 65, '_edit_last', '1'),
(62, 65, '_edit_lock', '1516745264:1'),
(63, 65, '_wp_page_template', 'templates/internal.php'),
(64, 1, '_wp_trash_meta_status', 'publish'),
(65, 1, '_wp_trash_meta_time', '1516744415'),
(66, 1, '_wp_desired_post_slug', 'hello-world'),
(67, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:"1";}'),
(68, 69, '_edit_last', '1'),
(69, 69, '_edit_lock', '1516744486:1'),
(72, 71, '_wp_trash_meta_status', 'publish'),
(73, 71, '_wp_trash_meta_time', '1516744684'),
(74, 72, '_wp_trash_meta_status', 'publish'),
(75, 72, '_wp_trash_meta_time', '1516744848'),
(76, 73, '_wp_trash_meta_status', 'publish'),
(77, 73, '_wp_trash_meta_time', '1516744887'),
(78, 74, '_edit_last', '1'),
(79, 74, '_edit_lock', '1517609154:1'),
(80, 74, '_wp_page_template', 'templates/internal.php'),
(81, 78, '_wp_attached_file', '2018/02/afoa.png'),
(82, 78, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:169;s:6:"height";i:165;s:4:"file";s:16:"2018/02/afoa.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"afoa-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(83, 79, '_wp_attached_file', '2018/02/ou.jpg'),
(84, 79, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:169;s:6:"height";i:169;s:4:"file";s:14:"2018/02/ou.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"ou-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(85, 80, '_wp_attached_file', '2018/02/sf.jpg'),
(86, 80, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:306;s:6:"height";i:169;s:4:"file";s:14:"2018/02/sf.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:14:"sf-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:14:"sf-300x166.jpg";s:5:"width";i:300;s:6:"height";i:166;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(87, 81, '_wp_attached_file', '2018/02/usda.jpg'),
(88, 81, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:169;s:6:"height";i:169;s:4:"file";s:16:"2018/02/usda.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"usda-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(89, 82, '_wp_attached_file', '2018/02/aocs.jpg'),
(90, 82, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:465;s:6:"height";i:150;s:4:"file";s:16:"2018/02/aocs.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"aocs-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"aocs-300x97.jpg";s:5:"width";i:300;s:6:"height";i:97;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(91, 83, '_wp_attached_file', '2018/02/uschamber.jpg'),
(92, 83, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:169;s:6:"height";i:169;s:4:"file";s:21:"2018/02/uschamber.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"uschamber-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(93, 84, '_wp_attached_file', '2018/02/siliker.jpg'),
(94, 84, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:235;s:6:"height";i:59;s:4:"file";s:19:"2018/02/siliker.jpg";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"siliker-150x59.jpg";s:5:"width";i:150;s:6:"height";i:59;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(95, 85, '_wp_attached_file', '2018/02/wbenc.jpg'),
(96, 85, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:136;s:6:"height";i:59;s:4:"file";s:17:"2018/02/wbenc.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(97, 86, '_wp_attached_file', '2018/02/org.jpg'),
(98, 86, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:314;s:6:"height";i:268;s:4:"file";s:15:"2018/02/org.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:15:"org-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:15:"org-300x256.jpg";s:5:"width";i:300;s:6:"height";i:256;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(99, 87, '_wp_attached_file', '2018/02/plma.jpg'),
(100, 87, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:820;s:6:"height";i:644;s:4:"file";s:16:"2018/02/plma.jpg";s:5:"sizes";a:3:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"plma-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"plma-300x236.jpg";s:5:"width";i:300;s:6:"height";i:236;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"plma-768x603.jpg";s:5:"width";i:768;s:6:"height";i:603;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(101, 90, '_wp_attached_file', '2018/02/haccp.jpg'),
(102, 90, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:140;s:6:"height";i:132;s:4:"file";s:17:"2018/02/haccp.jpg";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2017-12-20 21:56:38', '2017-12-20 21:56:38', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2018-01-23 21:53:35', '2018-01-23 21:53:35', '', 0, 'http://cibaria-intl.press/?p=1', 0, 'post', '', 1),
(4, 1, '2017-12-21 17:43:50', '2017-12-21 17:43:50', '<h1 style="padding: 25px; text-align: center;">The Natural Choice for Quality Oils, Premium Vinegars, Gourmet Foods,<br /> Gift Packs and Fine Accessories</h1>\r\n<p><span style="font-size: 12px; line-height: 5px;">Located in Southern California, Cibaria International is an established company with a proven track record. Incorporated in 1998, we continue to be a major force in the industry, packing and distributing branded and private label food products across the United States, Canada and Mexico.</span></p>\r\n<p><span style="font-size: 12px; line-height: 5px;">Our roots started in olive oil and quickly branched out into Specialty Oils, Certified Organic oils, Non GMO Oils, Vegetable Oils, Personal Care Oils, Aerosol Oils, Balsamic and other Vinegars, Gourmet Pantry items, and Accessories.  Today we carry hundreds of products and are proud to say we are still growing!</span></p>\r\n<p><span style="font-size: 12px; line-height: 5px;">Not all companies are the same. That is why we treat our customers as individuals and tailor the products to meet their company’s requirements. When you succeed, we succeed. Let us know what your needs are and we will start tailoring a program for you today.</span></p>\r\n<p><span style="font-size: 12px; line-height: 5px;">From product development to manufacturing and delivery, we are on a journey of continuous improvement, challenging ourselves to always strive for new levels of performance. We are committed to ensuring our customers are completely satisfied with the quality of the products, services and support they receive on an ongoing basis.</span></p>', 'Quality Wholesale Olive Oils and Vinegars', '', 'publish', 'closed', 'closed', '', 'quality-wholesale-olive-oils-and-vinegars', '', '', '2017-12-21 17:51:44', '2017-12-21 17:51:44', '', 0, 'http://cibaria-intl.press/?page_id=4', 0, 'page', '', 0),
(5, 1, '2017-12-21 17:43:50', '2017-12-21 17:43:50', '<h1 style="padding: 25px; text-align: center;">The Natural Choice for Quality Oils, Premium Vinegars, Gourmet Foods,<br /> Gift Packs and Fine Accessories</h1>\r\n<p><span style="font-size: 12px; line-height: 5px;">Located in Southern California, Cibaria International is an established company with a proven track record. Incorporated in 1998, we continue to be a major force in the industry, packing and distributing branded and private label food products across the United States, Canada and Mexico.<br /><br />\r\nOur roots started in olive oil and quickly branched out into Specialty Oils, Certified Organic oils, Non GMO Oils, Vegetable Oils, Personal Care Oils, Aerosol Oils, Balsamic and other Vinegars, Gourmet Pantry items, and Accessories.  Today we carry hundreds of products and are proud to say we are still growing!<br /><br />\r\nNot all companies are the same. That is why we treat our customers as individuals and tailor the products to meet their company’s requirements. When you succeed, we succeed. Let us know what your needs are and we will start tailoring a program for you today.<br /><br />\r\nFrom product development to manufacturing and delivery, we are on a journey of continuous improvement, challenging ourselves to always strive for new levels of performance. We are committed to ensuring our customers are completely satisfied with the quality of the products, services and support they receive on an ongoing basis.</span></p>', 'Quality Wholesale Olive Oils and Vinegars', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-12-21 17:43:50', '2017-12-21 17:43:50', '', 4, 'http://cibaria-intl.press/4-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2017-12-21 17:51:10', '2017-12-21 17:51:10', '<h1 style="padding: 25px; text-align: center;">The Natural Choice for Quality Oils, Premium Vinegars, Gourmet Foods,<br /> Gift Packs and Fine Accessories</h1>\n<p><span style="font-size: 12px; line-height: 5px;">Located in Southern California, Cibaria International is an established company with a proven track record. Incorporated in 1998, we continue to be a major force in the industry, packing and distributing branded and private label food products across the United States, Canada and Mexico.</span></p>\nOur roots started in olive oil and quickly branched out into Specialty Oils, Certified Organic oils, Non GMO Oils, Vegetable Oils, Personal Care Oils, Aerosol Oils, Balsamic and other Vinegars, Gourmet Pantry items, and Accessories.  Today we carry hundreds of products and are proud to say we are still growing!<br /><br />\nNot all companies are the same. That is why we treat our customers as individuals and tailor the products to meet their company’s requirements. When you succeed, we succeed. Let us know what your needs are and we will start tailoring a program for you today.<br /><br />\nFrom product development to manufacturing and delivery, we are on a journey of continuous improvement, challenging ourselves to always strive for new levels of performance. We are committed to ensuring our customers are completely satisfied with the quality of the products, services and support they receive on an ongoing basis.</span></p>', 'Quality Wholesale Olive Oils and Vinegars', '', 'inherit', 'closed', 'closed', '', '4-autosave-v1', '', '', '2017-12-21 17:51:10', '2017-12-21 17:51:10', '', 4, 'http://cibaria-intl.press/4-autosave-v1/', 0, 'revision', '', 0),
(8, 1, '2017-12-21 17:51:44', '2017-12-21 17:51:44', '<h1 style="padding: 25px; text-align: center;">The Natural Choice for Quality Oils, Premium Vinegars, Gourmet Foods,<br /> Gift Packs and Fine Accessories</h1>\r\n<p><span style="font-size: 12px; line-height: 5px;">Located in Southern California, Cibaria International is an established company with a proven track record. Incorporated in 1998, we continue to be a major force in the industry, packing and distributing branded and private label food products across the United States, Canada and Mexico.</span></p>\r\n<p><span style="font-size: 12px; line-height: 5px;">Our roots started in olive oil and quickly branched out into Specialty Oils, Certified Organic oils, Non GMO Oils, Vegetable Oils, Personal Care Oils, Aerosol Oils, Balsamic and other Vinegars, Gourmet Pantry items, and Accessories.  Today we carry hundreds of products and are proud to say we are still growing!</span></p>\r\n<p><span style="font-size: 12px; line-height: 5px;">Not all companies are the same. That is why we treat our customers as individuals and tailor the products to meet their company’s requirements. When you succeed, we succeed. Let us know what your needs are and we will start tailoring a program for you today.</span></p>\r\n<p><span style="font-size: 12px; line-height: 5px;">From product development to manufacturing and delivery, we are on a journey of continuous improvement, challenging ourselves to always strive for new levels of performance. We are committed to ensuring our customers are completely satisfied with the quality of the products, services and support they receive on an ongoing basis.</span></p>', 'Quality Wholesale Olive Oils and Vinegars', '', 'inherit', 'closed', 'closed', '', '4-revision-v1', '', '', '2017-12-21 17:51:44', '2017-12-21 17:51:44', '', 4, 'http://cibaria-intl.press/4-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2017-12-21 21:29:23', '2017-12-21 21:29:23', '<h2>Product you can trust, Service you can count on, and Unparalleled Dedication to being the best that we can be</h2>\r\nLocated in Southern California, Cibaria International is an established company with a proven track record. Incorporated in 1998, we continue to be a major force in the industry, packing and distributing branded and private label food products across the United States, Canada and Mexico.\r\n\r\nOur roots started in olive oil and quickly branched out into Specialty Oils, Certified Organic oils, Non GMO Oils, Vegetable Oils, Personal Care Oils, Aerosol Oils, Vinegars, Gourmet Pantry items, and Accessories. Today we carry hundreds of products and are proud to say we are still growing!\r\n\r\nFrom product development to manufacturing and delivery, we are on a journey of continuous improvement, challenging ourselves to always strive for new levels of performance. Committed to ensuring our customers are completely satisfied with the quality of the products, services and support they receive on an ongoing basis.', 'Products', '', 'publish', 'closed', 'closed', '', 'products', '', '', '2018-02-06 17:20:32', '2018-02-06 17:20:32', '', 0, 'http://cibaria-intl.press/?page_id=11', 0, 'page', '', 0),
(12, 1, '2017-12-21 21:29:23', '2017-12-21 21:29:23', '<h2>Product you can trust, Service you can count on, and Unparalleled Dedication to being the best that we can be</h2>\r\nLocated in Southern California, Cibaria International is an established company with a proven track record. Incorporated in 1998, we continue to be a major force in the industry, packing and distributing branded and private label food products across the United States, Canada and Mexico.\r\n\r\nOur roots started in olive oil and quickly branched out into Specialty Oils, Certified Organic oils, Non GMO Oils, Vegetable Oils, Personal Care Oils, Aerosol Oils, Vinegars, Gourmet Pantry items, and Accessories. Today we carry hundreds of products and are proud to say we are still growing!\r\n\r\nFrom product development to manufacturing and delivery, we are on a journey of continuous improvement, challenging ourselves to always strive for new levels of performance. Committed to ensuring our customers are completely satisfied with the quality of the products, services and support they receive on an ongoing basis.', 'Products', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2017-12-21 21:29:23', '2017-12-21 21:29:23', '', 11, 'http://cibaria-intl.press/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2017-12-21 22:30:43', '2017-12-21 22:30:43', '<h1>Our Facility</h1>\r\n<p>Riverside California has been our home since 2008. Having maxed out the available space (and then some!) in the existing facility, Cibaria made the decision in 2014 to move. Fortunately a state of the art building was found in the targeted area and the commitment was made. The new premises were designed to facilitate our on-going growth and we played an integral role in developing the plant’s production matrix to support our operation. The 23,000 square foot processing room with capabilities in excess of 30,000/ 500ml units per day, with multiple high speed lines, providing flexibility and high volume capacity.  The production lines are configured for continuous flow and continued improvement is a core aspect of the production process. The site has ample storage tanks providing our customers with added confidence of continued supply and improved transportation access. The manufacturing facility is in compliance with internationally recognized standards of current Good Manufacturing Practice’s (GMP’s) ensuring that every product produced has the attributes of Purity, Traceability, Safety and Quality.</p>\r\n<ul>\r\n<li>130,000 sq. ft. facility</li>\r\n<li>Plentiful and accessible loading docks</li>\r\n<li>Convenient Freeway Access</li>\r\n<li>23,000 sq. ft. State of the Art Processing Room</li>\r\n<li>PET, Plastic and Glass Bottle Packing Capabilities</li>\r\n<li>Tank Storage Farm</li>\r\n<li>Multiple Blending Totes</li>\r\n<li>Kosher Tote Washing Service</li>\r\n<li>Returnable Tote Program</li>\r\n<li>Bulk Packing Equipment</li>\r\n<li>Private Label Programs</li>\r\n<li>Custom Blending</li>\r\n<li>High Speed Net Weight Filler</li>\r\n<li>ROPP Capper Capability</li>\r\n<li>Non GMO Project Verification</li>\r\n<li>Organic Certified</li>\r\n<li>OU Kosher Certified</li>\r\n<li>In-House Graphic Designer</li>\r\n<li>Live Online Customer Service</li>\r\n<li>Online Bulk Quotes</li>\r\n</ul>         ', 'Capabilities', '', 'publish', 'closed', 'closed', '', 'capabilities', '', '', '2017-12-21 22:37:16', '2017-12-21 22:37:16', '', 0, 'http://cibaria-intl.press/?page_id=13', 0, 'page', '', 0),
(14, 1, '2017-12-21 22:30:43', '2017-12-21 22:30:43', '<h1>Our Facility</h1>\r\n<p>Riverside California has been our home since 2008. Having maxed out the available space (and then some!) in the existing facility, Cibaria made the decision in 2014 to move. Fortunately a state of the art building was found in the targeted area and the commitment was made. The new premises were designed to facilitate our on-going growth and we played an integral role in developing the plant’s production matrix to support our operation. The 23,000 square foot processing room with capabilities in excess of 30,000/ 500ml units per day, with multiple high speed lines, providing flexibility and high volume capacity.  The production lines are configured for continuous flow and continued improvement is a core aspect of the production process. The site has ample storage tanks providing our customers with added confidence of continued supply and improved transportation access. The manufacturing facility is in compliance with internationally recognized standards of current Good Manufacturing Practice’s (GMP’s) ensuring that every product produced has the attributes of Purity, Traceability, Safety and Quality.</p>\r\n<ul>\r\n<li>130,000 sq. ft. facility</li>\r\n<li>Plentiful and accessible loading docks</li>\r\n<li>Convenient Freeway Access</li>\r\n<li>23,000 sq. ft. State of the Art Processing Room</li>\r\n<li>PET, Plastic and Glass Bottle Packing Capabilities</li>\r\n<li>Tank Storage Farm</li>\r\n<li>Multiple Blending Totes</li>\r\n<li>Kosher Tote Washing Service</li>\r\n<li>Returnable Tote Program</li>\r\n<li>Bulk Packing Equipment</li>\r\n<li>Private Label Programs</li>\r\n<li>Custom Blending</li>\r\n<li>High Speed Net Weight Filler</li>\r\n<li>ROPP Capper Capability</li>\r\n<li>Non GMO Project Verification</li>\r\n<li>Organic Certified</li>\r\n<li>OU Kosher Certified</li>\r\n<li>In-House Graphic Designer</li>\r\n<li>Live Online Customer Service</li>\r\n<li>Online Bulk Quotes</li>\r\n</ul>         ', 'Capabilities', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2017-12-21 22:30:43', '2017-12-21 22:30:43', '', 13, 'http://cibaria-intl.press/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2017-12-26 22:29:01', '2017-12-26 22:29:01', '<p>Cibaria offers industrial and bulk oil sales on all of our Olive Oil, Seed Oil, and Vinegar Products. We strive to meet and exceed expectations on every level. Cibaria\'s ordering process is simple, and it\'s staff is friendly and easy to work with on special requirements.</p>\r\n<p><strong>We have the following pack sizes and services available:</strong></p>\r\n<p><strong>Pack Sizes</strong></p>\r\n<ul>\r\n<li>35 lbs packages</li>\r\n<li>55 gallon drums</li>\r\n<li>200 gallon disposable totes</li>\r\n<li>275 gallon returnable tote</li>\r\n<li>50,000 lb Tankers</li>\r\n</ul>\r\n<p><strong>Services</strong></p>\r\n<ul>\r\n<li>Custom Blends</li>\r\n<li>Contracts</li>\r\n<li>Returnable Tote Program</li>\r\n<li>Online Bulk Quotes</li>\r\n</ul>', 'Industrial and Bulk Capabilities', '', 'publish', 'closed', 'closed', '', 'industrial', '', '', '2017-12-26 22:31:52', '2017-12-26 22:31:52', '', 13, 'http://cibaria-intl.press/?page_id=15', 0, 'page', '', 0),
(16, 1, '2017-12-26 22:28:20', '2017-12-26 22:28:20', '', 'Industrial and Bulk Capabilities', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2017-12-26 22:28:20', '2017-12-26 22:28:20', '', 15, 'http://cibaria-intl.press/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2017-12-26 22:29:01', '2017-12-26 22:29:01', '<p>Cibaria offers industrial and bulk oil sales on all of our Olive Oil, Seed Oil, and Vinegar Products. We strive to meet and exceed expectations on every level. Cibaria\'s ordering process is simple, and it\'s staff is friendly and easy to work with on special requirements.</p>\r\n<p><strong>We have the following pack sizes and services available:</strong></p>\r\n<p><strong>Pack Sizes</strong></p>\r\n<ul>\r\n<li>35 lbs packages</li>\r\n<li>55 gallon drums</li>\r\n<li>200 gallon disposable totes</li>\r\n<li>275 gallon returnable tote</li>\r\n<li>50,000 lb Tankers</li>\r\n</ul>\r\n<p><strong>Services</strong></p>\r\n<ul>\r\n<li>Custom Blends</li>\r\n<li>Contracts</li>\r\n<li>Returnable Tote Program</li>\r\n<li>Online Bulk Quotes</li>\r\n</ul>', 'Industrial and Bulk Capabilities', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2017-12-26 22:29:01', '2017-12-26 22:29:01', '', 15, 'http://cibaria-intl.press/15-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2017-12-26 22:36:30', '2017-12-26 22:36:30', 'Creating your own brand identity in today\'s competitive marketplace is essential in growing and maintaining your business. With our private label program for olive oils and balsamic vinegars, we can assist you in developing your product line. We offer value-added solutions for private label olive oils, certified organic and specialty oils, blended oils, flavored oils, aerosol oils, and Italian balsamic vinegars.\r\n\r\n<strong>We pride ourselves on being flexible, quick, and honest.</strong>\r\n\r\nSmall minimums are available and can be done. Please contact us to discuss your private label program requirements or let our experienced team assist you in that development. Our creativity, flexibility, quality products, and superior customer service is sure to exceed your expectations.\r\n\r\n&nbsp;\r\n\r\n<img class="aligncenter size-full wp-image-87" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/plma.jpg" alt="" width="820" height="644" />', 'Private Label', '', 'publish', 'closed', 'closed', '', 'private-label', '', '', '2018-02-07 16:31:33', '2018-02-07 16:31:33', '', 13, 'http://cibaria-intl.press/?page_id=19', 0, 'page', '', 0),
(20, 1, '2017-12-26 22:35:28', '2017-12-26 22:35:28', 'Creating your own brand identity in today\'s competitive marketplace is essential in growing and maintaining your business. With our private label program for olive oils and balsamic vinegars, we can assist you in developing your product line. We offer value-added solutions for private label olive oils, certified organic and specialty oils, blended oils, flavored oils, aerosol oils, and Italian balsamic vinegars.\r\n\r\n<strong>We pride ourselves on being flexible, quick, and honest.</strong>\r\n\r\nSmall minimums are available and can be done. Please contact us to discuss your private label program requirements or let our experienced team assist you in that development. Our creativity, flexibility, quality products, and superior customer service is sure to exceed your expectations.\r\n\r\n<img class="aligncenter size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" />', 'Private Label', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2017-12-26 22:35:28', '2017-12-26 22:35:28', '', 19, 'http://cibaria-intl.press/19-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2017-12-26 22:36:07', '2017-12-26 22:36:07', 'Creating your own brand identity in today\'s competitive marketplace is essential in growing and maintaining your business. With our private label program for olive oils and balsamic vinegars, we can assist you in developing your product line. We offer value-added solutions for private label olive oils, certified organic and specialty oils, blended oils, flavored oils, aerosol oils, and Italian balsamic vinegars.\r\n\r\n<strong>We pride ourselves on being flexible, quick, and honest.</strong>\r\n\r\nSmall minimums are available and can be done. Please contact us to discuss your private label program requirements or let our experienced team assist you in that development. Our creativity, flexibility, quality products, and superior customer service is sure to exceed your expectations.\r\n\r\n<img class="aligncenter size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" />', 'Private Label', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2017-12-26 22:36:07', '2017-12-26 22:36:07', '', 19, 'http://cibaria-intl.press/19-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2017-12-26 22:37:53', '2017-12-26 22:37:53', ' <h2>Cooking with olive oil brings Fine Dining Flavor to the table.</h2>\r\n<p>Our Olive Oils give the taste of elegance to a wide variety of cuisines, making them a must-have item in all great kitchens! Whether it is menu development for a restaurant or product development for a food service manufacturer we have the product expertise to get the right flavor profile and appearance for all your culinary applications. In pursuit of a perfect pantry? Check out our specialty oils, blended oils, organic oils, flavored oils and aerosol oil sprays!</p>\r\n<ul>\r\n<li>Kosher Certified</li>\r\n<li>Organic Certified</li>\r\n<li>Small Business Certification</li>\r\n</ul>\r\n<h3>We pride ourselves in being a responsive supplier and we’ve built our reputation with quality products.</h3>', 'Food Service', '', 'publish', 'closed', 'closed', '', 'food-service', '', '', '2017-12-26 22:37:53', '2017-12-26 22:37:53', '', 13, 'http://cibaria-intl.press/?page_id=22', 0, 'page', '', 0),
(23, 1, '2017-12-26 22:37:53', '2017-12-26 22:37:53', ' <h2>Cooking with olive oil brings Fine Dining Flavor to the table.</h2>\r\n<p>Our Olive Oils give the taste of elegance to a wide variety of cuisines, making them a must-have item in all great kitchens! Whether it is menu development for a restaurant or product development for a food service manufacturer we have the product expertise to get the right flavor profile and appearance for all your culinary applications. In pursuit of a perfect pantry? Check out our specialty oils, blended oils, organic oils, flavored oils and aerosol oil sprays!</p>\r\n<ul>\r\n<li>Kosher Certified</li>\r\n<li>Organic Certified</li>\r\n<li>Small Business Certification</li>\r\n</ul>\r\n<h3>We pride ourselves in being a responsive supplier and we’ve built our reputation with quality products.</h3>', 'Food Service', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2017-12-26 22:37:53', '2017-12-26 22:37:53', '', 22, 'http://cibaria-intl.press/22-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2017-12-26 22:39:52', '2017-12-26 22:39:52', '<p>Cibaria offers a nice array of Retail Sizes that are available for your use. Various sizes are always stocked, so be sure to inquire within if you don\'t see a size that you\'re looking for. We bottle branded or private label containers that come in Tin, Glass, and P.E.T..</p>\r\n<h3>Cibaria can Custom Blend your Oils and Vinegars! Need a special Olive Oil blend? We\'ve got it covered. Our professional team will ensure your satisfaction down to the very last drop</h3>\r\n<p><strong>Retail Sizes Currently Available:</strong></p>\r\n<p><strong>Glass Bottles - Marasca</strong>\r\n</p><ul>\r\n<li>100 ml</li>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n<li>750 ml</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>Glass Bottles - Dorica</strong>\r\n</p><ul>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>Bertolli Style</strong>\r\n</p><ul>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>P.E.T. Square Style</strong>\r\n</p><ul>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n<li>1 lt</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>P.E.T.</strong>\r\n</p><ul>\r\n<li>64 oz</li>\r\n<li>2 lt</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>Square Tins</strong>\r\n</p><ul>\r\n<li>3 lt</li>\r\n<li>1 gal.</li>\r\n</ul>\r\n<p></p>', 'Retail Sizes', '', 'publish', 'closed', 'closed', '', 'retail-sizes', '', '', '2017-12-26 22:39:52', '2017-12-26 22:39:52', '', 13, 'http://cibaria-intl.press/?page_id=24', 0, 'page', '', 0),
(25, 1, '2017-12-26 22:39:52', '2017-12-26 22:39:52', '<p>Cibaria offers a nice array of Retail Sizes that are available for your use. Various sizes are always stocked, so be sure to inquire within if you don\'t see a size that you\'re looking for. We bottle branded or private label containers that come in Tin, Glass, and P.E.T..</p>\r\n<h3>Cibaria can Custom Blend your Oils and Vinegars! Need a special Olive Oil blend? We\'ve got it covered. Our professional team will ensure your satisfaction down to the very last drop</h3>\r\n<p><strong>Retail Sizes Currently Available:</strong></p>\r\n<p><strong>Glass Bottles - Marasca</strong>\r\n</p><ul>\r\n<li>100 ml</li>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n<li>750 ml</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>Glass Bottles - Dorica</strong>\r\n</p><ul>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>Bertolli Style</strong>\r\n</p><ul>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>P.E.T. Square Style</strong>\r\n</p><ul>\r\n<li>250 ml</li>\r\n<li>500 ml</li>\r\n<li>1 lt</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>P.E.T.</strong>\r\n</p><ul>\r\n<li>64 oz</li>\r\n<li>2 lt</li>\r\n</ul>\r\n<p></p>\r\n<p><strong>Square Tins</strong>\r\n</p><ul>\r\n<li>3 lt</li>\r\n<li>1 gal.</li>\r\n</ul>\r\n<p></p>', 'Retail Sizes', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2017-12-26 22:39:52', '2017-12-26 22:39:52', '', 24, 'http://cibaria-intl.press/24-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2017-12-26 22:42:13', '2017-12-26 22:42:13', '<p>Cibaria International, Inc. has a written quality management program which identifies and defines the policies and procedures for the operation and control of the site’s food safety and quality programs. The person with the responsibility for managing the overall program is Kathy Griset, company President and CEO. She does this with the help of Jose Rodriguez, Operations Manager; Josh Griset, Quality Control Manager and Charlotte Murphy, Compliance Manager as well as team members involved in managing the existing programs of HACCP and RECALL.</p>\r\n<p><strong>PROGRAMS:</strong></p>\r\n<ul>\r\n<li>HACCP</li>\r\n<li>Quality Management</li>\r\n<li>Recall</li>\r\n<li>Training</li>\r\n<li>Sanitation</li>\r\n<li>Bioterrorism</li>\r\n<li>Corrective Action/Hold and Release</li>\r\n<li>Pest Control Program</li>\r\n<li>Allergen Training</li>\r\n<li>Chemical Control</li>\r\n<li>GMP and SSOP Program</li>\r\n<li>Food Defense</li>\r\n<li>Third Party Audits</li>\r\n<li>Organic Certification</li>\r\n<li>Kosher Certification</li>\r\n</ul>\r\n<p><strong>DOCUMENTATION:</strong></p>\r\n<ul>\r\n<li>Lot Logs (electronic record)</li>\r\n<li>Specification Sheets (electronic record)</li>\r\n<li>Certificate of Analysis Sheets (electronic record and/or hardcopy)</li>\r\n<li>MSDS</li>\r\n<li>Records</li>\r\n<li>Traceability</li>\r\n</ul>\r\n<p>Each of our programs has a review period at least annually. These can be found in the individual books. Any time a change is proposed to any of our programs outside of this designated review period, the request for changes must be made in writing and approved prior to implementation. The approval must come from the individual or team overseeing that program.</p>', 'Quality Control', '', 'publish', 'closed', 'closed', '', 'quality-control', '', '', '2017-12-26 22:42:39', '2017-12-26 22:42:39', '', 0, 'http://cibaria-intl.press/?page_id=26', 0, 'page', '', 0),
(27, 1, '2017-12-26 22:42:13', '2017-12-26 22:42:13', '<p>Cibaria International, Inc. has a written quality management program which identifies and defines the policies and procedures for the operation and control of the site’s food safety and quality programs. The person with the responsibility for managing the overall program is Kathy Griset, company President and CEO. She does this with the help of Jose Rodriguez, Operations Manager; Josh Griset, Quality Control Manager and Charlotte Murphy, Compliance Manager as well as team members involved in managing the existing programs of HACCP and RECALL.</p>\r\n<p><strong>PROGRAMS:</strong></p>\r\n<ul>\r\n<li>HACCP</li>\r\n<li>Quality Management</li>\r\n<li>Recall</li>\r\n<li>Training</li>\r\n<li>Sanitation</li>\r\n<li>Bioterrorism</li>\r\n<li>Corrective Action/Hold and Release</li>\r\n<li>Pest Control Program</li>\r\n<li>Allergen Training</li>\r\n<li>Chemical Control</li>\r\n<li>GMP and SSOP Program</li>\r\n<li>Food Defense</li>\r\n<li>Third Party Audits</li>\r\n<li>Organic Certification</li>\r\n<li>Kosher Certification</li>\r\n</ul>\r\n<p><strong>DOCUMENTATION:</strong></p>\r\n<ul>\r\n<li>Lot Logs (electronic record)</li>\r\n<li>Specification Sheets (electronic record)</li>\r\n<li>Certificate of Analysis Sheets (electronic record and/or hardcopy)</li>\r\n<li>MSDS</li>\r\n<li>Records</li>\r\n<li>Traceability</li>\r\n</ul>\r\n<p>Each of our programs has a review period at least annually. These can be found in the individual books. Any time a change is proposed to any of our programs outside of this designated review period, the request for changes must be made in writing and approved prior to implementation. The approval must come from the individual or team overseeing that program.</p>', 'Quality Control', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2017-12-26 22:42:13', '2017-12-26 22:42:13', '', 26, 'http://cibaria-intl.press/26-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2017-12-26 22:43:54', '2017-12-26 22:43:54', '<p>Cibaria International Inc. is committed to protect our products and materials from tampering or other malicious, criminal, or terrorist actions. Weekly Operations meetings with management and staff are a part of Cibaria’s regular food security program.</p>\r\n<p>The company has assessed our vulnerability with regard to our products, our facility and our community and has instituted procedures to minimize the risks of contamination of our products.</p>\r\n<p><strong>These include:</strong></p>\r\n<ul>\r\n<li>Monitoring of who enters the facility</li>\r\n<li>Infrared camera system monitoring the inside and outside of the facility 24 hours per day</li>\r\n<li>Background checks of all new employees</li>\r\n<li>Potable water tests</li>\r\n<li>Vendor approval program</li>\r\n<li>Inspection and approval of all incoming shipments</li>\r\n<li>Protection of vital records</li>\r\n<li>Analysis of incoming products</li>\r\n<li>Mock trainings</li>\r\n</ul>\r\n<p>The Risk Team meets regularly to access our program and insure that Cibaria’s security program is active and working as planned. Emergency planning has become a part of our corporate culture.</p>', 'Bioterrorism Program', '', 'publish', 'closed', 'closed', '', 'bioterrorism-program', '', '', '2017-12-26 22:43:54', '2017-12-26 22:43:54', '', 26, 'http://cibaria-intl.press/?page_id=28', 0, 'page', '', 0),
(29, 1, '2017-12-26 22:43:54', '2017-12-26 22:43:54', '<p>Cibaria International Inc. is committed to protect our products and materials from tampering or other malicious, criminal, or terrorist actions. Weekly Operations meetings with management and staff are a part of Cibaria’s regular food security program.</p>\r\n<p>The company has assessed our vulnerability with regard to our products, our facility and our community and has instituted procedures to minimize the risks of contamination of our products.</p>\r\n<p><strong>These include:</strong></p>\r\n<ul>\r\n<li>Monitoring of who enters the facility</li>\r\n<li>Infrared camera system monitoring the inside and outside of the facility 24 hours per day</li>\r\n<li>Background checks of all new employees</li>\r\n<li>Potable water tests</li>\r\n<li>Vendor approval program</li>\r\n<li>Inspection and approval of all incoming shipments</li>\r\n<li>Protection of vital records</li>\r\n<li>Analysis of incoming products</li>\r\n<li>Mock trainings</li>\r\n</ul>\r\n<p>The Risk Team meets regularly to access our program and insure that Cibaria’s security program is active and working as planned. Emergency planning has become a part of our corporate culture.</p>', 'Bioterrorism Program', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2017-12-26 22:43:54', '2017-12-26 22:43:54', '', 28, 'http://cibaria-intl.press/28-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2017-12-26 22:46:29', '2017-12-26 22:46:29', 'Cibaria International, Inc. is proudly HACCP Certified. To inquire further about our HACCP Program and policies, please contact our Compliance Manager for more information.\r\n<p style="text-align: center;"><img class="aligncenter size-full wp-image-90" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/haccp.jpg" alt="" width="140" height="132" /></p>', 'Haccp', '', 'publish', 'closed', 'closed', '', 'haccp', '', '', '2018-02-07 16:33:03', '2018-02-07 16:33:03', '', 26, 'http://cibaria-intl.press/?page_id=30', 0, 'page', '', 0),
(31, 1, '2017-12-26 22:46:29', '2017-12-26 22:46:29', 'Cibaria International, Inc. is proudly HACCP Certified. To inquire further about our HACCP Program and policies, please contact our Compliance Manager for more information.\r\n\r\n<img class="aligncenter size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/haccp.jpg" alt="HACCP Program" width="140" height="132" />', 'Haccp', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2017-12-26 22:46:29', '2017-12-26 22:46:29', '', 30, 'http://cibaria-intl.press/30-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2017-12-26 22:47:12', '2017-12-26 22:47:12', 'Cibaria International, Inc. is proudly HACCP Certified. To inquire further about our HACCP Program and policies, please contact our Compliance Manager for more information.\r\n\r\n<img class="size-medium aligncenter" src="http://www.cibaria-intl.com/themes/main/assets/img/haccp.jpg" alt="HACCP Program" width="140" height="132" />', 'Haccp', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2017-12-26 22:47:12', '2017-12-26 22:47:12', '', 30, 'http://cibaria-intl.press/30-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2017-12-26 22:47:32', '2017-12-26 22:47:32', '<p>Cibaria International, Inc. is proudly HACCP Certified. To inquire further about our HACCP Program and policies, please contact our Compliance Manager for more information.</p>\r\n\r\n<img class="size-medium aligncenter" src="http://www.cibaria-intl.com/themes/main/assets/img/haccp.jpg" alt="HACCP Program" width="140" height="132" />', 'Haccp', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2017-12-26 22:47:32', '2017-12-26 22:47:32', '', 30, 'http://cibaria-intl.press/30-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2017-12-26 22:48:17', '2017-12-26 22:48:17', '<p>Cibaria International, Inc. is proudly HACCP Certified. To inquire further about our HACCP Program and policies, please contact our Compliance Manager for more information.</p>\r\n\r\n<p style="text-align:center;"><img class="size-medium aligncenter" src="http://www.cibaria-intl.com/themes/main/assets/img/haccp.jpg" alt="HACCP Program" width="140" height="132" /></p>', 'Haccp', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2017-12-26 22:48:17', '2017-12-26 22:48:17', '', 30, 'http://cibaria-intl.press/30-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2017-12-26 22:49:40', '2017-12-26 22:49:40', '<h2>Cibaria International, Inc, SILLIKER GOLD RECIPIENT</h2>\r\nCibaria International, Inc. takes great pride in it\'s clean facility, outstanding presentation, and overall product quality. We\'re on a mission to be the very best at what we do. With the help of our trusted third party auditors, we are able to continually monitor our progress as a food processor, and spot areas that could be improved. Cibaria\'s last third party audit was in December of 2010. We consider this audit a great success, and will continue to strive for excellence!\r\n<p style="text-align: center;"><img class="aligncenter size-full wp-image-84" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/siliker.jpg" alt="" width="235" height="59" /></p>', 'Third Party Audits', '', 'publish', 'closed', 'closed', '', 'third-party-audits', '', '', '2018-02-07 16:33:47', '2018-02-07 16:33:47', '', 26, 'http://cibaria-intl.press/?page_id=35', 0, 'page', '', 0),
(36, 1, '2017-12-26 22:49:40', '2017-12-26 22:49:40', '<h2>Cibaria International, Inc, SILLIKER GOLD RECIPIENT</h2>\r\nCibaria International, Inc. takes great pride in it\'s clean facility, outstanding presentation, and overall product quality. We\'re on a mission to be the very best at what we do. With the help of our trusted third party auditors, we are able to continually monitor our progress as a food processor, and spot areas that could be improved. Cibaria\'s last third party audit was in December of 2010. We consider this audit a great success, and will continue to strive for excellence!', 'Third Party Audits', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2017-12-26 22:49:40', '2017-12-26 22:49:40', '', 35, 'http://cibaria-intl.press/35-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2017-12-26 22:51:11', '2017-12-26 22:51:11', '<h2>Cibaria International, Inc, SILLIKER GOLD RECIPIENT</h2>\r\nCibaria International, Inc. takes great pride in it\'s clean facility, outstanding presentation, and overall product quality. We\'re on a mission to be the very best at what we do. With the help of our trusted third party auditors, we are able to continually monitor our progress as a food processor, and spot areas that could be improved. Cibaria\'s last third party audit was in December of 2010. We consider this audit a great success, and will continue to strive for excellence!\r\n\r\n<p style="text-align:center;"><img class="aligncenter size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Silliker Third Party Audit" width="235" height="59" /></p>', 'Third Party Audits', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2017-12-26 22:51:11', '2017-12-26 22:51:11', '', 35, 'http://cibaria-intl.press/35-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2017-12-26 23:15:05', '2017-12-26 23:15:05', ' <p>We are Cibaria International, Inc. are committed to offering products that are healthy and safe. Therefore, we have instituted the following programs to ensure that we maintain security and safe practices:</p>\r\n<p><strong>1. GMP Program In Place</strong> <br> <strong>2. SSOP Program In Place</strong> <br> <strong>3. HACCP Program in Place with prerequisite programs that include:</strong></p>\r\n<ul>\r\n<li>a. Random inspections performed by the operations manager and reviewed by the company president.</li>\r\n<li>b. Inspections of all incoming and outgoing shipments by our quality control manager.</li>\r\n<li>c. COA\'s required on all ingredients with random verification of information.</li>\r\n</ul>\r\n<p><strong>4. Registration with the FDA</strong> <br> <strong>5. Registration confirmation with all suppliers</strong> <br> <strong>6. Development of program for prevention of Bioterrorism in Food Products which includes:</strong></p>\r\n<ul>\r\n<li>a. Background checks on all new employees</li>\r\n<li>b. Shipments made by approved trucking companies and in locked vehicles.</li>\r\n<li>c. Approval system for all new vendors</li>\r\n<li>d. Continuing quarterly meetings of a committee to review all policies and procedures.</li>\r\n<li>e. Security sytem including sign-in of visitors and truckers, docks and door security and motion sensitive cameras</li>\r\n<li>f. Potable water test</li>\r\n</ul>', 'GMP SSOP', '', 'publish', 'closed', 'closed', '', 'gmp-ssop', '', '', '2017-12-26 23:15:05', '2017-12-26 23:15:05', '', 26, 'http://cibaria-intl.press/?page_id=38', 0, 'page', '', 0),
(39, 1, '2017-12-26 23:15:05', '2017-12-26 23:15:05', ' <p>We are Cibaria International, Inc. are committed to offering products that are healthy and safe. Therefore, we have instituted the following programs to ensure that we maintain security and safe practices:</p>\r\n<p><strong>1. GMP Program In Place</strong> <br> <strong>2. SSOP Program In Place</strong> <br> <strong>3. HACCP Program in Place with prerequisite programs that include:</strong></p>\r\n<ul>\r\n<li>a. Random inspections performed by the operations manager and reviewed by the company president.</li>\r\n<li>b. Inspections of all incoming and outgoing shipments by our quality control manager.</li>\r\n<li>c. COA\'s required on all ingredients with random verification of information.</li>\r\n</ul>\r\n<p><strong>4. Registration with the FDA</strong> <br> <strong>5. Registration confirmation with all suppliers</strong> <br> <strong>6. Development of program for prevention of Bioterrorism in Food Products which includes:</strong></p>\r\n<ul>\r\n<li>a. Background checks on all new employees</li>\r\n<li>b. Shipments made by approved trucking companies and in locked vehicles.</li>\r\n<li>c. Approval system for all new vendors</li>\r\n<li>d. Continuing quarterly meetings of a committee to review all policies and procedures.</li>\r\n<li>e. Security sytem including sign-in of visitors and truckers, docks and door security and motion sensitive cameras</li>\r\n<li>f. Potable water test</li>\r\n</ul>', 'GMP SSOP', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2017-12-26 23:15:05', '2017-12-26 23:15:05', '', 38, 'http://cibaria-intl.press/38-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2017-12-26 23:20:08', '2017-12-26 23:20:08', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-full wp-image-78" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/afoa.png" alt="" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-full wp-image-79" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/ou.jpg" alt="" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium wp-image-80" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/sf-300x166.jpg" alt="" width="300" height="166" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-full wp-image-81" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/usda.jpg" alt="" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium wp-image-82" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/aocs-300x97.jpg" alt="" width="300" height="97" /></td>\r\n<td align="center"><img class="alignnone size-full wp-image-83" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/uschamber.jpg" alt="" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-full wp-image-84" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/siliker.jpg" alt="" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-full wp-image-85" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/wbenc.jpg" alt="" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" align="center"><img class="alignnone size-medium wp-image-86" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/org-300x256.jpg" alt="" width="300" height="256" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium wp-image-87" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/plma-300x236.jpg" alt="" width="300" height="236" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'publish', 'closed', 'closed', '', 'certifications', '', '', '2018-02-07 16:27:34', '2018-02-07 16:27:34', '', 0, 'http://cibaria-intl.press/?page_id=40', 0, 'page', '', 0),
(41, 1, '2017-12-26 23:20:08', '2017-12-26 23:20:08', '<div class="col-md-12">\r\n            <div class="page-layout">\r\n                <p></p><div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n    <tr>\r\n        <td align="center"><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/afoa.png" alt="AFOA Certification" class="img-responsive"></td>\r\n        <td align="center"><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/ou.jpg" alt="Orthodox Union Certification" class="img-responsive"></td>\r\n        <td><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/sf.jpg" alt="Specialty Food Association" class="img-responsive"></td>\r\n    </tr>\r\n    <tr>\r\n        <td><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/usda.jpg" alt="USDA Certification" class="img-responsive"></td>\r\n        <td colspan="2"><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/aocs.jpg" alt="AOCS Certification" class="img-responsive"></td>\r\n        <td align="center"><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/uschamber.jpg" alt="US Chamber of Commerce" class="img-responsive"></td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="2" align="center"><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Siliker" class="img-responsive"></td>\r\n        <td colspan="2" align="center"><img src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/wbenc.jpg" alt="WBENC Certification" class="img-responsive"></td>\r\n    </tr>\r\n    <tr>\r\n      <td colspan="4"><p style="text-align: center;"><img src="http://www.cibaria-intl.com/themes/main/assets/img/org.jpg" alt="Organic Certifiers" class="img-responsive"></p></td>\r\n    </tr>\r\n    <tr>\r\n        <td colspan="4">\r\n            <p style="text-align: center;">\r\n            <img src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA Certification" class="img-responsive">\r\n            </p>\r\n        </td>\r\n    </tr>\r\n</tbody></table>\r\n</div>\r\n</div>\r\n        </div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2017-12-26 23:20:08', '2017-12-26 23:20:08', '', 40, 'http://cibaria-intl.press/40-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2018-02-07 16:27:18', '2018-02-07 16:27:18', '<div class="col-md-12">\n<div class="page-layout">\n<div class="table-responsive">\n<table class="table table-bordered">\n<tbody>\n<tr>\n<td align="center"><img class="alignnone size-full wp-image-78" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/afoa.png" alt="" width="169" height="165" /></td>\n<td align="center"><img class="alignnone size-full wp-image-79" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/ou.jpg" alt="" width="169" height="169" /></td>\n<td><img class="alignnone size-medium wp-image-80" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/sf-300x166.jpg" alt="" width="300" height="166" /></td>\n</tr>\n<tr>\n<td><img class="alignnone size-full wp-image-81" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/usda.jpg" alt="" width="169" height="169" /></td>\n<td colspan="2"><img class="alignnone size-medium wp-image-82" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/aocs-300x97.jpg" alt="" width="300" height="97" /></td>\n<td align="center"><img class="alignnone size-full wp-image-83" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/uschamber.jpg" alt="" width="169" height="169" /></td>\n</tr>\n<tr>\n<td colspan="2" align="center"><img class="alignnone size-full wp-image-84" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/siliker.jpg" alt="" width="235" height="59" /></td>\n<td colspan="2" align="center"><img class="alignnone size-full wp-image-85" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/wbenc.jpg" alt="" width="136" height="59" /></td>\n</tr>\n<tr>\n<td colspan="4" align="center">&nbsp;</td>\n</tr>\n<tr>\n<td colspan="4">\n<p style="text-align: center;"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" /></p>\n</td>\n</tr>\n</tbody>\n</table>\n</div>\n</div>\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-autosave-v1', '', '', '2018-02-07 16:27:18', '2018-02-07 16:27:18', '', 40, 'http://cibaria-intl.press/40-autosave-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(43, 1, '2017-12-26 23:24:55', '2017-12-26 23:24:55', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/afoa.png" alt="AFOA" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/ou.jpg" alt="OU" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/sf.jpg" alt="Specialty Food Association" width="306" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/usda.jpg" alt="USDA Organic" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/aocs.jpg" alt="AOCS" width="465" height="150" /></td>\r\n<td align="center"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/uschamber.jpg" alt="Chamber of Commerce" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Silliker" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/wbenc.jpg" alt="WBENC" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/org.jpg" alt="Certified Organic" width="314" height="268" /></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2017-12-26 23:24:55', '2017-12-26 23:24:55', '', 40, 'http://cibaria-intl.press/40-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2017-12-26 23:26:16', '2017-12-26 23:26:16', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/afoa.png" alt="AFOA" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/ou.jpg" alt="OU" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/sf.jpg" alt="Specialty Food Association" width="306" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/usda.jpg" alt="USDA Organic" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/aocs.jpg" alt="AOCS" width="465" height="150" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/uschamber.jpg" alt="Chamber of Commerce" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Silliker" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/wbenc.jpg" alt="WBENC" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/org.jpg" alt="Certified Organic" width="314" height="268" /></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2017-12-26 23:26:16', '2017-12-26 23:26:16', '', 40, 'http://cibaria-intl.press/40-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2017-12-26 23:26:50', '2017-12-26 23:26:50', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/afoa.png" alt="AFOA" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/ou.jpg" alt="OU" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/sf.jpg" alt="Specialty Food Association" width="306" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/usda.jpg" alt="USDA Organic" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/aocs.jpg" alt="AOCS" width="465" height="150" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/uschamber.jpg" alt="Chamber of Commerce" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Silliker" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/wbenc.jpg" alt="WBENC" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="aligncenter size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/org.jpg" alt="Certified Organic" width="314" height="268" /></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2017-12-26 23:26:50', '2017-12-26 23:26:50', '', 40, 'http://cibaria-intl.press/40-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2017-12-26 23:27:34', '2017-12-26 23:27:34', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/afoa.png" alt="AFOA" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/ou.jpg" alt="OU" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/sf.jpg" alt="Specialty Food Association" width="306" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/usda.jpg" alt="USDA Organic" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/aocs.jpg" alt="AOCS" width="465" height="150" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/uschamber.jpg" alt="Chamber of Commerce" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Silliker" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/wbenc.jpg" alt="WBENC" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" align="center">\r\n<p style="text-align: center;"><img class="aligncenter size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/org.jpg" alt="Certified Organic" width="314" height="268" /></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2017-12-26 23:27:34', '2017-12-26 23:27:34', '', 40, 'http://cibaria-intl.press/40-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2017-12-26 23:28:22', '2017-12-26 23:28:22', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/afoa.png" alt="AFOA" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/ou.jpg" alt="OU" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/sf.jpg" alt="Specialty Food Association" width="306" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/usda.jpg" alt="USDA Organic" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/aocs.jpg" alt="AOCS" width="465" height="150" /></td>\r\n<td align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/uschamber.jpg" alt="Chamber of Commerce" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/siliker.jpg" alt="Silliker" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/certifications/wbenc.jpg" alt="WBENC" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" align="center">\r\n<p><img class="aligncenter size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/org.jpg" alt="Certified Organic" width="314" height="268" /></p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium img-responsive" src="http://www.cibaria-intl.com/themes/main/assets/img/plma.jpg" alt="PLMA" width="820" height="644" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2017-12-26 23:28:22', '2017-12-26 23:28:22', '', 40, 'http://cibaria-intl.press/40-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-01-22 23:32:46', '2018-01-22 23:32:46', '<h2>Product you can trust, service you can count on, and unparalleled dedication to being the best that we can be</h2>\r\n<p>Located in Southern California, Cibaria International has long been a leading supplier of <a href="http://www.cibariastoresupply.com/shop/olive-oils.html" title="wholesale olive oils">wholesale olive oils</a> and <a href="http://www.cibariastoresupply.com/shop/wholesale-vinegars.html" title="wholesale balsamic vinegars">wholesale balsamic vinegars</a> to every segment of the food industry. Kathy Griset, our founder, has traveled the world procuring the very finest products from some of the oldest and the most traditional olive oil and balsamic vinegar producing families in the Mediterranean, making them readily available to our customers.</p>\r\n<p>From there, it’s been a natural expansion into Specialty Oil, Certified Organic Oils, Non GMO Oils, Vegetable Oils, Aerosol Oils, and <a href="http://www.cibariasoapsupply.com/shop/" title="soap making oils">Soap Making Oils</a>. Our growth continues to be fueled, in large part, by our customer’s continued interest in both the quality of products being brought to the market place and cost effectiveness. We are committed to constantly improving both, working hard to build strong partnerships with our customers and suppliers.</p>\r\n<p>Our brand new 55,000 square foot manufacturing facility is a testament to our dedication to continue to be better, do better, and provide the best products available. We are thrilled to bring our products online and hope you will be too.</p>', 'About Cibaria International', '', 'publish', 'closed', 'closed', '', 'about', '', '', '2018-01-23 19:19:12', '2018-01-23 19:19:12', '', 0, 'http://cibaria-intl.press/?page_id=49', 0, 'page', '', 0),
(50, 1, '2018-01-22 23:32:46', '2018-01-22 23:32:46', '<h1 style="color:#999779;border-bottom:1px solid lightgrey;">About Cibaria International</h1>\r\n<h2>Product you can trust, service you can count on, and unparalleled dedication to being the best that we can be</h2>\r\n<p>Located in Southern California, Cibaria International has long been a leading supplier of <a href="http://www.cibariastoresupply.com/shop/olive-oils.html" title="wholesale olive oils">wholesale olive oils</a> and <a href="http://www.cibariastoresupply.com/shop/wholesale-vinegars.html" title="wholesale balsamic vinegars">wholesale balsamic vinegars</a> to every segment of the food industry. Kathy Griset, our founder, has traveled the world procuring the very finest products from some of the oldest and the most traditional olive oil and balsamic vinegar producing families in the Mediterranean, making them readily available to our customers.</p>\r\n<p>From there, it’s been a natural expansion into Specialty Oil, Certified Organic Oils, Non GMO Oils, Vegetable Oils, Aerosol Oils, and <a href="http://www.cibariasoapsupply.com/shop/" title="soap making oils">Soap Making Oils</a>. Our growth continues to be fueled, in large part, by our customer’s continued interest in both the quality of products being brought to the market place and cost effectiveness. We are committed to constantly improving both, working hard to build strong partnerships with our customers and suppliers.</p>\r\n<p>Our brand new 55,000 square foot manufacturing facility is a testament to our dedication to continue to be better, do better, and provide the best products available. We are thrilled to bring our products online and hope you will be too.</p>', 'About Cibaria International', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-01-22 23:32:46', '2018-01-22 23:32:46', '', 49, 'http://cibaria-intl.press/49-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2018-01-23 15:08:11', '2018-01-23 15:08:11', '[specsheets]', 'Specsheets', '', 'publish', 'closed', 'closed', '', 'specsheets', '', '', '2018-01-23 15:21:37', '2018-01-23 15:21:37', '', 0, 'http://cibaria-intl.press/?page_id=51', 0, 'page', '', 0),
(52, 1, '2018-01-23 15:08:11', '2018-01-23 15:08:11', '[specsheets]', 'Specsheets', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2018-01-23 15:08:11', '2018-01-23 15:08:11', '', 51, 'http://cibaria-intl.press/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2018-01-23 19:19:12', '2018-01-23 19:19:12', '<h2>Product you can trust, service you can count on, and unparalleled dedication to being the best that we can be</h2>\r\n<p>Located in Southern California, Cibaria International has long been a leading supplier of <a href="http://www.cibariastoresupply.com/shop/olive-oils.html" title="wholesale olive oils">wholesale olive oils</a> and <a href="http://www.cibariastoresupply.com/shop/wholesale-vinegars.html" title="wholesale balsamic vinegars">wholesale balsamic vinegars</a> to every segment of the food industry. Kathy Griset, our founder, has traveled the world procuring the very finest products from some of the oldest and the most traditional olive oil and balsamic vinegar producing families in the Mediterranean, making them readily available to our customers.</p>\r\n<p>From there, it’s been a natural expansion into Specialty Oil, Certified Organic Oils, Non GMO Oils, Vegetable Oils, Aerosol Oils, and <a href="http://www.cibariasoapsupply.com/shop/" title="soap making oils">Soap Making Oils</a>. Our growth continues to be fueled, in large part, by our customer’s continued interest in both the quality of products being brought to the market place and cost effectiveness. We are committed to constantly improving both, working hard to build strong partnerships with our customers and suppliers.</p>\r\n<p>Our brand new 55,000 square foot manufacturing facility is a testament to our dedication to continue to be better, do better, and provide the best products available. We are thrilled to bring our products online and hope you will be too.</p>', 'About Cibaria International', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2018-01-23 19:19:12', '2018-01-23 19:19:12', '', 49, 'http://cibaria-intl.press/49-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-01-23 19:52:12', '2018-01-23 19:52:12', '<div class="row">\r\n    <div class="col-md-4">\r\n        <iframe width="888" height="315" src="https://www.youtube.com/embed/rfKDuun6oak" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div class="row">\r\n\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/dRT8_C4TCzA" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n\r\n<div class="col-md-4">\r\n<iframe width="290" height="290" src="https://www.youtube.com/embed/zf2zM_zJhNY" frameborder="0" allowfullscreen=""></iframe>\r\n</div>\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/5DcFl3oYu-g" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n\r\n    \r\n</div>\r\n<!--row 2-->\r\n<div class="row">\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/QOHIZNaG16Q" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n<div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/5dLIrPL9m8E" frameborder="0" allowfullscreen=""></iframe>    </div>\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/XfMDFt0OPA4" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n\r\n    \r\n</div>\r\n<!--row 3-->\r\n<div class="row">\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/wXEoTitmdsg" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n<div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/aQO5Tm9RQj0" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/KWddATnVtrU" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n</div>  ', 'Videos', '', 'publish', 'closed', 'closed', '', 'videos', '', '', '2018-01-23 19:52:29', '2018-01-23 19:52:29', '', 0, 'http://cibaria-intl.press/?page_id=54', 0, 'page', '', 0),
(55, 1, '2018-01-23 19:52:12', '2018-01-23 19:52:12', '<div class="row">\r\n    <div class="col-md-4">\r\n        <iframe width="888" height="315" src="https://www.youtube.com/embed/rfKDuun6oak" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div class="row">\r\n\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/dRT8_C4TCzA" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n\r\n<div class="col-md-4">\r\n<iframe width="290" height="290" src="https://www.youtube.com/embed/zf2zM_zJhNY" frameborder="0" allowfullscreen=""></iframe>\r\n</div>\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/5DcFl3oYu-g" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n\r\n    \r\n</div>\r\n<!--row 2-->\r\n<div class="row">\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/QOHIZNaG16Q" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n<div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/5dLIrPL9m8E" frameborder="0" allowfullscreen=""></iframe>    </div>\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/XfMDFt0OPA4" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n\r\n    \r\n</div>\r\n<!--row 3-->\r\n<div class="row">\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/wXEoTitmdsg" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n<div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/aQO5Tm9RQj0" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n    <div class="col-md-4">\r\n        <iframe width="290" height="290" src="https://www.youtube.com/embed/KWddATnVtrU" frameborder="0" allowfullscreen=""></iframe>\r\n    </div>\r\n</div>  ', 'Videos', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-01-23 19:52:12', '2018-01-23 19:52:12', '', 54, 'http://cibaria-intl.press/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2018-01-23 20:00:40', '2018-01-23 20:00:40', '<p>This is the WPForms preview page. All your form previews will be handled on this page.</p><p>The page is set to private, so it is not publicly accessible. Please do not delete this page :) .</p>', 'WPForms Preview', '', 'private', 'closed', 'closed', '', 'wpforms-preview', '', '', '2018-01-23 20:00:40', '2018-01-23 20:00:40', '', 0, 'http://cibaria-intl.press/wpforms-preview/', 0, 'page', '', 0),
(57, 1, '2018-01-23 20:01:07', '2018-01-23 20:01:07', '{"id":"57","field_id":9,"fields":{"3":{"id":"3","type":"name","label":"Your Name","format":"simple","description":"","required":"1","size":"medium","simple_placeholder":"Enter Name","simple_default":"","first_placeholder":"","first_default":"","middle_placeholder":"","middle_default":"","last_placeholder":"","last_default":"","css":"form-control"},"4":{"id":"4","type":"name","label":"Your Company","format":"simple","description":"","required":"1","size":"medium","simple_placeholder":"Enter Company Name","simple_default":"","first_placeholder":"","first_default":"","middle_placeholder":"","middle_default":"","last_placeholder":"","last_default":"","css":"form-control"},"5":{"id":"5","type":"name","label":"Your Address","format":"simple","description":"","required":"1","size":"medium","simple_placeholder":"Enter Address","simple_default":"","first_placeholder":"","first_default":"","middle_placeholder":"","middle_default":"","last_placeholder":"","last_default":"","css":"form-control"},"6":{"id":"6","type":"number","label":"Your Telephone Number","description":"","size":"medium","placeholder":"Enter Telephone","default_value":"","css":"form-control"},"1":{"id":"1","type":"email","label":"Your Email","description":"","required":"1","size":"medium","placeholder":"Enter Email","confirmation_placeholder":"","default_value":"","css":"form-control"},"2":{"id":"2","type":"textarea","label":"Products Needed","description":"","size":"medium","placeholder":"Enter Products","css":"form-control"},"7":{"id":"7","type":"textarea","label":"Estimated Monthly Income","description":"","size":"medium","placeholder":"Estimated Monthly Income","css":"form-control"},"8":{"id":"8","type":"textarea","label":"Special Needs or Instructions","description":"","size":"medium","placeholder":"Special Needs or Instructions","css":"form-control"}},"settings":{"form_title":"Bulk Quotes","form_desc":"","form_class":"","submit_text":"Submit","submit_text_processing":"Sending...","submit_class":"","honeypot":"1","notification_enable":"1","notifications":{"1":{"email":"{admin_email}","subject":"New Entry: Bulk Quotes","sender_name":"{field_id=\\"0\\"}","sender_address":"{admin_email}","replyto":"{field_id=\\"1\\"}","message":"{all_fields}"}},"confirmation_type":"message","confirmation_message":"Thanks for contacting us! We will be in touch with you shortly.","confirmation_message_scroll":"1","confirmation_page":"49","confirmation_redirect":""},"meta":{"template":"contact"}}', 'Bulk Quotes', '', 'publish', 'closed', 'closed', '', 'bulk-quotes', '', '', '2018-01-23 20:08:20', '2018-01-23 20:08:20', '', 0, 'http://cibaria-intl.press/?post_type=wpforms&#038;p=57', 0, 'wpforms', '', 0),
(58, 1, '2018-01-23 20:19:03', '2018-01-23 20:19:03', '[bulk-quotes]', 'Bulk Quotes', '', 'publish', 'closed', 'closed', '', 'bulk-quotes', '', '', '2018-01-23 20:19:19', '2018-01-23 20:19:19', '', 0, 'http://cibaria-intl.press/?page_id=58', 0, 'page', '', 0),
(59, 1, '2018-01-23 20:19:03', '2018-01-23 20:19:03', '[bulk-quotes]', 'Bulk Quotes', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2018-01-23 20:19:03', '2018-01-23 20:19:03', '', 58, 'http://cibaria-intl.press/58-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2018-01-23 21:16:15', '2018-01-23 21:16:15', '<p>Thank you. A member of our staff will contact you regarding your Bulk Request. </p>', 'Thank you', '', 'publish', 'closed', 'closed', '', 'thank-you', '', '', '2018-01-23 21:19:38', '2018-01-23 21:19:38', '', 0, 'http://cibaria-intl.press/?page_id=60', 0, 'page', '', 0),
(61, 1, '2018-01-23 21:16:15', '2018-01-23 21:16:15', '<p>Thank you. A member of our staff will contact you regarding your Bulk Request. You will be redirected to the homepage in a few seconds. </p>', 'Thank you', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2018-01-23 21:16:15', '2018-01-23 21:16:15', '', 60, 'http://cibaria-intl.press/60-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2018-01-23 21:19:38', '2018-01-23 21:19:38', '<p>Thank you. A member of our staff will contact you regarding your Bulk Request. </p>', 'Thank you', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2018-01-23 21:19:38', '2018-01-23 21:19:38', '', 60, 'http://cibaria-intl.press/60-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2018-01-23 21:39:59', '2018-01-23 21:39:59', '[cibaria-contact]', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2018-01-23 21:40:16', '2018-01-23 21:40:16', '', 0, 'http://cibaria-intl.press/?page_id=63', 0, 'page', '', 0),
(64, 1, '2018-01-23 21:39:59', '2018-01-23 21:39:59', '[cibaria-contact]', 'Contact', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2018-01-23 21:39:59', '2018-01-23 21:39:59', '', 63, 'http://cibaria-intl.press/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2018-01-23 21:50:26', '2018-01-23 21:50:26', '[display-posts posts_per_page="10" order="DESC"]', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-01-23 21:51:46', '2018-01-23 21:51:46', '', 0, 'http://cibaria-intl.press/?page_id=65', 0, 'page', '', 0),
(66, 1, '2018-01-23 21:50:26', '2018-01-23 21:50:26', '[display-posts posts_per_page="1000" order="DESC"]', 'Blog', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-01-23 21:50:26', '2018-01-23 21:50:26', '', 65, 'http://cibaria-intl.press/65-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2018-01-23 21:51:46', '2018-01-23 21:51:46', '[display-posts posts_per_page="10" order="DESC"]', 'Blog', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2018-01-23 21:51:46', '2018-01-23 21:51:46', '', 65, 'http://cibaria-intl.press/65-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-01-23 21:53:35', '2018-01-23 21:53:35', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-01-23 21:53:35', '2018-01-23 21:53:35', '', 1, 'http://cibaria-intl.press/1-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2018-01-23 21:54:27', '2018-01-23 21:54:27', '<p>Visit us at the 2018 Winter Fancy Food Show on January 21-23 2018 at booth #1678. For more information </p>', '2018 Winter Fancy Food Show', '', 'publish', 'open', 'open', '', '2018-winter-fancy-food-show', '', '', '2018-01-23 21:54:27', '2018-01-23 21:54:27', '', 0, 'http://cibaria-intl.press/?p=69', 0, 'post', '', 0),
(70, 1, '2018-01-23 21:54:27', '2018-01-23 21:54:27', '<p>Visit us at the 2018 Winter Fancy Food Show on January 21-23 2018 at booth #1678. For more information </p>', '2018 Winter Fancy Food Show', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2018-01-23 21:54:27', '2018-01-23 21:54:27', '', 69, 'http://cibaria-intl.press/69-revision-v1/', 0, 'revision', '', 0),
(71, 1, '2018-01-23 21:58:04', '2018-01-23 21:58:04', '{\n    "blogdescription": {\n        "value": "",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-01-23 21:58:04"\n    },\n    "page_for_posts": {\n        "value": "0",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-01-23 21:58:04"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'b62c7836-ccc9-4257-a34b-afd725a8f131', '', '', '2018-01-23 21:58:04', '2018-01-23 21:58:04', '', 0, 'http://cibaria-intl.press/b62c7836-ccc9-4257-a34b-afd725a8f131/', 0, 'customize_changeset', '', 0),
(72, 1, '2018-01-23 22:00:48', '2018-01-23 22:00:48', '{\n    "page_for_posts": {\n        "value": "65",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-01-23 22:00:48"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'df6d7525-297d-490f-9910-5f9b938fef1c', '', '', '2018-01-23 22:00:48', '2018-01-23 22:00:48', '', 0, 'http://cibaria-intl.press/df6d7525-297d-490f-9910-5f9b938fef1c/', 0, 'customize_changeset', '', 0),
(73, 1, '2018-01-23 22:01:27', '2018-01-23 22:01:27', '{\n    "page_for_posts": {\n        "value": "0",\n        "type": "option",\n        "user_id": 1,\n        "date_modified_gmt": "2018-01-23 22:01:27"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '1eb5771a-76c1-4f04-af56-7d50ccdc0f94', '', '', '2018-01-23 22:01:27', '2018-01-23 22:01:27', '', 0, 'http://cibaria-intl.press/1eb5771a-76c1-4f04-af56-7d50ccdc0f94/', 0, 'customize_changeset', '', 0),
(74, 1, '2018-02-02 21:56:13', '2018-02-02 21:56:13', '[coas]', 'Certificate of Authority (COA)', '', 'publish', 'closed', 'closed', '', 'coas', '', '', '2018-02-02 22:05:53', '2018-02-02 22:05:53', '', 0, 'http://cibaria-intl.press/?page_id=74', 0, 'page', '', 0),
(75, 1, '2018-02-02 21:56:13', '2018-02-02 21:56:13', '[coas]', 'COA', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2018-02-02 21:56:13', '2018-02-02 21:56:13', '', 74, 'http://cibaria-intl.press/74-revision-v1/', 0, 'revision', '', 0),
(76, 1, '2018-02-02 22:05:53', '2018-02-02 22:05:53', '[coas]', 'Certificate of Authority (COA)', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2018-02-02 22:05:53', '2018-02-02 22:05:53', '', 74, 'http://cibaria-intl.press/74-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2018-02-07 16:25:33', '2018-02-07 16:25:33', '', 'afoa', '', 'inherit', 'open', 'closed', '', 'afoa', '', '', '2018-02-07 16:25:33', '2018-02-07 16:25:33', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/afoa.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2018-02-07 16:25:48', '2018-02-07 16:25:48', '', 'ou', '', 'inherit', 'open', 'closed', '', 'ou', '', '', '2018-02-07 16:25:48', '2018-02-07 16:25:48', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/ou.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2018-02-07 16:26:01', '2018-02-07 16:26:01', '', 'sf', '', 'inherit', 'open', 'closed', '', 'sf', '', '', '2018-02-07 16:26:01', '2018-02-07 16:26:01', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/sf.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2018-02-07 16:26:14', '2018-02-07 16:26:14', '', 'usda', '', 'inherit', 'open', 'closed', '', 'usda', '', '', '2018-02-07 16:26:14', '2018-02-07 16:26:14', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/usda.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2018-02-07 16:26:28', '2018-02-07 16:26:28', '', 'aocs', '', 'inherit', 'open', 'closed', '', 'aocs', '', '', '2018-02-07 16:26:28', '2018-02-07 16:26:28', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/aocs.jpg', 0, 'attachment', 'image/jpeg', 0),
(83, 1, '2018-02-07 16:26:39', '2018-02-07 16:26:39', '', 'uschamber', '', 'inherit', 'open', 'closed', '', 'uschamber', '', '', '2018-02-07 16:26:39', '2018-02-07 16:26:39', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/uschamber.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2018-02-07 16:26:51', '2018-02-07 16:26:51', '', 'siliker', '', 'inherit', 'open', 'closed', '', 'siliker', '', '', '2018-02-07 16:26:51', '2018-02-07 16:26:51', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/siliker.jpg', 0, 'attachment', 'image/jpeg', 0),
(85, 1, '2018-02-07 16:27:03', '2018-02-07 16:27:03', '', 'wbenc', '', 'inherit', 'open', 'closed', '', 'wbenc', '', '', '2018-02-07 16:27:03', '2018-02-07 16:27:03', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/wbenc.jpg', 0, 'attachment', 'image/jpeg', 0),
(86, 1, '2018-02-07 16:27:17', '2018-02-07 16:27:17', '', 'org', '', 'inherit', 'open', 'closed', '', 'org', '', '', '2018-02-07 16:27:17', '2018-02-07 16:27:17', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/org.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2018-02-07 16:27:30', '2018-02-07 16:27:30', '', 'plma', '', 'inherit', 'open', 'closed', '', 'plma', '', '', '2018-02-07 16:27:30', '2018-02-07 16:27:30', '', 40, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/plma.jpg', 0, 'attachment', 'image/jpeg', 0),
(88, 1, '2018-02-07 16:27:34', '2018-02-07 16:27:34', '<div class="col-md-12">\r\n<div class="page-layout">\r\n<div class="table-responsive">\r\n<table class="table table-bordered">\r\n<tbody>\r\n<tr>\r\n<td align="center"><img class="alignnone size-full wp-image-78" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/afoa.png" alt="" width="169" height="165" /></td>\r\n<td align="center"><img class="alignnone size-full wp-image-79" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/ou.jpg" alt="" width="169" height="169" /></td>\r\n<td><img class="alignnone size-medium wp-image-80" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/sf-300x166.jpg" alt="" width="300" height="166" /></td>\r\n</tr>\r\n<tr>\r\n<td><img class="alignnone size-full wp-image-81" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/usda.jpg" alt="" width="169" height="169" /></td>\r\n<td colspan="2"><img class="alignnone size-medium wp-image-82" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/aocs-300x97.jpg" alt="" width="300" height="97" /></td>\r\n<td align="center"><img class="alignnone size-full wp-image-83" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/uschamber.jpg" alt="" width="169" height="169" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="2" align="center"><img class="alignnone size-full wp-image-84" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/siliker.jpg" alt="" width="235" height="59" /></td>\r\n<td colspan="2" align="center"><img class="alignnone size-full wp-image-85" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/wbenc.jpg" alt="" width="136" height="59" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4" align="center"><img class="alignnone size-medium wp-image-86" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/org-300x256.jpg" alt="" width="300" height="256" /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="4">\r\n<p style="text-align: center;"><img class="alignnone size-medium wp-image-87" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/plma-300x236.jpg" alt="" width="300" height="236" /></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>', 'Certifications', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2018-02-07 16:27:34', '2018-02-07 16:27:34', '', 40, 'https://www.cibariaoutlet.com/40-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2018-02-07 16:31:33', '2018-02-07 16:31:33', 'Creating your own brand identity in today\'s competitive marketplace is essential in growing and maintaining your business. With our private label program for olive oils and balsamic vinegars, we can assist you in developing your product line. We offer value-added solutions for private label olive oils, certified organic and specialty oils, blended oils, flavored oils, aerosol oils, and Italian balsamic vinegars.\r\n\r\n<strong>We pride ourselves on being flexible, quick, and honest.</strong>\r\n\r\nSmall minimums are available and can be done. Please contact us to discuss your private label program requirements or let our experienced team assist you in that development. Our creativity, flexibility, quality products, and superior customer service is sure to exceed your expectations.\r\n\r\n&nbsp;\r\n\r\n<img class="aligncenter size-full wp-image-87" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/plma.jpg" alt="" width="820" height="644" />', 'Private Label', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-02-07 16:31:33', '2018-02-07 16:31:33', '', 19, 'https://www.cibariaoutlet.com/19-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2018-02-07 16:32:57', '2018-02-07 16:32:57', '', 'haccp', '', 'inherit', 'open', 'closed', '', 'haccp-2', '', '', '2018-02-07 16:32:57', '2018-02-07 16:32:57', '', 30, 'https://www.cibariaoutlet.com/wp-content/uploads/2018/02/haccp.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2018-02-07 16:33:03', '2018-02-07 16:33:03', 'Cibaria International, Inc. is proudly HACCP Certified. To inquire further about our HACCP Program and policies, please contact our Compliance Manager for more information.\r\n<p style="text-align: center;"><img class="aligncenter size-full wp-image-90" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/haccp.jpg" alt="" width="140" height="132" /></p>', 'Haccp', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2018-02-07 16:33:03', '2018-02-07 16:33:03', '', 30, 'https://www.cibariaoutlet.com/30-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2018-02-07 16:33:47', '2018-02-07 16:33:47', '<h2>Cibaria International, Inc, SILLIKER GOLD RECIPIENT</h2>\r\nCibaria International, Inc. takes great pride in it\'s clean facility, outstanding presentation, and overall product quality. We\'re on a mission to be the very best at what we do. With the help of our trusted third party auditors, we are able to continually monitor our progress as a food processor, and spot areas that could be improved. Cibaria\'s last third party audit was in December of 2010. We consider this audit a great success, and will continue to strive for excellence!\r\n<p style="text-align: center;"><img class="aligncenter size-full wp-image-84" src="https://www.cibariaoutlet.com/wp-content/uploads/2018/02/siliker.jpg" alt="" width="235" height="59" /></p>', 'Third Party Audits', '', 'inherit', 'closed', 'closed', '', '35-revision-v1', '', '', '2018-02-07 16:33:47', '2018-02-07 16:33:47', '', 35, 'https://www.cibariaoutlet.com/35-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(69, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'b.siegel.cibaria21887'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:5:{s:64:"ab1846426808989977d198a824834bae13f93a568baf4fca5b4febcb1f280d26";a:4:{s:10:"expiration";i:1518107484;s:2:"ip";s:15:"104.254.163.134";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";s:5:"login";i:1517934684;}s:64:"40e90c9f9c6adb580e1a08eb829bb53b57fc6b1802b374be925e01b29c5773b9";a:4:{s:10:"expiration";i:1518110198;s:2:"ip";s:15:"104.254.163.134";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";s:5:"login";i:1517937398;}s:64:"8c94ab12c817df923b56a29d70a2d55dee78129bd5133d2a598b591d0b057adc";a:4:{s:10:"expiration";i:1518125150;s:2:"ip";s:15:"104.254.163.134";s:2:"ua";s:78:"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0";s:5:"login";i:1517952350;}s:64:"6fae6bc1defb9a1d173d160b351bd1a821ba0e71621bdc002517c741a91910b5";a:4:{s:10:"expiration";i:1518125180;s:2:"ip";s:15:"104.254.163.134";s:2:"ua";s:78:"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:59.0) Gecko/20100101 Firefox/59.0";s:5:"login";i:1517952380;}s:64:"88d2a71634f1b5a0a365843b4f5a4c6b740fc950bc83b6446e11c61d57e3ef1c";a:4:{s:10:"expiration";i:1518193493;s:2:"ip";s:15:"104.254.163.134";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36";s:5:"login";i:1518020693;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '77'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:13:"104.254.163.0";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&align=center&imgsize=full'),
(20, 1, 'wp_user-settings-time', '1518021088');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'b.siegel.cibaria21887', '$P$BIxKMbGCUcCQnE1I3Exoaxfs082A6K1', 'b-siegel-cibaria21887', 'bsiegel@cibaria-intl.com', '', '2018-02-06 16:31:08', '', 0, 'b.siegel.cibaria21887');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=393;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
