
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
<style>
body, html {
  height: 100%;
  margin: 0;
  font: 400 15px/1.8 "Lato", sans-serif;
  color: #777;
}



.bgimg-1, .bgimg-2, .bgimg-3 {
  position: relative;
  opacity: 0.65;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

}
.bgimg-1 {
  background-image: url("img_parallax.jpg");
  min-height: 100%;
}

.bgimg-2 {
  background-image: url("img_parallax2.jpg");
  min-height: 400px;
}

.bgimg-3 {
  background-image: url("img_parallax3.jpg");
  min-height: 100%;
}

.caption {
  position: absolute;
  left: 0;
  top: 30%;
  width: 100%;
  text-align: center;
  color: #000;
}

.caption h2 {
	font-size:60px;
	-webkit-text-stroke: 1px white;
	font-weight:bold;
}

.caption h1 {
	font-size:100px;
	font-weight:bold;
	-webkit-text-stroke: 1px white;
}


.caption span.border {
  background-color: #111;
  color: #fff;
  padding: 18px;
  font-size: 25px;
  letter-spacing: 10px;
}

h3 {
  letter-spacing: 5px;
  text-transform: uppercase;
  font: 20px "Lato", sans-serif;
  color: #111;
}

.large {
	font-size:28px;
	font-weight: bold;
}

.medium {
	font-size:20px;
	/*font-weight: bold;*/
}

.small {
	font-size: 14px;
	/*font-weight: bold;*/
}

/* Turn off parallax scrolling for tablets and phones */
@media only screen and (max-device-width: 1024px) {
    .bgimg-1, .bgimg-2, .bgimg-3 {
        background-attachment: scroll;
    }
}
</style>
</head>
<body>


<div class="bgimg-1">
	
  <div class="caption">
  	<h1>Then And Now</h1>
  	<h2>1998 - Present</h2>
  	<br />
    <span class="border">SCROLL DOWN</span>
  </div>
</div>

<div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
  <h3 style="text-align:center;">Then - 1998</h3>
  <p><span class="large">Cibaria International was Founded</span>, <span class="medium">Google Was Founded</span>,  <span class="small">A gallon of gas cost $1.15</span>, <span class="large">Dozen Eggs cost 88 cents</span>, <span class="small">Loaf of Bread cost $1.26</span>, <span class="medium">Average Cost of a new home was $129,300.00</span>, <span class="large">Average Monthly Rent $619.00</span>, <span class="small">Worlds biggest airport in the world opens in Hong Kong</span>, <span class="medium">Bill Clinton denies that he had "sexual relations" with former White House intern Monica Lewinsky</span>, <span class="large">The Winter Olympic Games are held in Nagano, Japan</span>, <span class="small">An earthquake measuring 6.1 on the Richter Scale hits northease Afghanistan</span>, <span class="medium">Financial Crisis hits much of South East Asia</span>, <span class="large">France Wins 1998 World Cup in France</span>, <span>Average Income per year is $38,100.00</span></p>
</div>

<div class="bgimg-2 no-show">
  <div class="caption">
  </div>
</div>

<div style="position:relative;">
  <div style="color:#ddd;background-color:#282E34;text-align:center;padding:50px 80px;text-align: justify;">
  	<div class="">
  <div class="row">
    <div class="col-sm">
      <iframe width="420" height="315" src="https://www.youtube.com/embed/U2xJHprqeCs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    <div class="col-sm">
      <img src="windows-98.jpg" class="img-rounded">
    </div>
    <div class="col-sm">
      <img src="armegedon.jpg" class="img-rounded">
    </div>
  </div>
</div>
  </div>
</div>

<div class="bgimg-3">
  <div class="caption">
  	  	<h1>Now</h1>
  	<br />
    <span class="border" style="background-color:transparent;font-size:25px;color: #f7f7f7;">SCROLL DOWN</span>
  </div>
</div>

<div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
  <h3 style="text-align:center;">Now - 2018</h3>
  <p><span class="large">3,812,564,450 internet users spanning the globe</span>, <span class="small">1.3 Billion websites</span>, <span class="medium">Google now processes over 6,586,013,574 search queries a day worldwide</span>, <span class="large">People spend an average of 2 hours and 15 minutes per day on social media networks.</span> <span class="small">People spend 89% of their mobile media time on apps and the other 11% spent on websites. </span>, <span class="large">Facebook currently has 2.07 billion users</span>, <span class="medium">Twitter has 330 million monthly active users.</span>, <span class="small">Over 2 billion messages are exchanged between brands and users each month, with 45.8% of people saying they would rather contact a business through messaging than email.</span>, <span class="medium">US unemployment rate falls to 3.8%, lowest since 2000</span>, <span class="large">First bionic hand with a sense of touch, for use outside a lab unveiled in Rome</span>, <span class="small">North Korea accepts South Korean invitation for high-level talks</span>, <span class="medium">Elon Musk's company SpaceX launches Falcon Heavy, world's most powerful rocket</span>
  	

  </p>
</div>

<div style="position:relative;">
  <div style="color:#ddd;background-color:#282E34;text-align:center;padding:50px 80px;text-align: justify;">
    <div class="">
  <div class="row">
    <div class="col-sm">
      <iframe width="420" height="315" src="https://www.youtube.com/embed/sB_nEtZxPog?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
    </div>
    <div class="col-sm">
      <img src="Windows-10-logo.jpg">
    </div>
    <div class="col-sm">
      <img src="panther.jpg">
    </div>
  </div>
</div>
  </div>
</div>


</body>
</html>