<?php //load wordpress
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php'); ?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>COAS</title>
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo plugins_url( 'css/bootstrap.css', __FILE__ ); ?>"/>
    <link media="print" type="text/css" rel="stylesheet" href="<?php echo plugins_url( 'css/print.css', __FILE__ ); ?>"/>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <script type="text/javascript" src="<?php echo plugins_url( 'js/html2canvas.js?1401228710', __FILE__ ); ?>"></script>
    <script type="text/javascript" src="<?php echo plugins_url('js/jquery.plugin.html2canvas.js?1401228719', __FILE__ ); ?>/"></script>
    <style>
        body {
            margin: 40px;
            -webkit-print-color-adjust: exact;
            font-size: 13px;
        }
        table {
            font-size: 13px;
        }
    </style>
</head>
<body>
<!-- turn canvas into .png -->
<script type="text/javascript">
    function capture() {
        $('#target').html2canvas({
            onrendered: function (canvas) {
                //Set hidden field's value to image data (base-64 string)
                $('#img_val').val(canvas.toDataURL("image/png"'] )) : ?>;
                //Submit the form manually
                document.getElementById("myForm").submit();
            }
        });
    }

</script>
<div class="container">
<div class="span8">

<div style="text-align:center;">
        <img src="<?php echo plugins_url('img/cibaria-logo.jpg',  __FILE__ ); ?>" alt="" width="178" height="157"/> <br />
        <span style="font-size:14px;margin-left:20px;"><strong>TEL: (951) 823 - 8490 - FAX: (951) 823 - 8495</strong></span><br />
        <span style="font-size:14px;margin-left:20px;"><strong>705 Columbia Ave Riverside, CA 92507</strong></span><br/>
        <br/>
        <br/>
        <h2 style="font-size:26px;margin-left:20px;"><u>CERTIFICATE OF ANALYSIS</u></h2>
        <?php if(!empty($_REQUEST['description'])) : ?>
        <h2 style="font-size:26px;margin-left:20px;">
        <?php echo htmlentities($_REQUEST['description']); ?>
        </h2>
    <?php endif; ?>
    </div>

<br />
<br />

<div style="margin-left:80px;">
<table width="100%">

        <?php if(!empty($_REQUEST['lot'] )) : ?>
        <tr>
        <td><strong>LOT NUMBER:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities($_REQUEST['lot']); ?></strong></td>
        <tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['origin'] )) : ?>
        <tr>
        <td><strong>ORIGIN:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['origin'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['expiration_date'] )) : ?>
        <td><strong>MANF DATE:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['manf_date'] )); ?></strong></td>
        </tr>
        <?php endif; ?>
        
        <?php if(!empty($_REQUEST['expiration_date'] )) : ?>
        <td><strong>EXP DATE:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['expiration_date'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

<tr>
    <td height="50px"></td>
</tr>

        <?php if(!empty($_REQUEST['absorbtion_in_ultra_violet'] )) : ?>
        <td><strong>Absorbtion in Ultra Violet:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['absorbtion_in_ultra_violet'] )); ?></strong></td>
        </tr>
        <?php endif; ?>
        
        <?php if(!empty($_REQUEST['k268'] )) : ?>
        <td><strong>k268:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['k268'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['deltak'] )) : ?>
        <td><strong>Delta K:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['deltak'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                                                                      
        <?php if(!empty($_REQUEST['nm232'] )) : ?>
        <td><strong>232nm:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['nm232'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['acid_value'] )) : ?>
        <td><strong>Acid Value:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['acid_value'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['acidity'] )) : ?>
        <td><strong>Acidity:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['acidity'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['additives'] )) : ?>
        <td><strong>Additives:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['additives'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['alcohol'] )) : ?>
        <td><strong>Alcohol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['alcohol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['aom'] )) : ?>
        <td><strong>AOM:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['aom'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['appearance'] )) : ?>
        <td><strong>Appearance:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['appearance'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['arachidic_acid'] )) : ?>
        <td><strong>Arachidic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['arachidic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['aroma'] )) : ?>
        <td><strong>Aroma:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['aroma'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['ash'] )) : ?>
        <td><strong>Ash:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['ash'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['behenic_acid'] )) : ?>
        <td><strong>Behenic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['behenic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['beta_sitosterol_delta5_avenasterol'] )) : ?>
        <td><strong>Beta Sitosterol Delta5 Avenasterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['beta_sitosterol_delta5_avenasterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['brassicasterol'] )) : ?>
        <td><strong>Brassicasterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['brassicasterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['brix'] )) : ?>
        <td><strong>Brix:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['brix'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C3_0'] )) : ?>
        <td><strong>Propionic acid C3:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C3_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C4_0'] )) : ?>
        <td><strong>Butyric acid C4:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C4_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C5_0'] )) : ?>
        <td><strong>Valeric acid C5:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C5_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C6_0'] )) : ?>
        <td><strong>Caproic acid C6:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C6_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C7_0'] )) : ?>
        <td><strong>Enanthic acid C7:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C7_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C8_0'] )) : ?>
        <td><strong>Caprylic acid C8:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C8_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C9_0'] )) : ?>
        <td><strong>Pelargonic acid C9:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C9_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C10_0'] )) : ?>
        <td><strong>Capric acid C10:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C10_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C11_0'] )) : ?>
        <td><strong>Undecylic acid C11:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C11_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C12_0'] )) : ?>
        <td><strong>Lauric acid C12:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C12_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C13_0'] )) : ?>
        <td><strong>Tridecylic acid C13:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C13_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C14_0'] )) : ?>
        <td><strong>Myristic acid C14:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C14_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C15_0'] )) : ?>
        <td><strong>Pentadecylic acid C15:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C15_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C16_0'] )) : ?>
        <td><strong>Palmitic acid C16:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C16_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C16_1'] )) : ?>
        <td><strong>Palmitoliec acid C16:1:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C16_1'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C17_0'] )) : ?>
        <td><strong>Margaric acid C17:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C17_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C17_0'] )) : ?>
        <td><strong>Heptadecenoic acid C17:1:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C17_1'] )); ?></strong></td>
        </tr>
        <?php endif; ?>


        <?php if(!empty($_REQUEST['C18_0'] )) : ?>
        <td><strong>Stearic acid C18:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C18_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C18_1'] )) : ?>
        <td><strong>Oleic acid C18:1:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C18_1'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['trans_c_18_1'] )) : ?>
        <td><strong>Alpha Linolenic C18:3:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['trans_c_18_1'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['c18_1t'] )) : ?>
        <td><strong>C18:1T:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['c18_1t'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C18_2'] )) : ?>
        <td><strong>Linoleic acid C18:2:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C18_2'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['C18_3'] )) : ?>
        <td><strong>Alpha Linolenic C18:3:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C18_3'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['c18_2t_c18_3t'] )) : ?>
        <td><strong>C18:2T + C18:3T:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['c18_2t_c18_3t'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C19_0'] )) : ?>
        <td><strong>Nonadecylic acid C19:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C19_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C20_0'] )) : ?>
        <td><strong>Arachidic acid C20:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C20_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C20_1'] )) : ?>
        <td><strong>Eicosenic Acid C20:1:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C20_1'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C21_0'] )) : ?>
        <td><strong>Heneicosylic acid C21:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C21_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C22_0'] )) : ?>
        <td><strong>Behenic acid C22:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C22_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C23_0'] )) : ?>
        <td><strong>Tricosylic acid C23:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C23_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C24_0'] )) : ?>
        <td><strong>Lignoceric acid C24:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C24_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C25_0'] )) : ?>
        <td><strong>Pentacosylic acid C25:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C25_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C26_0'] )) : ?>
        <td><strong>Cerotic acid C26:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C26_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C27_0'] )) : ?>
        <td><strong>Heptacosylic acid C27:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C27_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C28_0'] )) : ?>
        <td><strong>Montanic acid C28:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C28_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C29_0'] )) : ?>
        <td><strong>Nonacosylic acid C29:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C29_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C30_0'] )) : ?>
        <td><strong>Melissic acid C30:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C30_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C31_0'] )) : ?>
        <td><strong>Henatriacontylic acid C31:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C31_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C32_0'] )) : ?>
        <td><strong>Lacceroic acid C32:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C32_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C33_0'] )) : ?>
        <td><strong>Psyllic acid C33:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C33_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C34_0'] )) : ?>
        <td><strong>Geddic acid C34:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C34_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C35_0'] )) : ?>
        <td><strong>Ceroplastic acid C35:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C35_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C36_0'] )) : ?>
        <td><strong>Hexatriacontylic acid C36:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C36_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C37_0'] )) : ?>
        <td><strong>Heptatriacontanoic acid C37:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C37_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['C38_0'] )) : ?>
        <td><strong>Octatriacontanoic acid C38:0:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['C38_0'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['campestanol_campesterol'] )) : ?>
        <td><strong>Campestanol+Campesterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['campestanol_campesterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['capric_acid'] )) : ?>
        <td><strong>Capric Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['capric_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                          

        <?php if(!empty($_REQUEST['capronic_acid'] )) : ?>
        <td><strong>Capronic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['capronic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['caprylic_acid'] )) : ?>
        <td><strong>Caprylic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['caprylic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['cholesterol'] )) : ?>
        <td><strong>Cholesterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['cholesterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['cold_test'] )) : ?>
        <td><strong>Cold Test:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['cold_test'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['color'] )) : ?>
        <td><strong>Color:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['color'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['color_gardner'] )) : ?>
        <td><strong>Color Gardner:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['color_gardner'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['color_lovibond'] )) : ?>
        <td><strong>Color Lovibond:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['color_lovibond'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['color_red'] )) : ?>
        <td><strong>Color Red:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['color_red'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['blue'] )) : ?>
        <td><strong>Color Blue:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['blue'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['neutral'] )) : ?>
        <td><strong>Color Neutral:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['neutral'] )); ?></strong></td>
        </tr>
        <?php endif; ?>      

        <?php if(!empty($_REQUEST['color_visual'] )) : ?>
        <td><strong>Color Visual:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['color_visual'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['color_yellow'] )) : ?>
        <td><strong>Color Yellow:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['color_yellow'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['neutral'] )) : ?>
        <td><strong>Color Neutral:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['neutral'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['compesterol'] )) : ?>
        <td><strong>Compesterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['compesterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                                                                                              
        <?php if(!empty($_REQUEST['copper'] )) : ?>
        <td><strong>Copper:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['copper'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['delta7_stigmasternol'] )) : ?>
        <td><strong>Delta7 Stigmasternol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['delta7_stigmasternol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['density_20'] )) : ?>
        <td><strong>Density 20:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['density_20'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['developed_alcohol_degree'] )) : ?>
        <td><strong>Developed Alcohol Degree:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['developed_alcohol_degree'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['dextros_equivalent'] )) : ?>
        <td><strong>Dextros Equivalent:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['dextros_equivalent'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['eicosanedienoic_acid'] )) : ?>
        <td><strong>Eicosanedienoic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['eicosanedienoic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['eicosaneteetraenoic_acid'] )) : ?>
        <td><strong>Eicosaneteetraenoic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['eicosaneteetraenoic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['erucic_acid'] )) : ?>
        <td><strong>Erucic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['erucic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['fatty_acid'] )) : ?>
        <td><strong>Fatty Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['fatty_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['ffa'] )) : ?>
        <td><strong>FFA:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['ffa'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['flavor'] )) : ?>
        <td><strong>Flavor:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['flavor'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['free_acidity'] )) : ?>
        <td><strong>Free Acidity:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['free_acidity'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['free_fatty_acids'] )) : ?>
        <td><strong>Free Fatty Acids:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['free_fatty_acids'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['free_so2'] )) : ?>
        <td><strong>Free SO2:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['free_so2'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['free_sulpphurous_anhydride'] )) : ?>
        <td><strong>Free Sulpphurous Anhydride:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['free_sulpphurous_anhydride'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['fructose'] )) : ?>
        <td><strong>Fructose:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['fructose'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['gadoleic_acid'] )) : ?>
        <td><strong>Gadoleic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['gadoleic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['glucose'] )) : ?>
        <td><strong>Glucose:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['glucose'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['grains'] )) : ?>
        <td><strong>Grains:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['grains'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['heavy_metals_as_lead'] )) : ?>
        <td><strong>Heavy Metals as Lead:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['heavy_metals_as_lead'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['heptadacanoic_acid'] )) : ?>
        <td><strong>Heptadacanoic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['heptadacanoic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['heptadecenoic_acid'] )) : ?>
        <td><strong>Heptadecenoic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['heptadecenoic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                                                                  

        <?php if(!empty($_REQUEST['hydroxyl_value'] )) : ?>
        <td><strong>Hydroxyl Value:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['hydroxyl_value'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['identification_a'] )) : ?>
        <td><strong>Identification A:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['identification_a'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['identification_b'] )) : ?>
        <td><strong>Identification B:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['identification_b'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                                                                  


        <?php if(!empty($_REQUEST['identification_c'] )) : ?>
        <td><strong>Identification C:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['identification_c'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['insoluble_impurities'] )) : ?>
        <td><strong>Insoluble Impurities:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['insoluble_impurities'] )); ?></strong></td>
        </tr>
        <?php endif; ?>          

        <?php if(!empty($_REQUEST['iodine_value'] )) : ?>
        <td><strong>Iodine Value:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['iodine_value'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['iodine_value_ri'] )) : ?>
        <td><strong>Iodine Value RI:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['iodine_value_ri'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['iron'] )) : ?>
        <td><strong>Iron:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['iron'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['iv_wijs'] )) : ?>
        <td><strong>iv_wijs:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['iv_wijs'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['lauric_acid'] )) : ?>
        <td><strong>Lauric Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['lauric_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['lead'] )) : ?>
        <td><strong>Lead:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['lead'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['lignoceric_acid'] )) : ?>
        <td><strong>Lignoceric Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['lignoceric_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['linoleic_acid'] )) : ?>
        <td><strong>Linoleic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['linoleic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['linolenic_acid'] )) : ?>
        <td><strong>Linolenic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['linolenic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                          

        <?php if(!empty($_REQUEST['m_i'] )) : ?>
        <td><strong>m_i:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['m_i'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                          

        <?php if(!empty($_REQUEST['maltose'] )) : ?>
        <td><strong>Maltose:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['maltose'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['melting_point'] )) : ?>
        <td><strong>Melting Point:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['melting_point'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['minor'] )) : ?>
        <td><strong>Minor:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['minor'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['moisture'] )) : ?>
        <td><strong>Moisture:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['moisture'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['moisture_and_impurities'] )) : ?>
        <td><strong>Moisture and Impurities:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['moisture_and_impurities'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['moisture_and_sediment_content'] )) : ?>
        <td><strong>Moisture and Sediment Content:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['moisture_and_sediment_content'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['moisture_level'] )) : ?>
        <td><strong>Moisture level:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['moisture_level'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['myistoleic_acid'] )) : ?>
        <td><strong>Myistoleic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['myistoleic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['myristic'] )) : ?>
        <td><strong>Myristic:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['myristic'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['nervonic_acid'] )) : ?>
        <td><strong>Nervonic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['nervonic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['octadecatetraen_acid'] )) : ?>
        <td><strong>Octadecatetraen Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['octadecatetraen_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['odor'] )) : ?>
        <td><strong>Odor:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['odor'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['oleic_acid'] )) : ?>
        <td><strong>Oleic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['oleic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['optical_rotation'] )) : ?>
        <td><strong>Optical Rotation:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['optical_rotation'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['osi'] )) : ?>
        <td><strong>OSI:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['osi'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['other_carbohydrates'] )) : ?>
        <td><strong>Other Carbohydrates:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['other_carbohydrates'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['palmitic_acid'] )) : ?>
        <td><strong>Palmitic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['palmitic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['palmitoliec_acid'] )) : ?>
        <td><strong>Palmitoliec Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['palmitoliec_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['peroxide_value'] )) : ?>
        <td><strong>Peroxide Value:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['peroxide_value'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['ph'] )) : ?>
        <td><strong>Ph:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['ph'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['pounds_per_gallon'] )) : ?>
        <td><strong>Pounds Per Gallon:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['pounds_per_gallon'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['pv'] )) : ?>
        <td><strong>PV:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['pv'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                                                                                                                                                          
        <?php if(!empty($_REQUEST['refactive_index'] )) : ?>
        <td><strong>Refactive Index:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['refactive_index'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['refractive_index'] )) : ?>
        <td><strong>Refractive Index:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['refractive_index'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['rifractometric_at_20'] )) : ?>
        <td><strong>rifractometric_at_20:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['rifractometric_at_20'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['salt'] )) : ?>
        <td><strong>Salt:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['salt'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['saponification'] )) : ?>
        <td><strong>Saponification:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['saponification'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['saponification_value'] )) : ?>
        <td><strong>Saponification Value:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['saponification_value'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['saturated_fatty_acids'] )) : ?>
        <td><strong>Saturated Fatty Acids:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['saturated_fatty_acids'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['smoke_point'] )) : ?>
        <td><strong>Smoke Point:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['smoke_point'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['so2'] )) : ?>
        <td><strong>SO2:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['so2'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['specific_gravity'] )) : ?>
        <td><strong>Specific Gravity:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['specific_gravity'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['specific_weight'] )) : ?>
        <td><strong>Specific Weight:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['specific_weight'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['specific_weight_as_baumic_degrees'] )) : ?>
        <td><strong>Specific Weight as Baumic Degrees:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['specific_weight_as_baumic_degrees'] )); ?></strong></td>
        </tr>
        <?php endif; ?>      

        <?php if(!empty($_REQUEST['starch'] )) : ?>
        <td><strong>Starch:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['starch'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['stearic_acid'] )) : ?>
        <td><strong>Stearic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['stearic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['stigmasterol'] )) : ?>
        <td><strong>Stigmasterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['stigmasterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['taste'] )) : ?>
        <td><strong>Taste:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['taste'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['tbhq'] )) : ?>
        <td><strong>tbhq:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['tbhq'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['total_acidity_as_acetic_acid'] )) : ?>
        <td><strong>Total Acidity as Acetic Acid:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_acidity_as_acetic_acid'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['total_dry_extract'] )) : ?>
        <td><strong>Total Dry Extract:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_dry_extract'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['total_monounsaturated'] )) : ?>
        <td><strong>Total Monounsaturated:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_monounsaturated'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['total_polyunsaturated'] )) : ?>
        <td><strong>Total Polyunsaturated:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_polyunsaturated'] )); ?></strong></td>
        </tr>
        <?php endif; ?>                                                                                                                                                                          
        <?php if(!empty($_REQUEST['total_saturated'] )) : ?>
        <td><strong>Total Saturated:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_saturated'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['total_so2'] )) : ?>
        <td><strong>Total SO2:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_so2'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['total_sterols'] )) : ?>
        <td><strong>Total Sterols:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_sterols'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['total_sulphurous_anhydride'] )) : ?>
        <td><strong>Total Sulphurous Anhydride:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['total_sulphurous_anhydride'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['trans_c_18_1'] )) : ?>
        <td><strong>trans_c_18_1:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['trans_c_18_1'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['trans_c_18_2'] )) : ?>
        <td><strong>trans_c_18_2:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['trans_c_18_2'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['trans_fatty_acids'] )) : ?>
        <td><strong>trans_fatty_acids:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['trans_fatty_acids'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['turbidity'] )) : ?>
        <td><strong>turbidity:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['turbidity'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['unsaponifiable_matter'] )) : ?>
        <td><strong>Unsaponifiable Matter:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['unsaponifiable_matter'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['vitamin_e'] )) : ?>
        <td><strong>Vitamin E:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['vitamin_e'] )); ?></strong></td>
        </tr>
        <?php endif; ?>  

        <?php if(!empty($_REQUEST['zinc'] )) : ?>
        <td><strong>Zinc:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['zinc'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['k268'] )) : ?>
        <td><strong>k268:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['k268'] )); ?></strong></td>
        </tr>
        <?php endif; ?>      

        <?php if(!empty($_REQUEST['k270'] )) : ?>
        <td><strong>K270:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['k270'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['im'] )) : ?>
        <td><strong>IM:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['im'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['pph'] )) : ?>
        <td><strong>PPH:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['pph'] )); ?></strong></td>
        </tr>
        <?php endif; ?>
                                                                                                        
        <?php if(!empty($_REQUEST['moi'] )) : ?>
        <td><strong>MOI:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['moi'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['ind'] )) : ?>
        <td><strong>IND:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['ind'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['bit225'] )) : ?>
        <td><strong>BIT(225):</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['bit225'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['ppp'] )) : ?>
        <td><strong>PPP:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['ppp'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['dag'] )) : ?>
        <td><strong>DAG:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['dag'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['defects'] )) : ?>
        <td><strong>Defects:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['defects'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['fruitness'] )) : ?>
        <td><strong>Fruitness:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['fruitness'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['bitterness'] )) : ?>
        <td><strong>Bitterness:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['bitterness'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['pungency'] )) : ?>
        <td><strong>Pungency:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['pungency'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['campesterol'] )) : ?>
        <td><strong>Campesterol:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['campesterol'] )); ?></strong></td>
        </tr>
        <?php endif; ?>

        <?php if(!empty($_REQUEST['difference_ecn42'] )) : ?>
        <td><strong>Triglecerides Difference ECN42:</strong></td>
        <td width="150px"></td>
        <td><strong><?php echo htmlentities(strtoupper($_REQUEST['difference_ecn42'] )); ?></strong></td>
        </tr>
        <?php endif; ?>
</table>
<hr>
        <p style="font-weight:bold;"><i>This is an electronically generated document and is valid without signature.</i></p>
</div>
</div>
</body>
</html>

