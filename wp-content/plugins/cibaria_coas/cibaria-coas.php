<?php
/**
 * @package Cibaria COAS
 */
/*
Plugin Name: Cibaria COA Connection
Plugin URI: cibaria-intl.com/coas
Description: This is used to connect the cibariainternational.com/coas, coas with the cibaria-intl.com website
Version: 0.0.1
Author: Cibaria International
Author URI: https://www.cibaria-intl.com
License: GPLv2 or later
Text Domain: Cibaria COAS
*/

function CibariaCOAS() {

global $content;

//Get Guzzle Composer Library
require('vendor/autoload.php');
//load wordpress
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

$client = new GuzzleHttp\Client();

//get json
$body = $client->get('https://www.cibariainternational.com/api/coas')->getBody()->getContents();

$response = json_decode($body, true);

$content .= '</div></div>';

$content .= '<table class="table table-striped">
    <thead>
    <tr>
        <th>Lot#</th>
        <th>Description</th>
        <th>&nbsp;</th>
    </tr>
    </thead>

    <tbody>
    <tr>';

foreach($response as $result) :

include('_form.php');

$content .= '<td><strong>' . $result['lot'] . '</strong></td>';
$content .= '<td>' . $result['description'] . '</td>';
$content .= '<td valign="center">';
$content .= '<input type="image" src="' . get_template_directory_uri() . '/img/view.jpg" class="" style="padding-top:8px;">';
$content .= '</td></tr></tbody></form>';

endforeach;

$content .= '</table>';

return $content;
}

add_shortcode('coas', 'CibariaCOAS');