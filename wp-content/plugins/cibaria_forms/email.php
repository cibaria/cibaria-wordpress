<?php 

if (isset($_POST['InputTelephone'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED//

// $email_to = "customerservice@cibaria-intl.com";   

    $email_to = "bsiegel@cibaria-intl.com";

    // $email_to = "rghazarian@cibaria-intl.com";    

    // $email_to = "kgriset@cibaria-intl.com";    
    // $email_to = "cibaria_karenmoore@hotmail.com";

    $email_subject = "Cibaria International Bulk Request Form";
    function died($error)
    {

        //html        

        //end html        

        echo "We are very sorry, but there were error(s) found with the form you submitted. ";

        echo "These errors appear below.<br /><br />";

        echo $error . "<br /><br />";

        echo "Please go back and fix these errors.<br /><br />";
        die();

    }

    // validation expected data exists    

    if (!isset($_POST['InputName']) || !isset($_POST['InputTelephone']) || !isset($_POST['InputEmail']) || !isset($_POST['InputCompany']) || !isset($_POST['InputAddress']) || !isset($_POST['InputMonthlyVolume']) || !isset($_POST['InputProductsNeeded']) || !isset($_POST['InputMessage'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }
    $name = $_POST['InputName'];
// required    

    $telephone = $_POST['InputTelephone'];
    // required    

    $email_from = $_POST['InputEmail'];
    // required    

    $company = $_POST['InputCompany'];
    // required    

    $address = $_POST['InputAddress'];
    // required    

    $monthly_volume = $_POST['InputMonthlyVolume'];

    $products_needed = $_POST['InputProductsNeeded'];

    $comments = $_POST['InputMessage'];
    // required    

    $date_added = Date("m/d/Y");

    $error_message = ""; 

    $email_message = "Cibaria International Bulk Request Form.\n\n";
    function clean_string($string)
    {
        $bad = array("content-type", "bcc:", "to:", "cc:", "href");
        return str_replace($bad, "", $string);
    }

    $email_message .= "Name: " . clean_string($name) . "\n";
    $email_message .= "Email: " . clean_string($email_from) . "\n";
    $email_message .= "Telephone: " . clean_string($telephone) . "\n";
    $email_message .= "Company: " . clean_string($company) . "\n";
    $email_message .= "Address: " . clean_string($address) . "\n";
    $email_message .= "Monthly Volume: " . clean_string($monthly_volume) . "\n";
    $email_message .= "Products Needed: " . clean_string($products_needed) . "\n";
    $email_message .= "Special Needs: " . clean_string($comments) . "\n";

    // create email headers    

    $headers = 'From: ' . $email_from . "\r\n" .

        'Reply-To: ' . $email_from . "\r\n" .

        'X-Mailer: PHP/' . phpversion();

    @mail($email_to, $email_subject, $email_message, $headers);
header("refresh:5;url=/");
}