<?php 
//load wordpress
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

if(isset($_POST['InputTelephone'])) :

//    $email_to = "customerservice@cibaria-intl.com";

    $email_to = "customerservice@cibaria-intl.com";

    $email_subject = "Cibaria International Contact Form";

    function died($error) {

        echo "We are very sorry, but there were error(s) found with the form you submitted. ";
        echo "These errors appear below.<br /><br />";
        echo $error."<br /><br />";
        echo "Please go back and fix these errors.<br /><br />";
        die();
    }

    // validation expected data exists
    if(!isset($_POST['InputName']) ||
        !isset($_POST['InputTelephone']) ||
        !isset($_POST['InputEmail']) ||
        !isset($_POST['InputMessage'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');
    }

    $name = $_POST['InputName']; // required
    $telephone = $_POST['InputTelephone']; // required
    $email_from = $_POST['InputEmail']; // required
    $comments = $_POST['InputMessage']; // required
    $error_message = "";

//email template for inbox
    $email_message = "Cibaria International Contact Form.\n\n";

    function clean_string($string) {
        $bad = array("content-type","bcc:","to:","cc:","href");
        return str_replace($bad,"",$string);
    }

    $email_message .= "Name: ".clean_string($name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
    $email_message .= "Telephone: ".clean_string($telephone)."\n";
    $email_message .= "Message: ".clean_string($comments)."\n";

// create email headers
    $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();
    @mail($email_to, $email_subject, $email_message, $headers);
?>
    <?php wp_redirect( '/thank-you');  ?>

   <?php endif; ?>