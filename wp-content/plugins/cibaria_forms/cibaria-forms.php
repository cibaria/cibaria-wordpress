<?php

/**
 * @package Cibaria Forms
 */
/*
Plugin Name: Cibaria Forms
Plugin URI: cibaria-intl.com
Description: Custom Contact Forms
Version: 0.0.1
Author: Cibaria International
Author URI: https://www.cibaria-intl.com
License: GPLv2 or later
Text Domain: Cibaria Forms
*/


function BulkQuotes() {

$content = '';

$content .= '<form role="form" action="' . plugin_dir_url(__FILE__) . 'bulk_request_post.php' . '" method="post" >
    <div class="col-lg-6">
        <div class="form-group">
            <label for="InputName">Your Name</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Name" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputCompany">Your Company</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputCompany" id="InputCompany" placeholder="Enter Company" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputAddress">Your Address</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputAddress" id="InputAddress" placeholder="Enter Address" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputName">Your Telephone Number</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputTelephone" id="InputTelephone" placeholder="Enter Telephone" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputEmail">Your Email</label>
            <div class="input-group">
                <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email" required  >
                <span class="input-group-addon"><i class="glyphicon  form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputMessage">Products Needed</label>
            <div class="input-group">
                <textarea name="InputProductsNeeded" id="InputProductsNeeded" class="form-control" rows="5" required></textarea>
                <span class="input-group-addon"><i class="glyphicon  form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputMonthlyVolume">Estimated Monthly Volume</label>
            <div class="input-group">
                <textarea name="InputMonthlyVolume" id="InputMonthlyVolume" class="form-control" rows="5" required></textarea>
                <span class="input-group-addon"><i class="glyphicon  form-control-feedback"></i></span></div>
        </div>

        <div class="form-group">
            <label for="InputMessage">Special Needs Or Instructions</label>
            <div class="input-group">
                <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required></textarea>
                <span class="input-group-addon"><i class="glyphicon  form-control-feedback"></i></span></div>
        </div>

        <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
    </div>
</form>

<hr class="featurette-divider hidden-lg" />
<div class="col-lg-5 col-md-push-1"><address>
    <h3>Office Location</h3>
    <p class="lead"> 705 Columbia Ave, Riverside, CA 92507<br /> Phone: (951) 823-8490<br /> Fax: (951) 823-8495</p>
</address>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3307.6386219871947!2d-117.32656569999999!3d34.00181489999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcae778032e42b%3A0xee65736fd898ad77!2s705+Columbia+Ave%2C+Riverside%2C+CA+92507!5e0!3m2!1sen!2sus!4v1442495222580" width="400" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>';


return $content;
}

function CibariaContact() {
	$content = '';

$content .= '<form role="form" action="' . plugin_dir_url(__FILE__) . 'cibaria_contact_post.php' . '" method="post" >
    <div class="col-lg-6">
        <div class="form-group">
            <label for="InputName">Your Name</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Name" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputCompany">Your Company</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputCompany" id="InputCompany" placeholder="Enter Company" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputAddress">Your Address</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputAddress" id="InputAddress" placeholder="Enter Address" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputName">Your Telephone Number</label>
            <div class="input-group">
                <input type="text" class="form-control" name="InputTelephone" id="InputTelephone" placeholder="Enter Telephone" required>
                <span class="input-group-addon"><i class="glyphicon form-control-feedback"></i></span></div>
        </div>
        <div class="form-group">
            <label for="InputEmail">Your Email</label>
            <div class="input-group">
                <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email" required  >
                <span class="input-group-addon"><i class="glyphicon  form-control-feedback"></i></span></div>
        </div>
          <div class="form-group">
            <label for="InputMessage">Message</label>
            <div class="input-group">
                <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required></textarea>
                <span class="input-group-addon"><i class="glyphicon  form-control-feedback"></i></span></div>
        </div>
        <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
    </div>
</form>

<hr class="featurette-divider hidden-lg" />
<div class="col-lg-5 col-md-push-1"><address>
    <h3>Office Location</h3>
    <p class="lead"> 705 Columbia Ave, Riverside, CA 92507<br /> Phone: (951) 823-8490<br /> Fax: (951) 823-8495</p>
</address>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3307.6386219871947!2d-117.32656569999999!3d34.00181489999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80dcae778032e42b%3A0xee65736fd898ad77!2s705+Columbia+Ave%2C+Riverside%2C+CA+92507!5e0!3m2!1sen!2sus!4v1442495222580" width="400" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>';


return $content;
}

add_shortcode( 'bulk-quotes', 'BulkQuotes' );

add_shortcode( 'cibaria-contact', 'CibariaContact' );
