<?php
$content .= '
<form action="' . plugin_dir_url(__FILE__) . 'spec.php' . '" method="post">
<input type="hidden" value="' . $result["is_oil_vinegar"]. '" name="is_oil_vinegar" />
<input type="hidden" value="' . $result["title"]. '" name="title" />
<input type="hidden" value="' . $result["description"]. '" name="description" />
<input type="hidden" value="' . $result["reviewed"]. '" name="reviewed" />';

$content .= '<input type="hidden" value="' . utf8_encode($result["storage"]) . '"name="storage" />';

$content .= '
<input type="hidden" value="' . $result["shelf_life"]. '" name="shelf_life" />
<input type="hidden" value="' . $result["sewer"]. '" name="sewer" />
<input type="hidden" value="' . $result["applications"]. '" name="applications" />
<input type="hidden" value="' . $result["nutrition"]. '" name="nutrition" />
<input type="hidden" value="' . $result["country"]. '" name="country" />
<input type="hidden" value="' . $result["certifications_cas"]. '" name="certifications_cas" />
<input type="hidden" value="' . $result["certifications_einecs"]. '" name="certifications_einecs" />
<input type="hidden" value="' . $result["certifications_incl"]. '" name="certifications_incl" />
<input type="hidden" value="' . $result["typical_270"]. '" name="typical_270" />
<input type="hidden" value="' . $result["typical_delta"]. '" name="typical_delta" />
<input type="hidden" value="' . $result["typical_270after"]. '" name="typical_270after" />
<input type="hidden" value="' . $result["typical_passage"]. '" name="typical_passage" />
<input type="hidden" value="' . $result["typical_peroxide"]. '" name="typical_peroxide" />
';

$content .= '<input type="hidden" value="' . utf8_encode($result["typical_gravity"]) . '" name="typical_gravity" />';

$content .= '
<input type="hidden" value="' . $result["typical_myristic"]. '" name="typical_myristic" />
<input type="hidden" value="' . $result["typical_palmitic"]. '" name="typical_palmitic" />
<input type="hidden" value="' . $result["typical_heptadecanoic"]. '" name="typical_heptadecanoic" />
<input type="hidden" value="' . $result["typical_heptadecenoic"]. '" name="typical_heptadecenoic" />
<input type="hidden" value="' . $result["typical_stearic"]. '" name="typical_stearic" />
<input type="hidden" value="' . $result["typical_oleic"]. '" name="typical_oleic" />
';

$content .='<input type="hidden" value="' . utf8_encode($result["typical_linoleic"]) . '" name="typical_linoleic" />';
$content .='<input type="hidden" value="' . utf8_encode($result["typical_linolenic"]) . '" name="typical_linolenic" />';

$content .='
<input type="hidden" value="' . $result["typical_arachidic"]. '" name="typical_arachidic" />
<input type="hidden" value="' . $result["typical_gadoleic"]. '" name="typical_gadoleic" />
<input type="hidden" value="' . $result["typical_behenic"]. '" name="typical_behenic" />
<input type="hidden" value="' . $result["typical_lignoceric"]. '" name="typical_lignoceric" />
<input type="hidden" value="' . $result["water_unit"]. '" name="water_unit" />
<input type="hidden" value="' . $result["water_value"]. '" name="water_value" />
<input type="hidden" value="' . $result["water_tbsp"]. '" name="water_tbsp" />
<input type="hidden" value="' . $result["energy_unit"]. '" name="energy_unit" />
<input type="hidden" value="' . $result["energy_value"]. '" name="energy_value" />
<input type="hidden" value="' . $result["energy_tbsp"]. '" name="energy_tbsp" />
<input type="hidden" value="' . $result["protein_unit"]. '" name="protein_unit" />
<input type="hidden" value="' . $result["protein_value"]. '" name="protein_value" />
<input type="hidden" value="' . $result["protein_tbsp"]. '" name="protein_tbsp" />
<input type="hidden" value="' . $result["protein_unit"]. '" name="protein_unit" />
<input type="hidden" value="' . $result["totallipid_value"]. '" name="totallipid_value" />
<input type="hidden" value="' . $result["totallipid_unit"]. '" name="totallipid_unit" />
<input type="hidden" value="' . $result["totallipid_tbsp"]. '" name="totallipid_tbsp" />
<input type="hidden" value="' . $result["carbohydrate_unit"]. '" name="carbohydrate_unit" />
<input type="hidden" value="' . $result["carbohydrate_value"]. '" name="carbohydrate_value" />
<input type="hidden" value="' . $result["carbohydrate_tbsp"]. '" name="carbohydrate_tbsp" />
<input type="hidden" value="' . $result["fiber_unit"]. '" name="fiber_unit" />
<input type="hidden" value="' . $result["fiber_value"]. '" name="fiber_value" />
<input type="hidden" value="' . $result["fiber_tbsp"]. '" name="fiber_tbsp" />
<input type="hidden" value="' . $result["sugars_unit"]. '" name="sugars_unit" />
<input type="hidden" value="' . $result["sugars_value"]. '" name="sugars_value" />
<input type="hidden" value="' . $result["sugars_tbsp"]. '" name="sugars_tbsp" />
<input type="hidden" value="' . $result["calcium_unit"]. '" name="calcium_unit" />
<input type="hidden" value="' . $result["calcium_value"]. '" name="calcium_value" />
<input type="hidden" value="' . $result["calcium_tbsp"]. '" name="calcium_tbsp" />
<input type="hidden" value="' . $result["iron_unit"]. '" name="iron_unit" />
<input type="hidden" value="' . $result["iron_value"]. '" name="iron_value" />
<input type="hidden" value="' . $result["iron_tbsp"]. '" name="iron_tbsp" />
<input type="hidden" value="' . $result["magnesium_unit"]. '" name="magnesium_unit" />
<input type="hidden" value="' . $result["magnesium_value"]. '" name="magnesium_value" />
<input type="hidden" value="' . $result["magnesium_tbsp"]. '" name="magnesium_tbsp" />
<input type="hidden" value="' . $result["phosphorus_unit"]. '" name="phosphorus_unit" />
<input type="hidden" value="' . $result["phosphorus_value"]. '" name="phosphorus_value" />
<input type="hidden" value="' . $result["phosphorus_tbsp"]. '" name="phosphorus_tbsp" />
<input type="hidden" value="' . $result["potassium_unit"]. '" name="potassium_unit" />
<input type="hidden" value="' . $result["potassium_value"]. '" name="potassium_value" />
<input type="hidden" value="' . $result["potassium_tbsp"]. '" name="potassium_tbsp" />
<input type="hidden" value="' . $result["sodium_unit"]. '" name="sodium_unit" />
<input type="hidden" value="' . $result["sodium_value"]. '" name="sodium_value" />
<input type="hidden" value="' . $result["sodium_tbsp"]. '" name="sodium_tbsp" />
<input type="hidden" value="' . $result["zinc_unit"]. '" name="zinc_unit" />
<input type="hidden" value="' . $result["zinc_value"]. '" name="zinc_value" />
<input type="hidden" value="' . $result["zinc_tbsp"]. '" name="zinc_tbsp" />
<input type="hidden" value="' . $result["vitaminc_unit"]. '" name="vitaminc_unit" />
<input type="hidden" value="' . $result["vitaminc_value"]. '" name="vitaminc_value" />
<input type="hidden" value="' . $result["vitaminc_tbsp"]. '" name="vitaminc_tbsp" />
<input type="hidden" value="' . $result["folate_unit"]. '" name="folate_unit" />
<input type="hidden" value="' . $result["folate_value"]. '" name="folate_value" />
<input type="hidden" value="' . $result["folate_tbsp"]. '" name="folate_tbsp" />
<input type="hidden" value="' . $result["vitaminb6_unit"]. '" name="vitaminb6_unit" />
<input type="hidden" value="' . $result["vitaminb6_value"]. '" name="vitaminb6_value" />
<input type="hidden" value="' . $result["vitaminb6_tbsp"]. '" name="vitaminb6_tbsp" />
<input type="hidden" value="' . $result["thiamin_unit"]. '" name="thiamin_unit" />
<input type="hidden" value="' . $result["thiamin_value"]. '" name="thiamin_value" />
<input type="hidden" value="' . $result["thiamin_tbsp"]. '" name="thiamin_tbsp" />
<input type="hidden" value="' . $result["riboflavin_unit"]. '" name="riboflavin_unit" />
<input type="hidden" value="' . $result["riboflavin_value"]. '" name="riboflavin_value" />
<input type="hidden" value="' . $result["riboflavin_tbsp"]. '" name="riboflavin_tbsp" />
<input type="hidden" value="' . $result["niacin_unit"]. '" name="niacin_unit" />
<input type="hidden" value="' . $result["niacin_value"]. '" name="niacin_value" />
<input type="hidden" value="' . $result["niacin_tbsp"]. '" name="niacin_tbsp" />
<input type="hidden" value="' . $result["vitaminb12_unit"]. '" name="vitaminb12_unit" />
<input type="hidden" value="' . $result["vitaminb12_value"]. '" name="vitaminb12_value" />
<input type="hidden" value="' . $result["vitaminb12_tbsp"]. '" name="vitaminb12_tbsp" />
<input type="hidden" value="' . $result["vitaminar_unit"]. '" name="vitaminar_unit" />
<input type="hidden" value="' . $result["vitaminar_value"]. '" name="vitaminar_value" />
<input type="hidden" value="' . $result["vitaminar_tbsp"]. '" name="vitaminar_tbsp" />
<input type="hidden" value="' . $result["vitaminai_tbsp"]. '" name="vitaminai_tbsp" />
<input type="hidden" value="' . $result["vitaminai_value"]. '" name="vitaminai_value" />
<input type="hidden" value="' . $result["vitaminai_tbsp"]. '" name="vitaminai_tbsp" />
<input type="hidden" value="' . $result["vitamine_tbsp"]. '" name="vitamine_tbsp" />
<input type="hidden" value="' . $result["vitamine_value"]. '" name="vitamine_value" />
<input type="hidden" value="' . $result["vitamine_unit"]. '" name="vitamine_unit" />
<input type="hidden" value="' . $result["vitamind2_unit"]. '" name="vitamind2_unit" />
<input type="hidden" value="' . $result["vitamind2_value"]. '" name="vitamind2_value" />
<input type="hidden" value="' . $result["vitamind2_tbsp"]. '" name="vitamind2_tbsp" />
<input type="hidden" value="' . $result["vitamind_tbsp"]. '" name="vitamind_tbsp" />
<input type="hidden" value="' . $result["vitamind_unit"]. '" name="vitamind_unit" />
<input type="hidden" value="' . $result["vitamind_value"]. '" name="vitamind_value" />
<input type="hidden" value="' . $result["vitamink_value"]. '" name="vitamink_value" />
<input type="hidden" value="' . $result["vitamink_tbsp"]. '" name="vitamink_tbsp" />
<input type="hidden" value="' . $result["vitamink_unit"]. '" name="vitamink_unit" />
<input type="hidden" value="' . $result["fattysaturated_unit"]. '" name="fattysaturated_unit" />
<input type="hidden" value="' . $result["fattysaturated_value"]. '" name="fattysaturated_value" />
<input type="hidden" value="' . $result["fattysaturated_tbsp"]. '" name="fattysaturated_tbsp" />
<input type="hidden" value="' . $result["fattymonoun_unit"]. '" name="fattymonoun_unit" />
<input type="hidden" value="' . $result["fattymonoun_value"]. '" name="fattymonoun_value" />
<input type="hidden" value="' . $result["fattymonoun_tbsp"]. '" name="fattymonoun_tbsp" />
<input type="hidden" value="' . $result["fattypoly_unit"]. '" name="fattypoly_unit" />
<input type="hidden" value="' . $result["fattypoly_value"]. '" name="fattypoly_value" />
<input type="hidden" value="' . $result["fattypoly_tbsp"]. '" name="fattypoly_tbsp" />
<input type="hidden" value="' . $result["caffeine_unit"]. '" name="caffeine_unit" />
<input type="hidden" value="' . $result["caffeine_value"]. '" name="caffeine_value" />
<input type="hidden" value="' . $result["caffeine_tbsp"]. '" name="caffeine_tbsp" />
<input type="hidden" value="' . $result["sku"]. '" name="sku" />
<input type="hidden" value="' . $result["typical_acidity"]. '" name="typical_acidity" />
<input type="hidden" value="' . $result["typical_palmitoleic"]. '" name="typical_palmitoleic" />
<input type="hidden" value="' . $result["sodium_unit"]. '" name="sodium_unit" />
<input type="hidden" value="' . $result["cholesterol_unit"]. '" name="cholesterol_unit" />
<input type="hidden" value="' . $result["cholesterol_value"]. '" name="cholesterol_value" />
<input type="hidden" value="' . $result["cholesterol_tbsp"]. '" name="cholesterol_tbsp" />
<input type="hidden" value="' . $result["typical_max"]. '" name="typical_max" />
<input type="hidden" value="' . $result["nutrition_fact1"]. '" name="nutrition_fact1" />
<input type="hidden" value="' . $result["nutrition_fact2"]. '" name="nutrition_fact2" />
<input type="hidden" value="' . $result["nutrition_fact3"]. '" name="nutrition_fact3" />
';

 if($result["bullet_point1"] !== "") : 
    $content .='<input type="hidden" value="' . utf8_encode($result["bullet_point1"]) . '" name="bullet_point1" />';
endif;

if($result["bullet_point2"] !== "") :
    $content .='<input type="hidden" value="' . utf8_encode($result["bullet_point2"]) . '" name="bullet_point2" />';
endif;

if($result["bullet_point3"] !== "") :
    $content .='<input type="hidden" value="' . utf8_encode($result["bullet_point3"]) . '" name="bullet_point3" />';
endif;


if($result["bullet_point4"] !== "") :
    $content .= '<input type="hidden" value="' . utf8_encode($result["bullet_point4"]) . '" name="bullet_point4" />';
endif;

if($result["bullet_point5"] !== "") :
    $content .= '<input type="hidden" value="' . utf8_encode($result["bullet_point5"]) . '" name="bullet_point5" />';
endif;

if($result["bullet_point6"] !== "") :
    $content .= '<input type="hidden" value="' . utf8_encode($result["bullet_point6"]) . '" name="bullet_point6" />';
endif;

if($result["bullet_point7"] !== "") :
    $content .= '<input type="hidden" value="' . utf8_encode($result["bullet_point7"]) . '" name="bullet_point7" />';
endif;

if($result["bullet_point8"] !== "") :
    $content .= '<input type="hidden" value="' . utf8_encode($result["bullet_point8"]) . '" name="bullet_point8" />';
endif;

$content .= '
<input type="hidden" value="' . $result["stability_reactivity1"]. '" name="stability_reactivity1" />
<input type="hidden" value="' . $result["stability_reactivity2"]. '" name="stability_reactivity2" />
<input type="hidden" value="' . $result["stability_reactivity3"]. '" name="stability_reactivity3" />
<input type="hidden" value="' . $result["stability_reactivity4"]. '" name="stability_reactivity4" />
<input type="hidden" value="' . $result["stability_reactivity5"]. '" name="stability_reactivity5" />
<input type="hidden" value="' . $result["stability_reactivity6"]. '" name="stability_reactivity6" />
<input type="hidden" value="' . $result["tox_info1"]. '" name="tox_info1" />
<input type="hidden" value="' . $result["organoleptic_odor"]. '" name="organoleptic_odor" />
<input type="hidden" value="' . $result["tox_info2"]. '" name="tox_info2" />
<input type="hidden" value="' . $result["chemical_acidity"]. '" name="chemical_acidity" />
<input type="hidden" value="' . $result["chemical_ph"]. '" name="chemical_ph" />
<input type="hidden" value="' . $result["tox_info3"]. '" name="tox_info3" />
<input type="hidden" value="' . $result["tox_info4"]. '" name="tox_info4" />
<input type="hidden" value="' . $result["tox_info5"]. '" name="tox_info5" />
<input type="hidden" value="' . $result["tox_info6"]. '" name="tox_info6" />
<input type="hidden" value="' . $result["eco_info1"]. '" name="eco_info1" />
<input type="hidden" value="' . $result["eco_info2"]. '" name="eco_info2" />
<input type="hidden" value="' . $result["eco_info3"]. '" name="eco_info3" />
<input type="hidden" value="' . $result["eco_info4"]. '" name="eco_info4" />
<input type="hidden" value="' . $result["eco_info5"]. '" name="eco_info5" />
<input type="hidden" value="' . $result["chemical_gravity"]. '" name="chemical_gravity" />
<input type="hidden" value="' . $result["chemical_matters"]. '" name="chemical_matters" />
<input type="hidden" value="' . $result["chemical_metals"]. '" name="chemical_metals" />
<input type="hidden" value="' . $result["chemical_extract"]. '" name="chemical_extract" />
<input type="hidden" value="' . $result["chemical_anhydride"]. '" name="chemical_anhydride" />
<input type="hidden" value="' . $result["chemical_ash"]. '" name="chemical_ash" />
<input type="hidden" value="' . $result["chemical_grain"]. '" name="chemical_grain" />
<input type="hidden" value="' . $result["chemical_alcohol"]. '" name="chemical_alcohol" />
<input type="hidden" value="' . $result["chemical_sugar_free_extract"]. '" name="chemical_sugar_free_extract" />
<input type="hidden" value="' . $result["chemical_density"]. '" name="chemical_density" />
<input type="hidden" value="' . $result["chemical_brix"]. '" name="chemical_brix" />
<input type="hidden" value="' . $result["chemical_alcohol_degree"]. '" name="chemical_alcohol_degree" />
<input type="hidden" value="' . $result["chemical_reduced_dry_extract"]. '" name="chemical_reduced_dry_extract" />
<input type="hidden" value="' . $result["chemical_sugars"]. '" name="chemical_sugars" />
<input type="hidden" value="' . $result["chemical_rif"]. '" name="chemical_rif" />
<input type="hidden" value="' . $result["under_allergen"]. '" name="under_allergen" />
<input type="hidden" value="' . $result["eco_info6"]. '" name="eco_info6" />
<input type="hidden" value="' . $result["disposal_considerations1"]. '" name="disposal_considerations1" />
<input type="hidden" value="' . $result["disposal_considerations2"]. '" name="disposal_considerations2" />
<input type="hidden" value="' . $result["disposal_considerations3"]. '" name="disposal_considerations3" />
<input type="hidden" value="' . $result["disposal_considerations4"]. '" name="disposal_considerations4" />
<input type="hidden" value="' . $result["disposal_considerations5"]. '" name="disposal_considerations5" />
<input type="hidden" value="' . $result["disposal_considerations6"]. '" name="disposal_considerations6" />
<input type="hidden" value="' . $result["transport_info1"]. '" name="transport_info1" />
<input type="hidden" value="' . $result["transport_info2"]. '" name="transport_info2" />
<input type="hidden" value="' . $result["transport_info3"]. '" name="transport_info3" />
<input type="hidden" value="' . $result["transport_info4"]. '" name="transport_info4" />
<input type="hidden" value="' . $result["transport_info5"]. '" name="transport_info5" />
<input type="hidden" value="' . $result["transport_info6"]. '" name="transport_info6" />
<input type="hidden" value="' . $result["reg_info1"]. '" name="reg_info1" />
<input type="hidden" value="' . $result["reg_info2"]. '" name="reg_info2" />
<input type="hidden" value="' . $result["reg_info3"]. '" name="reg_info3" />
<input type="hidden" value="' . $result["reg_info4"]. '" name="reg_info4" />
<input type="hidden" value="' . $result["reg_info5"]. '" name="reg_info5" />
<input type="hidden" value="' . $result["reg_info6"]. '" name="reg_info6" />
<input type="hidden" value="' . $result["other_info1"]. '" name="other_info1" />
<input type="hidden" value="' . $result["other_info2"]. '" name="other_info2" />
<input type="hidden" value="' . $result["other_info3"]. '" name="other_info3" />
<input type="hidden" value="' . $result["other_info4"]. '" name="other_info4" />
<input type="hidden" value="' . $result["other_info5"]. '" name="other_info5" />
<input type="hidden" value="' . $result["other_info6"]. '" name="other_info6" />
<input type="hidden" value="' . $result["stability_and_reactivity"]. '" name="stability_and_reactivity" />
<input type="hidden" value="' . $result["toxilogical_information"]. '" name="toxilogical_information" />
<input type="hidden" value="' . $result["ecological_information"]. '" name="ecological_information" />
<input type="hidden" value="' . $result["disposal_considerations"]. '" name="disposal_considerations" />
<input type="hidden" value="' . $result["transport_information"]. '" name="transport_information" />
<input type="hidden" value="' . $result["regulatory_information"]. '" name="regulatory_information" />
<input type="hidden" value="' . $result["other_information"]. '" name="other_information" />
<input type="hidden" value="' . $result["vinegar_acidity"]. '" name="vinegar_acidity" />
';

$content .= '<input type="hidden" value="' . utf8_encode($result["vinegar_dry"]) . '" name="vinegar_dry" />';
$content .= '<input type="hidden" value="' . utf8_encode($result["vinegar_gravity"]) . '" name="vinegar_gravity" />';

$content .= '
<input type="hidden" value="' . $result["vinegar_ph"]. '" name="vinegar_ph" />
<input type="hidden" value="' . $result["vinegar_sulphurous"]. '" name="vinegar_sulphurous" />
<input type="hidden" value="' . $result["vinegar_metals"]. '" name="vinegar_metals" />
<input type="hidden" value="' . $result["vinegar_matters"]. '" name="vinegar_matters" />
<input type="hidden" value="' . $result["vinegar_color"]. '" name="vinegar_color" />
<input type="hidden" value="' . $result["vinegar_flavor"]. '" name="vinegar_flavor" />
<input type="hidden" value="' . $result["vinegar_odour"]. '" name="vinegar_odour" />
<input type="hidden" value="' . $result["vinegar_appearance"]. '" name="vinegar_appearance" />
<input type="hidden" value="' . $result["vinegar_microbiological"]. '" name="vinegar_microbiological" />
';

$content .= '<input type="hidden" value="' . utf8_encode($result["vinegar_density"]) . '" name="vinegar_density" />';
$content .= '<input type="hidden" value="' . utf8_encode($result["organoleptic_appearance"]) . '" name="organoleptic_appearance" />';

$content .'
<input type="hidden" value="' . $result["organoleptic_flavor"]. '" name="organoleptic_flavor" />
<input type="hidden" value="' . $result["organoleptic_color_red"]. '" name="organoleptic_color_red" />
<input type="hidden" value="' . $result["organoleptic_color_yellow"]. '" name="organoleptic_color_yellow" />
<input type="hidden" value="' . $result["organoleptic_color"]. '" name="organoleptic_color" />
';

$content .= '<input type="hidden" value="' . utf8_encode($result["typical_fatty_acid"]) . '" name="typical_fatty_acid" />';
$content .= '<input type="hidden" value="' . $result["typical_moisture2"] . '" name="typical_moisture2" />';

$content .= '
<input type="hidden" value="' . $result["typical_iodine"]. '" name="typical_iodine" />
<input type="hidden" value="' . $result["typical_saponification"]. '" name="typical_saponification" />
<input type="hidden" value="' . $result["typical_anisidine"]. '" name="typical_anisidine" />
';

$content .= '<input type="hidden" value="' . utf8_encode($result["typical_cold"]) . '" name="typical_cold" />';
$content .= '<input type="hidden" value="' . utf8_encode($result["typical_refractive"]) . '" name="typical_refractive" />';
$content .= '<input type="hidden" value="' . utf8_encode($result["typical_stability"]) . '" name="typical_stability" />';
$content .= '<input type="hidden" value="' . utf8_encode($result["typical_smoke"]) . '" name="typical_smoke" />';

$content . '
<input type="hidden" value="' . $result["typical_additives"]. '" name="typical_additives" />
<input type="hidden" value="' . $result["vinegar_nutrition_fat_100g"]. '" name="vinegar_nutrition_fat_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_fat_100ml"]. '" name="vinegar_nutrition_fat_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_sodium_100g"]. '" name="vinegar_nutrition_sodium_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_sodium_100ml"]. '" name="vinegar_nutrition_sodium_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_carb_100g"]. '" name="vinegar_nutrition_carb_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_carb_100ml"]. '" name="vinegar_nutrition_carb_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_sugars_100ml"]. '" name="vinegar_nutrition_sugars_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_sugars_100g"]. '" name="vinegar_nutrition_sugars_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_protein_100g"]. '" name="vinegar_nutrition_protein_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_protein_100ml"]. '" name="vinegar_nutrition_protein_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_kcal_100ml"]. '" name="vinegar_nutrition_kcal_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_kcal_100g"]. '" name="vinegar_nutrition_kcal_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_calories_100g"]. '" name="vinegar_nutrition_calories_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_calories_100ml"]. '" name="vinegar_nutrition_calories_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_calories_fat_100ml"]. '" name="vinegar_nutrition_calories_fat_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_calories_fat_100g"]. '" name="vinegar_nutrition_calories_fat_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_vitamin_a_100ml"]. '" name="vinegar_nutrition_vitamin_a_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_vitamin_a_100g"]. '" name="vinegar_nutrition_vitamin_a_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_vitamin_c_100g"]. '" name="vinegar_nutrition_vitamin_c_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_vitamin_c_100ml"]. '" name="vinegar_nutrition_vitamin_c_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_calcium_100ml"]. '" name="vinegar_nutrition_calcium_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_calcium_100g"]. '" name="vinegar_nutrition_calcium_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_iron_100g"]. '" name="vinegar_nutrition_iron_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_iron_100ml"]. '" name="vinegar_nutrition_iron_100ml" />
<input type="hidden" value="' . $result["other_product_info1"]. '" name="other_product_info1" />
<input type="hidden" value="' . $result["other_product_info2"]. '" name="other_product_info2" />
<input type="hidden" value="' . $result["other_product_info3"]. '" name="other_product_info3" />
<input type="hidden" value="' . $result["other_product_info4"]. '" name="other_product_info4" />
<input type="hidden" value="' . $result["other_product_info5"]. '" name="other_product_info5" />
<input type="hidden" value="' . $result["other_product_info6"]. '" name="other_product_info6" />
<input type="hidden" value="' . $result["vinegar_nutrition_cholesterol_100g"]. '" name="vinegar_nutrition_cholesterol_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_cholesterol_100ml"]. '" name="vinegar_nutrition_cholesterol_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_fiber_100g"]. '" name="vinegar_nutrition_fiber_100g" />
<input type="hidden" value="' . $result["vinegar_nutrition_fiber_100ml"]. '" name="vinegar_nutrition_fiber_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_moisture_100ml"]. '" name="vinegar_nutrition_moisture_100ml" />
<input type="hidden" value="' . $result["vinegar_nutrition_moisture_100g"]. '" name="vinegar_nutrition_moisture_100g" />
<input type="hidden" value="' . $result["typical_erucic"]. '" name="typical_erucic" />
<input type="hidden" value="' . $result["typical_moisture2"]. '" name="typical_moisture2" />
<input type="hidden" value="' . $result["developed_alcohol_degree"]. '" name="developed_alcohol_degree" />
<input type="hidden" value="' . $result["ashes"]. '" name="ashes" />
<input type="hidden" value="' . $result["reduced_dry_extract"]. '" name="reduced_dry_extract" />
<input type="hidden" value="' . $result["reducing_sugars"]. '" name="reducing_sugars" />
<input type="hidden" value="' . $result["antioxidant"]. '" name="antioxidant" />
<input type="hidden" value="' . $result["vinegar_microbiological"]. '" name="vinegar_microbiological" />
';
