<?php
/**
 * @package Cibaria Specsheets
 */
/*
Plugin Name: Cibaria Specsheets Connection
Plugin URI: cibaria-intl.com/specsheets
Description: This is used to connect the Cibaria Specsheets, specsheets with the cibaria-intl.com website
Version: 0.1.0
Author: Cibaria International
Author URI: https://www.cibaria-intl.com
License: GPLv2 or later
Text Domain: Cibaria Specsheets
*/

function CibariaSpecsheets() {

global $content;

//Get Guzzle Composer Library
require('vendor/autoload.php');
//load wordpress
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');

$client = new GuzzleHttp\Client();

//get json
$body = $client->get('https://www.cibariainternational.com/api/specsheets')->getBody()->getContents();

$response = json_decode($body, true);

$content .= '</div></div>';

$content .= '<table class="table table-striped" id="documentationTable">
    <thead>
    <tr>
        <th>Title</th>
        <th>Description</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
<tbody>';

foreach($response as $result) :
$content .= '<tr>';
include('_form.php');

$content .= '<td><strong>' . $result['title'] . '</strong></td>';
$content .= '<td>' . substr($result['description'], 0,150) . '...' . '</td>';
$content .= '<td valign="center">';
$content .= '<input type="image" src="' . get_template_directory_uri() . '/img/view.jpg" class="" style="padding-top:8px;">';
$content .= '</td></tr></form>';

endforeach;

$content .= '</tbody></table>';

return $content;
}

add_shortcode('specsheets', 'CibariaSpecsheets');