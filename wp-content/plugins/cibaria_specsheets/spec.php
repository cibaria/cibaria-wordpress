<?php //load wordpress
require($_SERVER['DOCUMENT_ROOT'].'/wp-load.php'); ?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Specsheet</title>
    <link media="all" type="text/css" rel="stylesheet" href="<?php echo plugins_url( 'css/bootstrap.css', __FILE__ ); ?>"/>
    <link media="print" type="text/css" rel="stylesheet" href="<?php echo plugins_url('css/print.css', __FILE__); ?>"/>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
    <script type="text/javascript" src="<?php echo plugins_url( 'js/html2canvas.js?1401228710', __FILE__ ); ?>"></script>
    <script type="text/javascript" src="<?php echo plugins_url( 'js/jquery.plugin.html2canvas.js?1401228719', __FILE__ ); ?>"></script>
    <style>
        body {
            margin: 40px;
            -webkit-print-color-adjust: exact;
            font-size: 13px;
        }
        table {
            font-size: 13px;
        }
    </style>
</head>
<body>
<!-- turn canvas into .png -->
<script type="text/javascript">
    function capture() {
        $('#target').html2canvas({
            onrendered: function (canvas) {
                //Set hidden field's value to image data (base-64 string)
                $('#img_val').val(canvas.toDataURL("image/png"));
                //Submit the form manually
                document.getElementById("myForm").submit();
            }
        });
    }
</script>
<div class="container">
<div class="span12">
</div>
<div class="span12">
<br/>
<div>
<div style="float:left; width:178px; margin-left:80px;">
    <img width="178" height="157" src="<?php echo plugins_url('img/cibaria-logo.jpg',  __FILE__ ); ?>" alt=""/></div>
<!--end logo -->
<div style="float:left; width:178px; margin-left:55px; margin-top:60px;">
    <strong>705 Columbia Ave, <br/>
        Riverside, CA 92507<br/>
        (P) (951) 823-8490<br/>
        (F) (951) 823-8495<br/>
        (E) <a href="mailto:info@cibaria-intl.com">info@cibaria-intl.com</a>
    </strong>
</div>
<!-- end contact info -->

<div style="float:left; margin-left:55px;margin-top:65px;">
    <h2><u>Specification Sheet</u></h2>
    Label Ingredients Statement:
    <h1><?php if (isset($_REQUEST['title'])) {
	echo htmlentities($_REQUEST['title']);
}
?></h1>
</div>
<!--end page title -->

<!-- color line -->
<div style="clear:both"></div>
<div style="height:5px; background-color:#97b854; margin-top:20px; display:inherit;"></div>
<br/>
<p><u><strong>Product Description:</strong></u>
    <?php if (isset($_REQUEST['description'])) {
	echo htmlentities($_REQUEST['description']);
}
?>
</p>
<div style="width:100%; padding-top:10px; padding-bottom:80px; margin-left:100px;"><!-- bullet points -->
    <div style="float:left;">
        <ul>
            <?php if (isset($_REQUEST['bullet_point1']) && !(empty($_REQUEST['bullet_point1']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point1']);?>
                </li>
            <?php }
?>
            <?php if (isset($_REQUEST['bullet_point2']) && !(empty($_REQUEST['bullet_point2']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point2']);?>
                </li>
            <?php }
?>
            <?php if (isset($_REQUEST['bullet_point3']) && !(empty($_REQUEST['bullet_point3']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point3']);?>
                </li>
            <?php }
?>

            <?php if (isset($_REQUEST['bullet_point4']) && $_REQUEST['bullet_point4'] != "") {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point4']);?>
                </li>
            <?php }
?>

        </ul>
    </div>
    <div style="float:left">
        <ul>
            <?php if (isset($_REQUEST['bullet_point5']) && !(empty($_REQUEST['bullet_point5']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point5']);?>
                </li>
            <?php }
?>

            <?php if (isset($_REQUEST['bullet_point6']) && !(empty($_REQUEST['bullet_point6']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point6']);?>
                </li>
            <?php }
?>
            <?php if (isset($_REQUEST['bullet_point7']) && !(empty($_REQUEST['bullet_point7']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point7']);?>
                </li>
            <?php }
?>


            <?php if (isset($_REQUEST['bullet_point8']) && !(empty($_REQUEST['bullet_point8']))) {?>
                <li>
                    <?php echo htmlentities($_REQUEST['bullet_point8']);?>
                </li>
            <?php }
?>
        </ul>
    </div>
</div>
<!-- end bullet points -->

<!-- middle section -->
<div style="clear:both"></div>
<br/>
<div><!-- middle container -->
    <div style="border: 1px solid #97b854; width:50%; padding:5px; height:350px; float:left;"><!-- allergen disclosure -->
        <h3 style="text-align:center;"><u>Allergen Disclosure</u></h3>
        <p style="padding:10px;">Statement:
            <?php if (isset($_REQUEST['title'])) {
	echo htmlentities($_REQUEST['title']);
}

?> does not contain any of the following allergens, sensitive ingredients, or restricted ingredients:</p>
        <!-- bullet list -->

        <!-- Allergen Disclosure Oil-->
        <?php if (isset($_REQUEST['is_oil_vinegar']) && $_REQUEST['is_oil_vinegar'] == "1") {?>
            <div style="padding:25px;">
                <div style="float:left; width:48%;">
                    Peanut/peanut products<br/>
                    Egg/egg products<br/>
                    Shellfish/shellfish products<br/>
                    Nuts/nut products<br/>
                    Other legumes<br/>
                    Gluten<br/>
                    Animal fat/oil<br/>
                    Hydrolyzed vegetable protein<br/>
                    BMA<br/>
                    FD&C colors<br/>
                    Food Starch<br/>
                </div>

                <div style="float:left; width:48%;">
                    Milk/milk products<br/>
                    Fish/fish products<br/>
                    Mollusks<br/>
                    Soy/soybeans/soy products<br/>
                    Wheat/wheat products<br/>
                    Lecithin<br/>
                    Hydrolyzed animal protein<br/>
                    Yeast Extract<br/>
                    Monosodium Glutamate<br/>
                    BHT<br/>
                    Natural Colors<br/>
                    Maltodextrin<br/>
                </div>
            </div>
            <!-- end Allergen Discloser Oil -->
        <?php } else {?>

            <!-- Allergen Discloser Vinegar -->
            <div style="padding:25px;">
                <div style="float:left; width:48%;">
                    Peanut/peanut products<br/>
                    Egg/egg products<br/>
                    Shellfish/shellfish products<br/>
                    Nuts/nut products<br/>
                    Other legumes<br/>
                    Gluten<br/>
                    Animal fat/oil<br/>
                    Hydrolyzed vegetable protein<br/>
                    BMA<br/>
                    FD&C colors<br/>
                    Food Starch<br/>
                </div>
                <div style="float:left; width:48%;">
                    Milk/milk products<br/>
                    Fish/fish products<br/>
                    Mollusks<br/>
                    Soy/soybeans/soy products<br/>
                    Wheat/wheat products<br/>
                    Lecithin<br/>
                    Hydrolyzed animal protein<br/>
                    Yeast Extract<br/>
                    Monosodium Glutamate<br/>
                    BHT<br/>
                    Natural Colors<br/>
                    Maltodextrin<br/>
                </div>
            </div>
        <?php }
?>
        <!-- end Allergen Discloser Vinegar -->

        <?php if (isset($_REQUEST['is_oil_vinegar']) && $_REQUEST['is_oil_vinegar'] == "1") { ?>
            <br />
            <div style="margin-top:225px;">
                <p style="width: 480px;word-wrap: break-word;">
                    <?php if ($_REQUEST['under_allergen']) {
		                      echo htmlentities($_REQUEST['under_allergen']);
	                       }
                	?>
                </p>
            </div>
        <?php } ?>
    </div><!-- end allergen disclosure-->

    <div style="float:right; width:38%;">
        <img width="200" height="400" src="https://www.cibariainternational.com/images/nutrition/<?php if (isset($_REQUEST['nutrition'])) { echo htmlentities($_REQUEST['nutrition']);
}

?>" alt=""/>
        <?php if (isset($_REQUEST['is_oil_vinegar']) && $_REQUEST['is_oil_vinegar'] == "1") {
	?>
            <p style="margin-left:65px;">
                <?php if (isset($_REQUEST['nutrition_fact1'])) {
		                  echo htmlentities($_REQUEST['nutrition_fact1']);

	           }

	           ?>
            </p>

            <p style="margin-left:65px;">
                <?php if (isset($_REQUEST['nutrition_fact2'])) {
                        echo htmlentities($_REQUEST['nutrition_fact2']);
	                   }
	            ?>
            </p>
            <p style="margin-left:65px;">
                <?php if (isset($_REQUEST['nutrition_fact3'])) {
		              echo htmlentities($_REQUEST['nutrition_fact3']);
                    	}
            	?>
            </p>

        <?php } else {echo "";}
?>
    </div>
    <!--end nutrition facts-->

</div>
<!-- end middle container -->
<div style="clear:both;"></div>
<div><!-- nutrition facts -->
</div>
</div>
<!-- end middle container -->
<div style="clear:both;"></div>
<br/>
<p><strong><u>Storage</u></strong>:
    <?php if (isset($_REQUEST['storage'])) {
	echo htmlentities($_REQUEST['storage']);
}

?>

</p>

<p><strong><u>Shelf Life</u></strong>:
    <?php if (isset($_REQUEST['shelf_life'])) {
	echo htmlentities($_REQUEST['shelf_life']);
}
?>

</p>



<p><strong><u>Sewer Sludge and Irradiation Statement</u></strong>:
    <?php if (isset($_REQUEST['sewer'])) {
	echo htmlentities($_REQUEST['sewer']);
}
?>

</p>



<p><strong><u>Applications For Product</u></strong>:
    <?php if (isset($_REQUEST['applications'])) {
	echo htmlentities($_REQUEST['applications']);
}
?>
</p>



<p><strong><u>Country of Origin</u></strong>:
    <?php if (isset($_REQUEST['country'])) {
	echo htmlentities($_REQUEST['country']);
}
?>
</p>
<br/>
<br/>
<!--2nd page-->
<div>
<div class="page"></div>
<div style="float:left;">
<?php if (isset($_REQUEST['is_oil_vinegar']) && $_REQUEST['is_oil_vinegar'] == "1") { ?>
<h3 style="text-align:center;">USDA NDB (National Nutrition Database)</h3>
<table width="390" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
<tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
    <td>Nutrient</td>
    <td>Unit</td>
    <td>Value per 100.0g</td>
    <td>Tbsp 13.5g</td>
</tr>
<tr>
    <td colspan="4" bgcolor="#d6e3bc"> Proximates</td>
</tr>
<tr>
    <td>Water</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['water_value'])) {
		echo htmlentities($_REQUEST['water_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['water_tbsp'])) {
		echo htmlentities($_REQUEST['water_tbsp']);
	}
	?></td>
</tr>

<tr>
    <td>Energy</td>
    <td>kcal</td>
    <td><?php if (isset($_REQUEST['energy_value'])) {
		echo htmlentities($_REQUEST['energy_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['energy_tbsp'])) {
		echo htmlentities($_REQUEST['energy_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Protein</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['protein_value'])) {
		echo htmlentities($_REQUEST['protein_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['protein_tbsp'])) {
		echo htmlentities($_REQUEST['protein_tbsp']);
	}
	?></td>

</tr>

<tr>
    <td>Total lipid (fat)</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['totallipid_value'])) {
		echo htmlentities($_REQUEST['totallipid_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['totallipid_tbsp'])) {
		echo htmlentities($_REQUEST['totallipid_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Carbohydrate, by difference</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['carbohydrate_value'])) {
		echo htmlentities($_REQUEST['carbohydrate_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['carbohydrate_tbsp'])) {
		echo htmlentities($_REQUEST['carbohydrate_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Fiber, total dietary</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['fiber_value'])) {
		echo htmlentities($_REQUEST['fiber_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['fiber_tbsp'])) {
		echo htmlentities($_REQUEST['fiber_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Sugars, total</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['sugars_value'])) {
		echo htmlentities($_REQUEST['sugars_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['sugars_tbsp'])) {
		echo htmlentities($_REQUEST['sugars_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td colspan="4" bgcolor="#d6e3bc">Minerals</td>
</tr>
<tr>
    <td>Calcium, Ca</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['calcium_value'])) {
		echo htmlentities($_REQUEST['calcium_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['calcium_tbsp'])) {
		echo htmlentities($_REQUEST['calcium_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Iron, Fe</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['iron_value'])) {
		echo htmlentities($_REQUEST['iron_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['iron_tbsp'])) {
		echo htmlentities($_REQUEST['iron_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Magnesium, Mg</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['magnesium_value'])) {
		echo htmlentities($_REQUEST['magnesium_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['magnesium_tbsp'])) {
		echo htmlentities($_REQUEST['magnesium_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Phosphorus, P</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['phosphorus_value'])) {
		echo htmlentities($_REQUEST['phosphorus_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['phosphorus_tbsp'])) {
		echo htmlentities($_REQUEST['phosphorus_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Potassium, K</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['potassium_value'])) {
		echo htmlentities($_REQUEST['potassium_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['potassium_tbsp'])) {
		echo htmlentities($_REQUEST['potassium_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Sodium, Na</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['sodium_value'])) {
		echo htmlentities($_REQUEST['sodium_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['sodium_tbsp'])) {
		echo htmlentities($_REQUEST['sodium_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Zinc, Zn</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['zinc_value'])) {
		echo htmlentities($_REQUEST['zinc_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['zinc_tbsp'])) {
		echo htmlentities($_REQUEST['zinc_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td colspan="4" bgcolor="#d6e3bc">Vitamins</td>
</tr>
<tr>
    <td>Vitamin C, total ascorbic acid</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['vitaminc_value'])) {
		echo htmlentities($_REQUEST['vitaminc_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitaminc_tbsp'])) {
		echo htmlentities($_REQUEST['vitaminc_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Thiamin</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['thiamin_value'])) {
		echo htmlentities($_REQUEST['thiamin_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['thiamin_tbsp'])) {
		echo htmlentities($_REQUEST['thiamin_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Riboflavin</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['riboflavin_value'])) {
		echo htmlentities($_REQUEST['riboflavin_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['riboflavin_tbsp'])) {
		echo htmlentities($_REQUEST['riboflavin_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Niacin</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['niacin_value'])) {
		echo htmlentities($_REQUEST['niacin_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['niacin_tbsp'])) {
		echo htmlentities($_REQUEST['niacin_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin B-6</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['vitaminb6_value'])) {
		echo htmlentities($_REQUEST['vitaminb6_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitaminb6_tbsp'])) {
		echo htmlentities($_REQUEST['vitaminb6_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Folate, DFE</td>
    <td>µg</td>
    <td><?php if (isset($_REQUEST['folate_value'])) {
		echo htmlentities($_REQUEST['folate_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['folate_tbsp'])) {
		echo htmlentities($_REQUEST['folate_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin B-12</td>
    <td>µg</td>
    <td><?php if (isset($_REQUEST['vitaminb12_value'])) {
		echo htmlentities($_REQUEST['vitaminb12_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitaminb12_tbsp'])) {
		echo htmlentities($_REQUEST['vitaminb12_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin A, RAE</td>
    <td>µg</td>
    <td><?php if (isset($_REQUEST['vitaminar_value'])) {
		echo htmlentities($_REQUEST['vitaminar_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitaminar_tbsp'])) {
		echo htmlentities($_REQUEST['vitaminar_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin A, IU</td>
    <td>IU</td>
    <td><?php if (isset($_REQUEST['vitaminai_value'])) {
		echo htmlentities($_REQUEST['vitaminai_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitaminai_tbsp'])) {
		echo htmlentities($_REQUEST['vitaminai_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin E (alpha-tocopherol)</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['vitamine_value'])) {
		echo htmlentities($_REQUEST['vitamine_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitamine_tbsp'])) {
		echo htmlentities($_REQUEST['vitamine_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin D (D2+D3)</td>
    <td>µg</td>
    <td><?php if (isset($_REQUEST['vitamind2_value'])) {
		echo htmlentities($_REQUEST['vitamind2_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitamind2_tbsp'])) {
		echo htmlentities($_REQUEST['vitamind2_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin D</td>
    <td>IU</td>
    <td><?php if (isset($_REQUEST['vitamind_value'])) {
		echo htmlentities($_REQUEST['vitamind_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitamind_tbsp'])) {
		echo htmlentities($_REQUEST['vitamind_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Vitamin K (phylloquinone)</td>
    <td>µg</td>
    <td><?php if (isset($_REQUEST['vitamink_value'])) {
		echo htmlentities($_REQUEST['vitamink_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['vitamink_tbsp'])) {
		echo htmlentities($_REQUEST['vitamink_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td colspan="4" bgcolor="#d6e3bc">Lipids</td>
</tr>
<tr>
    <td>Fatty acids, total saturated</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['fattysaturated_value'])) {
		echo htmlentities($_REQUEST['fattysaturated_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['fattysaturated_tbsp'])) {
		echo htmlentities($_REQUEST['fattysaturated_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Fatty acids, total monounsaturated</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['fattymonoun_value'])) {
		echo htmlentities($_REQUEST['fattymonoun_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['fattymonoun_tbsp'])) {
		echo htmlentities($_REQUEST['fattymonoun_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Fatty acids, polyunsaturated</td>
    <td>g</td>
    <td><?php if (isset($_REQUEST['fattypoly_value'])) {
		echo htmlentities($_REQUEST['fattypoly_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['fattypoly_tbsp'])) {
		echo htmlentities($_REQUEST['fattypoly_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td>Cholesterol</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['cholesterol_value'])) {
		echo htmlentities($_REQUEST['cholesterol_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['cholesterol_tbsp'])) {
		echo htmlentities($_REQUEST['cholesterol_tbsp']);
	}
	?></td>
</tr>
<tr>
    <td colspan="4" bgcolor="#d6e3bc">Other</td>
</tr>
<tr>
    <td>Caffeine</td>
    <td>mg</td>
    <td><?php if (isset($_REQUEST['caffeine_value'])) {
		echo htmlentities($_REQUEST['caffeine_value']);
	}
	?></td>
    <td><?php if (isset($_REQUEST['caffeine_tbsp'])) {
		echo htmlentities($_REQUEST['caffeine_tbsp']);
	}
	?></td>
</tr>
</table>
</div>
<!--end usda-->

<?php } else { ?>
<!--vinegar nutrition facts-->
<h3 style="text-align:center;">Nutrition Facts</h3>
<table width="390" border="1" bordercolor="#97b854" cellpadding="5" cellspacing="0">
    <tr bgcolor="#97b854" style="padding:10px; text-align:center;color:white;">
        <td>Nutrient</td>
        <td>Unit</td>
        <td>Value per 100g</td>
        <td>Value per 100ml</td>
    </tr>
    <tr>
        <td>Total Fat</td>
        <td>g</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_fat_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_fat_100g']);
	}
	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_fat_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_fat_100ml']);
	}
	?></td>
    </tr>
    <tr>
        <td>Sodium</td>
        <td>mg</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_sodium_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_sodium_100g']);
	}
	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_sodium_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_sodium_100ml']);
	}
	?></td>
    </tr>
    <tr>
        <td>Total Carbohydrates</td>
        <td>g</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_sodium_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_sodium_100ml']);
	}
	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_carb_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_carb_100ml']);
	}
	?></td>
    </tr>
    <tr>
        <td>Sugars</td>
        <td>g</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_sugars_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_sugars_100g']);
	}
	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_sugars_100ml'])) {

		echo htmlentities($_REQUEST['vinegar_nutrition_sugars_100ml']);

	}

	?></td>

    </tr>

    <tr>

        <td>Protein</td>
        <td>g</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_protein_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_protein_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_protein_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_protein_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Kcal</td>
        <td>kcal</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_kcal_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_kcal_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_kcal_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_kcal_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Calories</td>
        <td>cal</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_calories_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_calories_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_calories_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_calories_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Calories From Fat</td>
        <td>cal</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_calories_fat_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_calories_fat_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_calories_fat_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_calories_fat_100ml']);
	}

	?></td>

    </tr>

    <tr>

        <td>Vitamin A</td>
        <td>%</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_vitamin_a_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_vitamin_a_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_vitamin_a_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_vitamin_a_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Vitamin C</td>
        <td>%</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_vitamin_c_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_vitamin_c_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_vitamin_c_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_vitamin_c_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Calcium</td>
        <td>%</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_calcium_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_calcium_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_calcium_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_calcium_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Iron</td>
        <td>%</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_iron_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_iron_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_iron_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_iron_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Cholesterol</td>
        <td>mg</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_cholesterol_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_cholesterol_100g']);
	}

	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_cholesterol_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_cholesterol_100ml']);
	}

	?></td>
    </tr>
    <tr>
        <td>Fiber</td>
        <td>g</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_fiber_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_fiber_100g']);
	}
	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_fiber_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_fiber_100ml']);
	}
	?></td>
    </tr>
    <tr>
        <td>Moisture</td>
        <td>g</td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_moisture_100g'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_moisture_100g']);
	}
	?></td>
        <td><?php if (isset($_REQUEST['vinegar_nutrition_moisture_100ml'])) {
		echo htmlentities($_REQUEST['vinegar_nutrition_moisture_100ml']);
	}
	?></td>
    </tr>
</table>
</div>

<!--end vinegar nutrition facts-->

<?php } ?>



<?php if (isset($_REQUEST['is_oil_vinegar']) && $_REQUEST['is_oil_vinegar'] == "1") { ?>

<!--vinegar stuff-->

<!--    organoleptic characteristics-->

<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">
    <table width="500" border="0">
        <tr>
            <td colspan="2"><h3><u>Organoleptic Characteristics:</u></h3></td>
        </tr>
        <tr>
            <td>Appearance/Clarity</td>
            <td><?php if (isset($_REQUEST['organoleptic_appearance'])) {
		              echo htmlentities($_REQUEST['organoleptic_appearance']);
	}
	?></td>
        </tr>
        <tr>
            <td>Flavor/Odor</td>
            <td><?php if (isset($_REQUEST['organoleptic_flavor'])) {
		echo htmlentities($_REQUEST['organoleptic_flavor']);
	}
	?></td>
        </tr>
        <tr>
            <td>Color (Lovibond) Red</td>
            <td><?php if (isset($_REQUEST['organoleptic_color_red'])) {
		echo htmlentities($_REQUEST['organoleptic_color_red']);
	}
	?></td>
        </tr>
        <tr>
            <td>Color (Lovibond) Yellow</td>
            <td><?php if (isset($_REQUEST['organoleptic_color_yellow'])) {
		echo htmlentities($_REQUEST['organoleptic_color_yellow']);
	}

	?></td>
        </tr>
    </table></div>

<!--    end organoleptic characteristics-->

<!--    typical analysis range-->
<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">
    <table width="500" border="0">
        <tr>
            <td colspan="2"><h3><u>Typical Analysis Ranges:</u></h3></td>
        </tr>
        <tr>
            <td>Free Fatty Acid (% m/m expressed in oleic acid)</td>
            <td><?php if (isset($_REQUEST['typical_fatty_acid'])) {
		echo htmlentities($_REQUEST['typical_fatty_acid']);
	}
	?></td>
        </tr>
        <tr>
            <td>Moisture</td>
            <td><?php
if (isset($_REQUEST['typical_moisture2'])) {
		echo str_replace("?", "≤", $_REQUEST['typical_moisture2']);
	}
	?></td>
        </tr>
        <tr>
            <td>Peroxide Value</td>
            <td><?php if (isset($_REQUEST['typical_peroxide'])) {
		echo str_replace("?", "≤", $_REQUEST['typical_peroxide']);
	}
	?></td>
        </tr>
        <tr>
            <td>Iodine Value</td>
            <td><?php if (isset($_REQUEST['typical_iodine'])) {
		echo htmlentities($_REQUEST['typical_iodine']);
	}
	?></td>
        </tr>
        <tr>
            <td>Saponification Value</td>
            <td><?php if (isset($_REQUEST['typical_saponification'])) {
		echo htmlentities($_REQUEST['typical_saponification']);
	}
	?></td>
        </tr>
        <tr>
            <td>p-Anisidine Value</td>
            <td><?php if (isset($_REQUEST['typical_anisidine'])) {
		echo htmlentities($_REQUEST['typical_anisidine']);
	}
	?></td>
        </tr>
        <tr>
            <td>Cold Test</td>
            <td><?php if (isset($_REQUEST['typical_cold'])) {
		echo htmlentities($_REQUEST['typical_cold']);
	}
	?></td>
        </tr>
        <tr>
            <td>Refractive Index</td>
            <td>
                <?php if (isset($_REQUEST['typical_gravity'])) {
		echo $_REQUEST['typical_refractive'];
	}
	?>
            </td>
        </tr>
        <tr>
            <td>Specific Gravity</td>
            <td><?php if (isset($_REQUEST['typical_gravity'])) {
		echo htmlentities($_REQUEST['typical_gravity']);
	}
	?></td>
        </tr>
        <tr>
            <td>Oil Stability Index(OSI)</td>
            <td><?php if (isset($_REQUEST['typical_stability'])) {
		echo htmlentities($_REQUEST['typical_stability']);
	}
	?></td>
        </tr>
        <tr>
            <td>Smoke Point</td>
            <td><?php if (isset($_REQUEST['typical_smoke'])) {
		echo htmlentities($_REQUEST['typical_smoke']);
	}
	?></td>
        </tr>
        <tr>
            <td>Additives</td>
            <td><?php if (isset($_REQUEST['typical_additives'])) {
		echo htmlentities($_REQUEST['typical_additives']);
	}
	?></td>
        </tr>
    </table></div>
<!--    end typical analysis range-->

<!--typical fatty acid-->
<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">
    <table width="500" border="0">
        <tr>
            <td colspan="2"><h3><u>Typical Fatty Acid Ranges:</u></h3></td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="700" border="0">
                    <tr>
                        <td width="125">C 14:0</td>
                        <td width="263">Myristic acid</td>
                        <td width="298"><?php if (isset($_REQUEST['typical_myristic'])) {
		echo htmlentities($_REQUEST['typical_myristic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 16:0</td>
                        <td>Palmitic Acid</td>
                        <td><?php if (isset($_REQUEST['typical_palmitic'])) {
		echo htmlentities($_REQUEST['typical_palmitic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 16:1</td>
                        <td>Palmitoleic Acid</td>
                        <td><?php if (isset($_REQUEST['typical_palmitoleic'])) {
		echo htmlentities($_REQUEST['typical_palmitoleic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 17:0</td>
                        <td>Heptadecanoic Acid</td>
                        <td><?php if (isset($_REQUEST['typical_heptadecanoic'])) {
		echo htmlentities($_REQUEST['typical_heptadecanoic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 17:1</td>
                        <td>Heptadecenoic acid</td>
                        <td><?php if (isset($_REQUEST['typical_heptadecenoic'])) {
		echo htmlentities($_REQUEST['typical_heptadecenoic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 18:0</td>
                        <td>Stearic acid</td>
                        <td><?php if (isset($_REQUEST['typical_stearic'])) {
		echo htmlentities($_REQUEST['typical_stearic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 18:1</td>
                        <td>Oleic acid</td>
                        <td><?php if (isset($_REQUEST['typical_oleic'])) {
		echo htmlentities($_REQUEST['typical_oleic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 18:2</td>
                        <td>Linoleic acid</td>
                        <td><?php if (isset($_REQUEST['typical_linoleic'])) {
		echo htmlentities($_REQUEST['typical_linoleic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 18:3</td>
                        <td>Linolenic acid</td>
                        <td>
                            <?php if (isset($_REQUEST['typical_linolenic'])) {
		$typical_linolenic = $_REQUEST['typical_linolenic'];
		//TODO: REFACTOR BUT IT WORKS
		$result = str_replace(array("?:", "?:?"), array("a:", "y:≤"), $typical_linolenic);
		echo str_replace("a:?", "γ:≤", $result);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 20:0</td>
                        <td>Arachidic acid</td>
                        <td><?php if (isset($_REQUEST['typical_arachidic'])) {
		echo htmlentities($_REQUEST['typical_arachidic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 20:1</td>
                        <td>Gadoleic acid (eicosenoic)</td>
                        <td><?php if (isset($_REQUEST['typical_gadoleic'])) {
		echo htmlentities($_REQUEST['typical_gadoleic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 22:0</td>
                        <td>Behenic acid</td>
                        <td><?php if (isset($_REQUEST['typical_behenic'])) {
		echo htmlentities($_REQUEST['typical_behenic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 22:1</td>
                        <td>Erucic Acid</td>
                        <td><?php if (isset($_REQUEST['typical_erucic'])) {
		echo htmlentities($_REQUEST['typical_erucic']);
	}
	?></td>
                    </tr>
                    <tr>
                        <td>C 24:0</td>
                        <td>Lignoceric Acid</td>
                        <td><?php if (isset($_REQUEST['typical_lignoceric'])) {
		echo htmlentities($_REQUEST['typical_lignoceric']);
	}
	?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">
    <h3><u>Registrations and Other Product Information:</u></h3>
    <br/>
    <h4 style="text-align: left; padding-left:70px;">CAS <?php if (isset($_REQUEST['certifications_cas'])) {
		echo htmlentities($_REQUEST['certifications_cas']);
	}
	?></h4>
    <h4 style="text-align: left; padding-left:70px;">EINCS <?php if (isset($_REQUEST['certifications_einecs'])) {
		echo htmlentities($_REQUEST['certifications_einecs']);
	}
	?></h4>
    <h4 style="text-align: left; padding-left:70px;">INCL: <?php if (isset($_REQUEST['certifications_incl'])) {
		echo htmlentities($_REQUEST['certifications_incl']);
	}
	?></h4>
    <h4 style="text-align: left; padding-left:70px;"><?php if (isset($_REQUEST['certifications_notes'])) {
		echo htmlentities($_REQUEST['certifications_notes']);
	}
	?></h4>
</div>
</div>
<br />
<br />
<div style="float:right;margin-top:15px;width:50%;">
</div>
<!--end-->
<?php } ?>

<?php if (isset($_REQUEST['is_oil_vinegar']) && $_REQUEST['is_oil_vinegar'] == "2") { ?>

<!--vinegar stuff-->

<!--    organoleptic characteristics-->
<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">
    <table width="500" border="0">
        <tr>
            <td colspan="2"><h3><u>Organoleptic Characteristics:</u></h3></td>
        </tr>
        <tr>
            <td>Color</td>
            <td><?php if (isset($_REQUEST['organoleptic_color'])) {
		echo htmlentities($_REQUEST['organoleptic_color']);
	}
	?></td>
        </tr>
        <tr>
            <td>Bouquet/Odor</td>
            <td><?php if (isset($_REQUEST['organoleptic_odor'])) {
		echo htmlentities($_REQUEST['organoleptic_odor']);
	}
	?></td>
        </tr>
        <tr>
            <td>Taste/Flavor</td>
            <td><?php if (isset($_REQUEST['organoleptic_flavor'])) {
		echo htmlentities($_REQUEST['organoleptic_flavor']);
	}
	?></td>
        </tr>
        <tr>
            <td>Appearance/Texture</td>
            <td><?php if (isset($_REQUEST['organoleptic_appearance'])) {
		echo htmlentities($_REQUEST['organoleptic_appearance']);
	}
	?></td>
        </tr>
    </table>
</div>
<!--    end organoleptic characteristics-->

<!--    typical analysis range-->
<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">
    <table width="500" border="0">
        <tr>
            <td colspan="2"><h3><u>Chemical & Physical Characteristics:</u></h3></td>
        </tr>
        <tr>
            <td>Total Acidity</td>
            <td><?php if (isset($_REQUEST['vinegar_acidity'])) {
		echo htmlentities($_REQUEST['vinegar_acidity']);
	}
	?></td>
        </tr>
        <tr>
            <td>PH</td>
            <td><?php if (isset($_REQUEST['vinegar_ph'])) {
		echo htmlentities($_REQUEST['vinegar_ph']);
	}
	?></td>
        </tr>
        <tr>
            <td>Specific Gravity</td>
            <td><?php if (isset($_REQUEST['vinegar_gravity'])) {
		echo htmlentities($_REQUEST['vinegar_gravity']);
	}
	?></td>
        </tr>
        <tr>
            <td>Foreign Matters</td>
            <td><?php if (isset($_REQUEST['vinegar_matters'])) {
		echo htmlentities($_REQUEST['vinegar_matters']);
	}
	?></td>
        </tr>
        <tr>
            <td>Heavy Metals</td>
            <td><?php if (isset($_REQUEST['vinegar_metals'])) {
		echo htmlentities($_REQUEST['vinegar_metals']);
	}
	?></td>
        </tr>
        <tr>
            <td>p-Total Dry Extract</td>
            <td><?php if (isset($_REQUEST['vinegar_dry'])) {
		echo htmlentities($_REQUEST['vinegar_dry']);
	}
	?></td>
        </tr>
        <tr>
            <td>Total Sulphurous Anhydride</td>
            <td><?php if (isset($_REQUEST['vinegar_sulphurous'])) {
		echo htmlentities($_REQUEST['vinegar_sulphurous']);
	}
	?></td>
        </tr>
        <tr>
            <td>Ash</td>
            <td><?php if (isset($_REQUEST['ashes'])) {
		echo htmlentities($_REQUEST['ashes']);
	}
	?></td>
        </tr>
        <tr>
            <td>Grain</td>
            <td><?php if (isset($_REQUEST['chemical_grain'])) {
		echo htmlentities($_REQUEST['chemical_grain']);
	}
	?></td>
        </tr>
        <tr>
            <td>Residual Alcohol</td>
            <td><?php if (isset($_REQUEST['chemical_alcohol'])) {
		echo htmlentities($_REQUEST['chemical_alcohol']);
	}
	?></td>
        </tr>
        <tr>
            <td>Sugar Free Dry Extract</td>
            <td><?php if (isset($_REQUEST['chemical_sugar_free_extract'])) {
		echo htmlentities($_REQUEST['chemical_sugar_free_extract']);
	}
	?></td>
        </tr>
        <tr>
            <td>Density</td>
            <td><?php if (isset($_REQUEST['vinegar_density'])) {
		echo htmlentities($_REQUEST['vinegar_density']);
	}
	?></td>
        </tr>
        <tr>
            <td>Brix</td>
            <td><?php if (isset($_REQUEST['chemical_brix'])) {
		echo htmlentities($_REQUEST['chemical_brix']);
	}
	?></td>
        </tr>
        <tr>
            <td>Developed Alcohol Degree</td>
            <td><?php if (isset($_REQUEST['chemical_alcohol_degree'])) {
		echo htmlentities($_REQUEST['chemical_alcohol_degree']);
	}
	?></td>
        </tr>
        <tr>
            <td>Reduced Dry Extract</td>
            <td><?php if (isset($_REQUEST['chemical_reduced_dry_extract'])) {
		echo htmlentities($_REQUEST['chemical_reduced_dry_extract']);
	}
	?></td>
        </tr>
        <tr>
            <td>Reducing Stars</td>
            <td><?php if (isset($_REQUEST['reducing_sugars'])) {
		echo htmlentities($_REQUEST['reducing_sugars']);
	}
	?></td>
        </tr>
        <tr>
            <td>Rifractometric at 20°C</td>
            <td><?php if (isset($_REQUEST['chemical_rif'])) {
		echo htmlentities($_REQUEST['chemical_rif']);
	}
	?></td>

        </tr>



    </table>

</div>

<!--    end typical analysis range-->



<!--typical fatty acid-->

<div style="float:right;border: 1px solid #97b854; width:50%; padding:15px;">



    <table width="500" border="0">

        <tr>

            <td colspan="2"><h3><u>Other Product Information:</u></h3></td>

        </tr>

        <tr>

            <td colspan="2">

                <table width="700" border="0">

                    <tr>

                        <td><?php if (isset($_REQUEST['other_product_info1'])) {

		echo htmlentities($_REQUEST['other_product_info1']);

	}

	?></td>

                    </tr>

                    <tr>

                        <td><?php if (isset($_REQUEST['other_product_info2'])) {

		echo htmlentities($_REQUEST['other_product_info2']);

	}

	?></td>

                    </tr>

                    <tr>

                        <td><?php if (isset($_REQUEST['other_product_info3'])) {

		echo htmlentities($_REQUEST['other_product_info3']);

	}

	?></td>

                    </tr>

                    <tr>

                        <td><?php if (isset($_REQUEST['other_product_info4'])) {

		echo htmlentities($_REQUEST['other_product_info4']);

	}

	?></td>

                    </tr>

                    <tr>

                        <td><?php if (isset($_REQUEST['other_product_info5'])) {

		echo htmlentities($_REQUEST['other_product_info5']);

	}

	?></td>

                    </tr>

                    <tr>

                        <td><?php if (isset($_REQUEST['other_product_info6'])) {
		                          echo htmlentities($_REQUEST['other_product_info6']);

	                           }

	                   ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

</div>
<!--    end vinegars-->
<br />
<br />
<br />
<br />
<div style="float:right;margin-top:15px;width:50%;">
</div>
<!--    end leaf-->

<!--end-->
<?php } ?>

<br/>
<br/>
<br/>
<div style="clear:both;"></div>
<div style="text-align:center;margin-top:25px;">
    <p>This specification was developed with the utmost care based on up-to-date information available, but should be scrutinized by the recipient. It does not release him or her from checking the quality of goods delivered with proper diligence.
        <br/>
    <hr/>
    <strong>Reviewed: <?php if (isset($_REQUEST['reviewed'])) {
                                  echo htmlentities($_REQUEST['reviewed']);

                               }

                       ?></strong>
    </p>
    <hr/>
</div>

<!-- end second page -->

<!--msds-->



<div class="page"></div>



</div>

<footer>





</footer>

</div>



</body>

</html>

