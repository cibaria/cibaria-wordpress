<?php 
/*
Template Name: Blog
*/

 ?>
<?php echo get_template_part('partials/header-internal-pages'); ?>

<body>
    <?php echo get_template_part('partials/google-analytics'); ?>
    <div class="container">
        <?php echo get_template_part('partials/nav'); ?>
    </div>
    <div class="container" style="background-color:white;">
        <div class="jumbotron" style="height:160px;background-color:white;"></div>
        <div class="jumbotron" style="margin-top:5px;margin-bottom:2px;background-color:white; text-align: center;padding-bottom:20px;">
            <h1 style="color:#999779;border-bottom:1px solid lightgrey;"><?php echo wp_title('', true, ''); ?></h1>
        </div>
        <!-- main content start -->
        <!-- Page Content -->
        <div>
            <div class="row">
                <!-- Blog Entries Column -->
                <div class="col-md-8">
                    <?php 
            //posts
            if ( have_posts() ) : while ( have_posts() ) : the_post() ?>
                    <!-- First Blog Post -->
                    <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>" style="color:#6e6c55;"><?php the_title(); ?></a></h2>
                    <hr>
                    <?php the_post_thumbnail( 'full', ['class' => 'img-responsive']) ?>
                    <p>
                        <?php the_content(); ?>
                    </p>
                    <a class="btn btn-primary" href="<?php the_permalink(); ?>" rel="nofollow">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                    <hr>
                    <?php endwhile; endif; ?>
                    <!-- Pager -->
                    <!--                 <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul> -->
                </div>
                <!-- Blog Sidebar Widgets Column -->
                <div class="col-md-4">
                    <!-- Blog Search Well -->
                    <!-- Blog Categories Well -->
                    <div class="well">
                        <h4>Blog Categories</h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled">
                                    <?php
                                //Categories
                                    $args = array(
                                      'orderby' => 'name',
                                      'parent' => 0
                                      );
                                    $categories = get_categories( $args );
                                    foreach ( $categories as $category ) : ?>
                                        <li>
                                            <a href="<?php echo get_home_url() . '/category/' . $category->slug; ?>" style="color: #6e6c55;">
                                                <?php echo $category->name; ?>
                                            </a>
                                        </li>
                                        <br />
                                        <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- Side Widget Well -->
                    <div class="well">
                        <h4>About</h4>
                        <p>Check here for product updates, industry news, important events, newsworthy articles and everything else we think you might want to know.</p>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!--main content-->
        </div>
    </div>
    </div>
    </div>
    <?php echo get_template_part('partials/footer'); ?>
</body>

</html>