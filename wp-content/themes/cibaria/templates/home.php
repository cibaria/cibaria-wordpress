<?php 
/*
Template Name: Homepage
*/

 ?>

<?php get_template_part('partials/header-homepage'); ?>
<body>
<?php get_template_part('partials/google-analytics'); ?>
<?php get_template_part('partials/nav'); ?>
<?php //get_template_part('partials/parallax/hpparallax'); ?>
<div class="bgimg-1 mobile-no">
		<video autoplay loop muted poster="<?php echo get_bloginfo( 'template_directory' ); ?>img/slide2.jpg">
			<source type="video/webm" src="<?php echo get_bloginfo( 'template_directory' ); ?>/video/homepage-video.webm">
			<source type="video/mp4" src="<?php echo get_bloginfo( 'template_directory' ); ?>/video/homepage-video.mp4">
		</video>
	<div class="caption">
		<h1>Supplying your business with the Materials and Services you need to do business</h1>
		<div class="container">
			<ul class="list-horizontal mobile-no">
				<li>Competitive Prices</li>
				<li>Reliability</li>
				<li>Flexibility</li>
				<li>Knowledgeable Staff</li>
				<li>Stability</li>
				<li>Quick Response</li>
				<li>Go the Extra Mile</li>
				<li>Can-Do Attitude</li>
				<li>Extensive Product Line</li>
				<li>Quality Assurance over and above Standards</li>
			</ul>
		</div>
		<br />

		<!-- <span class="border shake-slow shake-constant">SCROLL DOWN</span> -->
		<span class="border">SCROLL DOWN</span>


	</div>
</div>
<div style="color: #777;background-color:black;text-align:center;padding:50px 80px;text-align: justify;">

	<div class="container">
		<div data-aos="fade-down">
			<h1 style="text-align: center;">Our Products</h1>
		</div><!--end fade down-->
		<br />
		<div class="col-md-11 col-centered blog-main">

			<div class="row">
				<div data-aos="fade-right">
					<div class="col-lg-6 col-md-4 thumb">
						<a class="thumbnail" href="http://www.cibariastoresupply.com" title="Cibaria Store Supply">
							<img class="img-responsive " src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/wholesale.jpg"
							     alt="Wholesale Olive Oils and Vinegars">
						</a>
					</div>
				</div><!--end fade-->

				<div data-aos="fade-left">
					<div class="col-lg-6 col-md-4 thumb">
						<a class="thumbnail" href="http://www.cibariasoapsupply.com/shop/">
							<img class="img-responsive" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/soap.jpg" alt="Soap Making Supplies">
						</a>
					</div>
				</div><!--end fade left-->
			</div><!--end row-->

			<div class="row">
				<div data-aos="fade-right">
					<div class="col-lg-6 col-md-4 thumb">
						<a class="thumbnail" href="" title="Industrial Bulk Capabilities">
							<img class="img-responsive" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/bulk.jpg" alt="Industrial Bulk Capabilities">
						</a>
					</div>
				</div><!--end fade-->

				<div data-aos="fade-left">
					<div class="col-lg-6 col-md-4 thumb">
						<a class="thumbnail" href="" title="Private Label">
							<img class="img-responsive" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/private label.jpg" alt="Private Label">
						</a>
					</div>
				</div><!--end fade left-->
			</div><!--end row-->

			<div class="row">
				<div data-aos="fade-right">
					<div class="col-lg-6 col-md-4 thumb">
						<a class="thumbnail" href="" title="Food Service">
							<img class="img-responsive" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/food.jpg" alt="Food Service">
						</a>
					</div>
				</div><!--end fade-->

				<div data-aos="fade-left">
					<div class="col-lg-6 col-md-4 thumb">
						<a class="thumbnail" href="" title="Retail Sizes">
							<img class="img-responsive" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/retail.jpg" alt="Retail Sizes">
						</a>
					</div>
				</div><!--end fade left-->
			</div><!--end row-->
		</div>
	</div>
</div>
<div class="clear"></div>

<div class="bgimg-2">
	<div class="container">
		<div class="d-flex align-items-center d-flex justify-content-center" style="min-height: 700px">
			<div class="row mt-5">
				<div class="col-md-12 wow fadeIn mb-3">
					<div class="intro-info-content">
						<div data-aos="fade-down">
							<h1 class="display-1 white-text mb-2" style="color:white; background-color:black;text-align:center;opacity:.7; padding-top:20px;padding-bottom:20px;">
								Our Capabilities
							</h1>
						</div>
						<div data-aos="fade-up">
							<h2 class="display-1 white-text mb-2" style="color:white; background-color:black;text-align:center;opacity:.7; padding-top:20px;padding-bottom:20px;">130,000 sq. ft. state of the art facility in Riverside California</h2>
						</div>

						<div data-aos="fade-up">
							<div class="col-md-6" style="color:white; background-color:black;opacity:.7;padding-top:10px;">
								<ul>
									<li>ROPP Capper Capability</li>
									<li>Organic Certified</li>
									<li>In-House Graphic Designer</li>
									<li>Online Bulk Quotes</li>
									<li>Convenient Freeway Access</li>
									<li>Nitrogen Blanketing Available</li>
									<li>PET, Plastic and Glass Bottle Packing Capabilities</li>
									<li>Multiple Blending Tanks</li>
									<li>Returnable Tote Program</li>
									<li>Private Label Programs</li>
								</ul>
							</div>
						</div>

						<div data-aos="fade-up">
							<div class="col-md-6" style="color:white;background-color:black;opacity: .7;padding-top:10px; margin-bottom:5px;">
								<ul>
									<li>*Non GMO Project Verification – in process</li>
									<li>OU Kosher Certified</li>
									<li>Live Online Customer Service</li>
									<li>Plentiful and accessible loading docks</li>
									<li>24,000 sq. ft. State of the Art Processing Room</li>
									<li>Third Party Audited</li>
									<li>Bulk Tank Storage Farm</li>
									<li>Kosher Tote Washing Service</li>
									<li>Bulk Packing Equipment</li>
									<li>Custom Blending</li>
								</ul>
							</div>
						</div>
						<br>
						<br>

						<div class="container-fluid">
							<div data-aos="fade-right">
								<p style="text-align: center;">
									<a class="btn large copper waves-effect waves-light mobile-no" href="" style="font-family: serif;margin-right:auto;margin-left:auto;">Click Here For a Bulk Quotes</a>
								</p>
							</div>
						</div>
						<br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="bgimg-3" >
	<div class="container">
		<div class="d-flex align-items-center d-flex justify-content-center" style="min-height: 700px">
			<div class="row mt-5">
				<div class="col-md-12 mb-3">
					<div class="intro-info-content">
						<div data-aos="fade-down">
							<h1 class="display-1 white-text mb-2" style="color:white; background-color:black;text-align:center;opacity:.7; padding-top:20px;padding-bottom:20px;">
								Why Us
							</h1>
						</div>

						<div data-aos="fade-up">
							<p class="display-1 white-text mb-2" style="color:white; background-color:black;text-align:left;opacity:.7; padding-top:20px;padding-bottom:20px;padding-left:5px;font-size:18px;">Since 1998 Cibaria International has been a supplier of Olive Oils and Vinegars to companies around the world. What makes Cibaria special is our outstanding customer service, attention to detail and our ability to provide customized solutions based upon our customers needs. Cibaria International, Inc. complies with U.S. Food and Drug Administration regulations and the Food Safety Modernization Act, has a valid Hazard Analysis Critical control Point (HACCP), and has established and follows Good Manufacturing Practices (GMP) and Sanitation Standard Operating Procedures (SSOP). Cibaria conducts a yearly audit to ensure our Facility and products hold up to the highest standard available.
							</p>
						</div>
						<br />
						<br />
						<div class="container-fluid">
							<div data-aos="fade-right">
								<p style="text-align: center;">
									<a class="btn large copper mobile-no" href="" style="font-family: serif;margin-right:auto;margin-left:auto;">Click Here For a Bulk Quotes</a>
								</p>
							</div>
						</div>
						<br />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part('partials/footer'); ?>
<script>
	AOS.init({
		delay: 100
	});
</script>
</body>
</html>