<?php 
/*
Template Name: Blog
*/

 ?>
<?php echo get_template_part('partials/header-internal-pages'); ?>

<body>
    <?php echo get_template_part('partials/google-analytics'); ?>
    <div class="container">
        <?php echo get_template_part('partials/nav'); ?>
    </div>
    <div class="container" style="background-color:white;">
        <div class="jumbotron" style="height:160px;background-color:white;"></div>
        <div class="jumbotron" style="margin-top:5px;margin-bottom:2px;background-color:white; text-align: center;padding-bottom:20px;">
            <h1 style="color:#999779;border-bottom:1px solid lightgrey;"><?php echo get_the_title(); ?></h1>
        </div>
        <!-- main content start -->
        <!-- Page Content -->
        <div>
            <div class="row">
                <!-- Blog Entries Column -->
                <div class="col-md-8">
                    <?php 
            //posts
            if ( have_posts() ) : while ( have_posts() ) : the_post();?>
                    <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                    <?php echo the_content(); ?>
                    <?php endwhile; endif; ?>
                    <!-- First Blog Post -->
                    <h2>
                    <a href="#">Blog Post Title</a>
                </h2>
                    <hr>
                    <img class="img-responsive" src="http://placehold.it/900x300" alt="">
                    <hr>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, veritatis, tempora, necessitatibus inventore nisi quam quia repellat ut tempore laborum possimus eum dicta id animi corrupti debitis ipsum officiis rerum.</p>
                    <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                    <hr>
                    <!-- Pager -->
                    <ul class="pager">
                        <li class="previous">
                            <a href="#">&larr; Older</a>
                        </li>
                        <li class="next">
                            <a href="#">Newer &rarr;</a>
                        </li>
                    </ul>
                </div>
                <!-- Blog Sidebar Widgets Column -->
                <div class="col-md-4">
                    <!-- Blog Search Well -->
                    <div class="well">
                        <h4>Blog Search</h4>
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
                        <!-- /.input-group -->
                    </div>
                    <!-- Blog Categories Well -->
                    <div class="well">
                        <h4>Blog Categories</h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled">
                                    <?php
                                //Categories
                                    $args = array(
                                      'orderby' => 'name',
                                      'parent' => 0
                                      );
                                    $categories = get_categories( $args );
                                    foreach ( $categories as $category ) : ?>
                                        <li>
                                            <a href="<?php echo get_home_url() . '/category/' . $category->slug; ?>">
                                                <?php echo $category->name; ?>
                                            </a>
                                        </li>
                                        <br />
                                        <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- Side Widget Well -->
                    <div class="well">
                        <h4>Side Widget Well</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!--main content-->
        </div>
    </div>
    </div>
    </div>
    <?php echo get_template_part('partials/footer'); ?>
</body>

</html>