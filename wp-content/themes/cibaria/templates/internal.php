<?php 
/*
Template Name: Internal Pages
*/

 ?>

<?php 

//if cibaria-intl.com/coas
if(is_page('coas')) : ?>


<?php 
//basic auth
$valid_passwords = array ("ciboils" => "oliveoil");
$valid_users = array_keys($valid_passwords);

$user = $_SERVER['PHP_AUTH_USER'];
$pass = $_SERVER['PHP_AUTH_PW'];

$validated = (in_array($user, $valid_users)) && ($pass == $valid_passwords[$user]);

if (!$validated) {
  header('WWW-Authenticate: Basic realm="My Realm"');
  header('HTTP/1.0 401 Unauthorized');
  die ("Not authorized");
}
 ?>

 <?php echo get_template_part('partials/header-internal-pages'); ?>
<body>
<?php echo get_template_part('partials/google-analytics'); ?>
<div class="container">
<?php echo get_template_part('partials/nav'); ?>
</div>
<div class="container" style="background-color:white;">
    <div class="jumbotron" style="height:160px;background-color:white;"></div>
    <div class="jumbotron" style="margin-top:5px;margin-bottom:2px;background-color:white; text-align: center;padding-bottom:20px;">
        <h1 style="color:#999779;border-bottom:1px solid lightgrey;"><?php echo get_the_title(); ?></h1>
    </div>
    <div class="col-sm-12">
        <div class="col-md-12">
            <div class="page-layout">
        <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post();
    
                the_content();
  
            endwhile; endif; 
            ?>
            </div>
        </div>
    </div><!--main content-->
</div>
</div>
</div>
</div>
<?php echo get_template_part('partials/footer'); ?>
</body>
</html>


<?php 
//show everything execpt coas

else : ?>


<?php echo get_template_part('partials/header-internal-pages'); ?>
<body>
<?php echo get_template_part('partials/google-analytics'); ?>
<div class="container">
<?php echo get_template_part('partials/nav'); ?>
</div>
<div class="container" style="background-color:white;">
    <div class="jumbotron" style="height:160px;background-color:white;"></div>
    <div class="jumbotron" style="margin-top:5px;margin-bottom:2px;background-color:white; text-align: center;padding-bottom:20px;">
        <h1 style="color:#999779;border-bottom:1px solid lightgrey;"><?php echo get_the_title(); ?></h1>
    </div>
    <div class="col-sm-12">
        <div class="col-md-12">
            <div class="page-layout">
        <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post();
    
                the_content();
  
            endwhile; endif; 
            ?>
            </div>
        </div>
    </div><!--main content-->
</div>
</div>
</div>
</div>
<?php echo get_template_part('partials/footer'); ?>
</body>
</html>
<?php endif; ?>