<?php
/*
Template Name: Products
*/
?>

<?php echo get_template_part('partials/header-internal-pages'); ?>

<body>
<?php echo get_template_part('partials/google-analytics'); ?>
<div class="container">
<?php echo get_template_part('partials/nav'); ?>
</div>
<div class="container" style="background-color:white;">
    <div class="jumbotron" style="height:150px;background-color:white;"></div>

    <div class="jumbotron" style="margin-top:5px;background-color:white; text-align: center;">
 <h1 style="color:#999779;border-bottom:1px solid lightgrey;"><?php echo get_the_title(); ?></h1>
            <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post();
    
                the_content();
  
            endwhile; endif; 
            ?>
    </div>
    <div class="col-md-11 col-centered blog-main">
    <div class="row">
        <div class="col-lg-6 col-md-4 thumb">
            <a class="thumbnail" href="http://www.cibariastoresupply.com" title="Cibaria Store Supply">
                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/wholesale.jpg"
                     alt="Wholesale Olive Oils and Vinegars">
            </a>
        </div>
        <div class="col-lg-6 col-md-4 thumb">
            <a class="thumbnail" href="http://www.cibariasoapsupply.com/shop/">
                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/soap.jpg" alt="Soap Making Supplies">
            </a>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-6 col-md-4 thumb">
            <a class="thumbnail" href="" title="Industrial Bulk Capabilities">
                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/bulk.jpg" alt="Industrial Bulk Capabilities">
            </a>
        </div>
        <div class="col-lg-6 col-md-4 thumb">
            <a class="thumbnail" href="" title="Private Label">
                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/private label.jpg" alt="Private Label">
            </a>
        </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-4 thumb">
                <a class="thumbnail" href="" title="Food Service">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/food.jpg" alt="Food Service">
                </a>
            </div>
            <div class="col-lg-6 col-md-4 thumb">
                <a class="thumbnail" href="" title="Retail Sizes">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/retail.jpg" alt="Retail Sizes">
                </a>
            </div>
        </div>
    </div>
    <br />
</div>
<?php echo get_template_part('partials/footer'); ?>
</body>
</html>
