<?php
/*
Template Name: Timeline
*/

?>


<?php echo get_template_part( 'partials/header-timeline' ); ?>
<body>
<?php echo get_template_part( 'partials/google-analytics' ); ?>
<div class="container">
	<?php echo get_template_part( 'partials/nav' ); ?>
</div>
<div class="container" style="background-color:white;">
	<div class="jumbotron" style="height:160px;background-color:white;"></div>
	<div class="jumbotron"
	     style="margin-top:5px;margin-bottom:2px;background-color:white; text-align: center;padding-bottom:20px;">
		<h1 style="color:#999779;border-bottom:1px solid lightgrey;"><?php echo get_the_title(); ?></h1>
	</div>
	<div class="col-sm-12">
		<div class="col-md-12">
			<div class="page-layout">
				<div class="row">
					<div class="blog-main">

						<?php
						if ( have_posts() ) : while ( have_posts() ) : the_post();

							the_content();

						endwhile; endif;
						?>
						<div class="wpex-timeline-list center-tl  no-more no-end show-icon" id="timeline-9288"
						     data-animations="">
							<div class="wpex-loading">
								<div class="wpex-spinner">
									<div class="rect1"></div>
									<div class="rect2"></div>
									<div class="rect3"></div>
									<div class="rect4"></div>
									<div class="rect5"></div>
								</div>
							</div>
							<ul class="wpex wpex-timeline style-center">

								<li class="filter-9288_5500 post-5500 wp-timeline type-wp-timeline status-publish hentry wpex_category-facebook"
								    data-id="filter-9288_5500">
									<div class="">

										<div class="wpex-timeline-label">

											<div class="wpex-content-left">
												<div class="wpex-leftdate">
													<span class="tlday">1998</span>

													<div>
														<span>&nbsp;</span>
														<span>- 2004</span>
													</div>
												</div>
											</div>
											<!--											timeline start-->
											<div class="timeline-details">
												<div class="tlct-shortdes">
													<div></div>
													<div class="row">
														<div class="col-sm-6">
															<img
																src="<?php echo get_template_directory_uri() ?>/img/timeline/winery-building.jpg"
																alt=""/>
														</div>
														<div class="col-sm-6">
															<ul>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
															</ul>
														</div>

													</div>
												</div>
												</div>
												<div class="clearfix"></div>
											</div>
											<div class="wptl-readmore-center">
												<a href="#"
												   title="">
													Continue reading <i class="fa fa-long-arrow-right"
													                     aria-hidden="true"></i>
												</a>
											</div>
										</div>
										<div class="wpex-timeline-icon">
											<i class="fa fa-square no-icon"></i>
										</div>
								</li>
								<li class="filter-9288_5498 post-5498 wp-timeline type-wp-timeline status-publish hentry wpex_category-facebook"
								    data-id="filter-9288_5498">
									<div class="">
										<div class="wpex-timeline-label">

											<div class="wpex-content-left">
												<div class="wpex-leftdate">
													<span class="tlday">2005</span>

													<div>
														<span>&nbsp;</span>
														<span>- 2007</span>
													</div>
												</div>
											</div>
											<div class="timeline-details">
												<div class="tlct-shortdes">
													<div class="row">
														<div class="col-sm-6">
															<img
																src="<?php echo get_template_directory_uri() ?>/img/timeline/fontana.jpg"
																alt=""/>
														</div>
														<div class="col-sm-6">
															<ul>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
															</ul>
														</div>

													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="wptl-readmore-center">
											<a href="#"
											   title="">
												Continue reading <i class="fa fa-long-arrow-right"
												                    aria-hidden="true"></i>
											</a>
										</div>
									</div>
									<div class="wpex-timeline-icon">
										<a href="#"
										   title=""><i class="fa fa-square no-icon"></i></a>
									</div>
								</li>
								<li class="filter-9288_5496 post-5496 wp-timeline type-wp-timeline status-publish hentry wpex_category-facebook"
								    data-id="filter-9288_5496">
									<div class="">
										<time class="wpex-timeline-time" datetime="8:03 am August 25, 2016">
											<a href="#"
											   title="#">
												<span class="info-img"></span> </a>
											<span class="clearfix"></span>
										</time>
										<div class="wpex-timeline-label">

											<div class="wpex-content-left">
												<div class="wpex-leftdate">
													<span class="tlday">2008</span>

													<div>
														<span>&nbsp;</span>
														<span>- 2013</span>
													</div>
												</div>
											</div>
											<div class="timeline-details">
												<div class="tlct-shortdes">
													<div class="row">
														<div class="col-sm-6">
															<img
																src="<?php echo get_template_directory_uri() ?>/img/timeline/hall-building.jpg"
																alt=""/>
														</div>
														<div class="col-sm-6">
															<ul>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
															</ul>
														</div>

													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="wptl-readmore-center">
											<a href="#"
											   title="#">
												Continue reading <i class="fa fa-long-arrow-right"
												                    aria-hidden="true"></i>
											</a>
										</div>
									</div>
									<div class="wpex-timeline-icon">
										<a href="#"
										   title="#"><i class="fa fa-square no-icon"></i></a>
									</div>
								</li>
								<li class="filter-9288_5494 post-5494 wp-timeline type-wp-timeline status-publish hentry wpex_category-facebook"
								    data-id="filter-9288_5494">
									<div class="">
										<time class="wpex-timeline-time" datetime="7:56 am August 25, 2016">
											<a href="#"
											   title="#">
												<span class="info-img"></span> </a>
											<span class="clearfix"></span>
										</time>
										<div class="wpex-timeline-label">

											<div class="wpex-content-left">
												<div class="wpex-leftdate">
													<span class="tlday">2014</span>

													<div>
														<span>&nbsp;</span>
														<span>- Present</span>
													</div>
												</div>
											</div>
											<div class="timeline-details">
												<div class="tlct-shortdes">
													<div class="row">
														<div class="col-sm-6">
															<img
																src="<?php echo get_template_directory_uri() ?>/img/timeline/columbia.jpg"
																alt=""/>
														</div>
														<div class="col-sm-6">
															<ul>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
																<li>We are super awesome</li>
															</ul>
														</div>

													</div>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="wptl-readmore-center">
											<a href="#"
											   title="">
												Continue reading <i class="fa fa-long-arrow-right"
												                    aria-hidden="true"></i>
											</a>
										</div>
									</div>
									<div class="wpex-timeline-icon">
										<a href="#"
										   title="#"><i class="fa fa-square no-icon"></i></a>
									</div>
								</li>
							</ul>
						</div>
						<div class="clearfix"></div>

					</div>
					<div>

					</div>
					<!-- /.row -->
					<!--					video-->

				</div>
				<a class="btn large copper" href="" style="display: block; margin: 0 auto;git ">2018 - Video Archive</a>

			</div>
		</div>
		<!--main content-->
	</div>
</div>
</div>
</div>
<?php echo get_template_part( 'partials/footer' ); ?>
</body>
</html>