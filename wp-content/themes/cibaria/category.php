
<?php echo get_template_part('partials/header-internal-pages'); ?>
<body>
<?php echo get_template_part('partials/google-analytics'); ?>
<div class="container">
<?php echo get_template_part('partials/nav'); ?>
</div>
<div class="container" style="background-color:white;">
    <div class="jumbotron" style="height:160px;background-color:white;"></div>
    <div class="jumbotron" style="margin-top:5px;margin-bottom:2px;background-color:white; text-align: center;padding-bottom:20px;">
        <h1 style="color:#999779;border-bottom:1px solid lightgrey;">Category: <?php echo single_cat_title(); ?></h1>
    </div>
    <div class="col-sm-12">
        <div class="col-md-12">
            <div class="page-layout">
<div class="row">
    <div class=" blog-main">
        <div class="post-list" style="font-size:22px;">

        <?php 
            //posts
            if ( have_posts() ) : while ( have_posts() ) : the_post();
    
                echo '<a href=' . get_post_permalink() . ' />' .'<br />' . get_the_title() . '</a>';
  
            endwhile; endif; 
            ?>
            </div>

    </div><!-- /.row -->
            </div>
        </div>
    </div><!--main content-->
</div>
</div>
</div>
</div>
<?php echo get_template_part('partials/footer'); ?>
</body>
</html>