<?php function my_login_logo() { ?>

    <style type="text/css">

        .login h1 a {
            background-image: url( <?php echo get_template_directory_uri() ?>/img/cibaria-logo.png );
            width: 100;
            height: 100;
            background-size: contain;
        }

    </style>

<?php }

add_action( 'login_head', 'my_login_logo' );

function my_login_logo_url() {
    return 'https://www.cibaria-intl.com';
}

add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'Cibaria International Inc.';
}

add_filter( 'login_headertitle', 'my_login_logo_url_title' );

?>