<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">

    <title><?php echo get_the_title(); ?> - <?php echo get_bloginfo( 'name' ); ?></title>

    <meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>" >
    <link rel="icon" href="<?php echo get_bloginfo( 'template_directory' ); ?>/favicon.ico" type="image/x-icon" />
    
    <link href="<?php echo get_bloginfo( 'template_directory' ); ?>/css/main.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo( 'template_directory' ); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo( 'template_directory' ); ?>/css/bootstrap-theme.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo( 'template_directory' ); ?>/css/style.css" rel="stylesheet">
    <link href="<?php echo get_bloginfo( 'template_directory' ); ?>/css/jquery.dataTables.min.css" rel="stylesheet">

    <script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/modernizr-2.7.1.min.js"></script>

    <?php wp_head(); ?>
    </head>