<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php echo get_bloginfo( 'description' ) ?>">
	<meta name="author" content="">
	<title><?php echo wp_title( '', true, '' ); ?> - <?php echo get_bloginfo( 'name' ); ?></title>
	<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/price-range.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/animate.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
	<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
	<![endif]-->

	<!--	Timeline-->
	<link rel='stylesheet' id='wpex-font-awesome-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/font-awesome/css/font-awesome.min.css?ver=4.9.6'
	      type='text/css' media='all'/>
	<link rel='stylesheet' id='wpex-google-fonts-css'
	      href='//fonts.googleapis.com/css?family=Source+Sans+Pro&#038;ver=1.0.0' type='text/css' media='all'/>
	<link rel='stylesheet' id='wpex-ex_s_lick-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/js/ex_s_lick/ex_s_lick.css?ver=4.9.6'
	      type='text/css' media='all'/>
	<link rel='stylesheet' id='wpex-ex_s_lick-theme-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/js/ex_s_lick/ex_s_lick-theme.css?ver=4.9.6'
	      type='text/css' media='all'/>
	<link rel='stylesheet' id='wpex-timeline-animate-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/animate.css?ver=4.9.6' type='text/css'
	      media='all'/>
	<link rel='stylesheet' id='wpex-timeline-css-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/style.css?ver=4.9.6' type='text/css'
	      media='all'/>
	<link rel='stylesheet' id='wpex-timeline-sidebyside-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/style-sidebyside.css?ver=4.9.6'
	      type='text/css' media='all'/>
	<link rel='stylesheet' id='wpex-horiz-css-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/horiz-style.css?ver=4.9.6' type='text/css'
	      media='all'/>
	<link rel='stylesheet' id='wpex-timeline-dark-css-css'
	      href='<?php echo get_template_directory_uri(); ?>/css/timeline/dark.css?ver=4.9.6' type='text/css'
	      media='all'/>
	<style id='wpex-timeline-dark-css-inline-css' type='text/css'>
		.wpextl-loadicon::before,
		.wpextl-loadicon::after {
			#999779
		}

		.wpex-filter > .fa,
		.wpex-endlabel.wpex-loadmore span, .wpex-tltitle.wpex-loadmore span, .wpex-loadmore .loadmore-timeline,
		.wpex-timeline-list.show-icon .wpex-timeline > li:after, .wpex-timeline-list.show-icon .wpex-timeline > li:first-child:before,
		.wpex-timeline-list.show-icon .wpex-timeline.style-center > li .wpex-content-left .wpex-leftdate,
		.wpex-timeline-list.show-icon li .wpex-timeline-icon .fa,
		.wpex .timeline-details .wptl-readmore > a:hover,
		.wpex-spinner > div,
		.wpex.horizontal-timeline .ex_s_lick-prev:hover, .wpex.horizontal-timeline .ex_s_lick-next:hover,
		.wpex.horizontal-timeline .horizontal-content .ex_s_lick-next:hover,
		.wpex.horizontal-timeline .horizontal-content .ex_s_lick-prev:hover,
		.wpex.horizontal-timeline .horizontal-nav li.ex_s_lick-current a:before,
		.wpex.horizontal-timeline.tl-hozsteps .horizontal-nav li.ex_s_lick-current a i,
		.timeline-navigation a.btn,
		.timeline-navigation div > a,
		.wpex.horizontal-timeline.ex-multi-item .horizontal-nav li a:before,
		.wpex.horizontal-timeline.ex-multi-item .horizontal-nav li.ex_s_lick-current a:before,
		.wpex.wpex-horizontal-3.ex-multi-item .horizontal-nav h2 a,
		.wpex-timeline-list:not(.show-icon) .wptl-feature-name span,
		.wpex.horizontal-timeline.ex-multi-item:not(.wpex-horizontal-4) .horizontal-nav li a.wpex_point,
		.wpex.horizontal-timeline.ex-multi-item:not(.wpex-horizontal-4) .horizontal-nav li a.wpex_point,
		.show-wide_img .wpex-timeline > li .wpex-timeline-time span.tll-date,
		.wpex-timeline-list.show-bg.left-tl li .wpex-timeline-label .wpex-content-left .wpex-leftdate,
		.wpex-timeline-list.show-simple:not(.show-simple-bod) ul li .wpex-timeline-time .tll-date,
		.show-box-color .tlb-time,
		.sidebyside-tl.show-classic span.tll-date,
		.wpex-timeline > li .wpex-timeline-icon .fa {
			background: #999779
		}

		.wpex-timeline-list.show-icon li .wpex-timeline-icon .fa:before,
		.wpex-filter span.active,
		.wpex-timeline-list.show-simple.show-simple-bod ul li .wpex-timeline-time .tll-date,
		.wpex-timeline-list.show-simple .wptl-readmore-center a,
		.wpex-timeline-list .wpex-taxonomy-filter a:hover, .wpex-timeline-list .wpex-taxonomy-filter a.active,
		.wpex.horizontal-timeline .ex_s_lick-prev, .wpex.horizontal-timeline .ex_s_lick-next,
		.wpex.horizontal-timeline.tl-hozsteps .horizontal-nav li.prev_item:not(.ex_s_lick-current) a i,
		.wpex.horizontal-timeline.ex-multi-item .horizontal-nav li a.wpex_point i,
		.wpex-timeline-list.show-clean .wpex-timeline > li .wpex-timeline-label h2,
		.wpex-timeline-list.show-simple li .wpex-timeline-icon .fa:not(.no-icon):before,
		.show-wide_img.left-tl .wpex-timeline > li .wpex-timeline-icon .fa:not(.no-icon):not(.icon-img):before,
		.wpex-timeline > li .wpex-timeline-time span:last-child {
			color: #999779
		}

		.wpex .timeline-details .wptl-readmore > a,
		.wpex.horizontal-timeline .ex_s_lick-prev:hover, .wpex.horizontal-timeline .ex_s_lick-next:hover,
		.wpex.horizontal-timeline .horizontal-content .ex_s_lick-next:hover,
		.wpex.horizontal-timeline .horizontal-content .ex_s_lick-prev:hover,
		.wpex.horizontal-timeline .horizontal-nav li.ex_s_lick-current a:before,
		.wpex.horizontal-timeline .ex_s_lick-prev, .wpex.horizontal-timeline .ex_s_lick-next,
		.wpex.horizontal-timeline .timeline-pos-select,
		.wpex.horizontal-timeline .horizontal-nav li.prev_item a:before,
		.wpex.horizontal-timeline.tl-hozsteps .horizontal-nav li.ex_s_lick-current a i,
		.wpex.horizontal-timeline.tl-hozsteps .timeline-hr, .wpex.horizontal-timeline.tl-hozsteps .timeline-pos-select,
		.wpex.horizontal-timeline.tl-hozsteps .horizontal-nav li.prev_item a i,
		.wpex-timeline-list.left-tl.show-icon .wptl-feature-name,
		.wpex-timeline-list.show-icon .wptl-feature-name span,
		.wpex.horizontal-timeline.ex-multi-item .horizontal-nav li a.wpex_point i,
		.wpex.horizontal-timeline.ex-multi-item.wpex-horizontal-4 .wpextt_templates .wptl-readmore a,
		.wpex-timeline-list.show-box-color .style-center > li:nth-child(odd) .wpex-timeline-label,
		.wpex-timeline-list.show-box-color .style-center > li .wpex-timeline-label,
		.wpex-timeline-list.show-box-color .style-center > li:nth-child(odd) .wpex-timeline-icon .fa:after,
		.wpex-timeline-list.show-box-color li .wpex-timeline-icon i:after,
		.wpex.wpex-horizontal-3.ex-multi-item .horizontal-nav .wpextt_templates .wptl-readmore a {
			border-color: #999779;
		}

		.wpex-timeline > li .wpex-timeline-label:before,
		.show-wide_img .wpex-timeline > li .wpex-timeline-time span.tll-date:before,
		.wpex-timeline > li .wpex-timeline-label:before,
		.wpex-timeline-list.show-wide_img.left-tl .wpex-timeline > li .wpex-timeline-time span.tll-date:before,
		.wpex-timeline-list.show-icon.show-bg .wpex-timeline > li .wpex-timeline-label:after,
		.wpex-timeline-list.show-icon .wpex-timeline.style-center > li .wpex-timeline-label:after {
			border-right-color: #999779;
		}

		.wpex-filter span,
		.wpex-timeline > li .wpex-timeline-label {
			border-left-color: #999779;
		}

		.wpex-timeline-list.show-wide_img .wpex-timeline > li .timeline-details,
		.wpex.horizontal-timeline.ex-multi-item:not(.wpex-horizontal-4) .horizontal-nav li a.wpex_point:after {
			border-top-color: #999779;
		}

		.wpex.wpex-horizontal-3.ex-multi-item .wpex-timeline-label .timeline-details:after {
			border-bottom-color: #999779;
		}

		.timeline-details li{
			list-style-type: disc;
		}

		@media (min-width: 768px) {
			.wpex-timeline.style-center > li:nth-child(odd) .wpex-timeline-label {
				border-right-color: #999779;
			}

			.show-wide_img .wpex-timeline > li:nth-child(even) .wpex-timeline-time span.tll-date:before,
			.wpex-timeline.style-center > li:nth-child(odd) .wpex-timeline-label:before,
			.wpex-timeline-list.show-icon .style-center > li:nth-child(odd) .wpex-timeline-label:after {
				border-left-color: #999779;
			}
		}

	</style>
	<style type="text/css">
		body {
		<?php
		//background image
		if(is_page('products')): ?> background-image: url('<?php echo get_template_directory_uri(); ?>/img/shop/products-bg.jpg');
		<?php else: ?> background-image: url('<?php echo get_template_directory_uri(); ?>/img/slide3.jpg');
		<?php endif; ?>
		}
	</style>
	<?php wp_head(); ?>
</head>
<!--/head-->
