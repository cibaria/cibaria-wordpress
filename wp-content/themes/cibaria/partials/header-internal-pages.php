<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo get_bloginfo( 'description' ) ?>">
    <meta name="author" content="">
    <title><?php echo wp_title('', true, ''); ?> - <?php echo get_bloginfo( 'name' ); ?></title>
    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/price-range.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/animate.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/main.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
        body{
            <?php 
            //background image
            if(is_page('products')): ?>
            background-image: url('<?php echo get_template_directory_uri(); ?>/img/shop/products-bg.jpg');
            <?php else: ?>
            background-image: url('<?php echo get_template_directory_uri(); ?>/img/slide3.jpg');
            <?php endif; ?>
        }
    </style>
    <?php wp_head(); ?>
</head><!--/head-->