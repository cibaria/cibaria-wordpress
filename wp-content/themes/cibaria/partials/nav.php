<div class="navbar navbar-fixed-top" role="navigation">
    <div class="container" style="padding-left:50px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="row">
                <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo2.png" class="img-responsive" style="padding-top:8px;" /></a>
            </div>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="set"><a href="<?php echo get_permalink( get_page_by_path( 'bulk-quotes') ) ?>">Bulk Quotes</a></li>
                <li><a href="<?php echo get_permalink( get_page_by_path( 'about') ) ?>">About</a></li>
                <li><a href="<?php echo get_permalink( get_page_by_path( 'contact') ) ?>">Contact</a></li>
                <li><a href="<?php echo get_permalink( get_page_by_path( 'blog') ) ?>">Blog</a></li>
                <li><a href="<?php echo get_permalink( get_page_by_path( 'videos') ) ?>">Videos</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>

    <!-- main nav -->
    <div class="main-nav">
        <div class="container">
            <div class="col-md-2"></div>
            <div class="col-md-6">
                <!-- subnav -->
<div id="menu1">
    <ul class="menu-top">
        <li><a href="<?php echo get_home_url(); ?>" class="menu-button" title="Cibaria International"><span class="menu-label">Home</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'products') ) ?>" title="Products" class="menu-button menu-drop"><span class="menu-label">Products</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'capabilities') ) ?>" title="Capabilities" class="menu-button menu-drop"><span class="menu-label">Capabilities</span></a>
            <div class="menu-dropdown menu-dropdown3">
                <div class="menu-row">
                    <ul class="menu-sub">
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'capabilities/industrial') ) ?>" class="menu-subbutton" title="Industrial Capabilities"><span class="menu-label">Industrial/Bulk</span></a></li>
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'capabilities/private-label') ) ?>" class="menu-subbutton" title="Private Label"><span class="menu-label">Private Label Program</span></a></li>
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'capabilities/food-service') ) ?>" class="menu-subbutton" title="Food Service"><span class="menu-label">Food Service</span></a></li>
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'capabilities/retail-sizes') ) ?>" class="menu-subbutton" title="Retail Sizes"><span class="menu-label">Retail Sizes</span></a></li>
                    </ul>
                </div>
        </li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'quality-control') ) ?>" title="Quality Control" class="menu-button menu-drop"><span class="menu-label">Quality Control</span></a>
            <div class="menu-dropdown menu-dropdown3">
                <div class="menu-row">
                    <ul class="menu-sub">
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'quality-control/bioterrorism-program') ) ?>" class="menu-subbutton" title="Bioterrorism"><span class="menu-label">Bioterrorism</span></a></li>
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'quality-control/haccp') ) ?>" class="menu-subbutton" title="HACCP"><span class="menu-label">HACCP</span></a></li>
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'quality-control/third-party-audits') ) ?>" class="menu-subbutton" title="Third Party Audits"><span class="menu-label">Third Party Audits</span></a></li>
                        <li><a href="<?php echo get_permalink( get_page_by_path( 'quality-control/gmp-ssop') ) ?>" class="menu-subbutton" title="GMP and SSOP"><span class="menu-label">GMP's / SSOP's</span></a></li>
                    </ul>
                </div>
        </li>
        <li>
            <a href="<?php echo get_permalink( get_page_by_path( 'specsheets') ) ?>" title="Documentation" class="menu-button menu-drop"><span class="menu-label">Documentation</span></a>
            <div class="menu-dropdown menu-dropdown3">
                <div class="menu-row">
                    <ul class="menu-sub">
                <li><a href="<?php echo get_permalink( get_page_by_path( 'specsheets') ) ?>" title="Specsheets" class="menu-subbutton"><span class="menu-label">Specsheets</span></a></li>
                <!-- <li><a href="<?php //echo get_permalink( get_page_by_path( 'coas') ) ?>" title="COAS" class="menu-subbutton"><span class="menu-label">COAS</span></a></li> -->
            </ul>
        </div>
    </div>
</li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'certifications' ) ) ?>" title="Certifications" class="menu-button menu-drop"><span class="menu-label">Certifications</span></a>
<!--        <div class="menu-dropdown menu-dropdown3">-->
<!--            <div class="menu-row">-->
<!--                <ul class="menu-sub">-->
<!--                    <li><a href="" title="QA Statements" class="menu-subbutton"><span class="menu-label">QA Statements</span></a></li>-->
<!--                </ul>-->
<!--                </div>-->
<!--            </div>-->
        </li>
    </ul>
</div>
                <!-- end subnav -->
            </div>
        </div>
    </div>
</div>