<div id="footer">
    <!-- Begin contacts //-->
    <div class="column-contacts">
        <ul>
            <h3>Cibaria International</h3>
            <li class="phone">705 Columbia Ave, Riverside, CA 92507</li>
            <li class="phone">Phone: (951) 823 - 8490</li>
            <li class="mobile">Fax: (951) 823 - 8495</li>
        </ul>
    </div>
    <!-- End contacts //-->

    <!-- Begin footer columns //-->
    <div class="column4">
        <h3>Cibaria Websites</h3>
        <ul>
            <li><a href="http://www.cibariastoresupply.com" title="Bulk Olive Oil Supplier">Bulk Olive Oil Supplier</a></li>
            <li><a href="http://www.cibariasoapsupply.com/shop/" title="Wholesale Soap Making Supplies">Wholesale Soap Making Supplies</a></li>
        </ul>
    </div>

    <div class="column3">
        <h3>Why Us</h3>
        <ul>
            <li><a href="" title="Capabilities">Capabilities</a></li>
            <li><a href="" title="Quality Control">Quality Control</a></li>
            <li><a href="" title="Certifications">Certifications</a></li>
            <li><a href="" title="Products">Products</a></li>
        </ul>
    </div>

    <div class="column2">
        <h3>Customer Service</h3>
        <ul>
            <li><a href="" rel="nofollow" title="Privacy Policy">Privacy Policy</a></li>
            <li><a href="" rel="nofollow" title="Terms and Conditions">Terms &amp; Conditions</a></li>
        </ul>
    </div>

    <div class="column1">
        <h3>Information</h3>
        <ul>
            <li><a href="" title="About Us">About Us</a></li>
            <li><a href="" title="FAQ">FAQ</a></li>
        </ul>
    </div>
</div>

<div id="powered" style="padding-left:50px;">
    Copyright © <?php echo Date("Y"); ?> Cibaria International - All Rights Reserved
    <div style="clear:both"></div>
</div>

<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/aos.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/imagesloaded.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/enquire.min.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/skrollr.js"></script>
<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/_main.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo get_bloginfo( 'template_directory' ); ?>/js/bootstrap.min.js"></script>