<section id="slide-3" class="homeSlide">
    <div class="bcg"
         data-center="background-position: 50% 0px;"
         data-top-bottom="background-position: 50% -100px;"
         data-bottom-top="background-position: 50% 100px;"
         data-anchor-target="#slide-3"
            >
        <div class="hsContainer">
            <div class="hsContent"
                 data--50-bottom="opacity: 0;"
                 data--200-bottom="opacity: 1;"
                 data-center="opacity: 1"
                 data-200-top="opacity: 0"
                 data-anchor-target="#slide-3 h2">

                <?php 
                //Advanced Custom Fields Plugin
                echo get_field('services'); ?>
                
                <h2 style="text-align: center;">Our Products</h2>
<!--                <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/arrow.png" alt="" class="image_center"/>-->
            </div>
        </div>

    </div>
</section>