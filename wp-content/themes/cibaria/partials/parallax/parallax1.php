<section id="slide-2a" class="homeSlide">
    <div class="bcg"
         data-center="background-position: 50% 10px;"
         data-top-bottom="background-position: 50% -100px;"
         data-bottom-top="background-position: 50% 100px;"
         data-anchor-target="#slide-2a"
            >

        <div class="hsContainer">
            <div class="hsContent"
                 data-center="opacity: 1"
                 data-center-top="opacity: 0"
                 data--100-bottom="opacity: 0;"
                 data-anchor-target="#slide-2a">

                <?php 
                //Advanced Custom Fields Plugin
                echo get_field('capabilities'); ?>
                  
                    </div>
                <h2 style="text-align: center;margin-top:1px">Our Service</h2>
<!--                <img src="--><?php //echo get_template_directory_uri(); ?><!--/img/arrow.png" alt="Our Service" class="image_center img-responsive" style="width:125px;"/>-->
            </div>
        </div>
    </div>
</section>