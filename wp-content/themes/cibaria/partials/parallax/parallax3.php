<section id="slide-4" class="homeSlide">
    <div class="bcg"
         data-center="background-position: 50% 0px;"
         data-top-bottom="background-position: 50% -100px;"
         data-bottom-top="background-position: 50% 100px;"
         data-anchor-target="#slide-4"
            >
        <div class="hsContainer">
            <div class="hsContent"
                 data-bottom-top="opacity: 0"
                 data-25p-top="opacity: 0"
                 data-top="opacity: 1"
                 data-anchor-target="#slide-4"
                    >
                <a class="btn large copper" href="" style="margin-left:-150px;margin-top:300px;">Click Here to Explore Our Products</a>
            </div>
        </div>
    </div>
</section>